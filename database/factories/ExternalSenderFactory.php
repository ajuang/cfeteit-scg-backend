<?php

namespace Database\Factories;

use App\Models\ExternalSender;
use Illuminate\Database\Eloquent\Factories\Factory;

class ExternalSenderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ExternalSender::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
