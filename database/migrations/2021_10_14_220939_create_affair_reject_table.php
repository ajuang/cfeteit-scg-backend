<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAffairRejectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affair_rejects', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cat_rejection_reason_id')->nullable();
            $table->unsignedBigInteger('affair_reception_id')->nullable();
            $table->text('description');

            $table->foreign('affair_reception_id')->references('id')->on('affair_receptions');
//            $table->foreign('folio_origin')->references('folio')->on('affair_receptions');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affair_reject');
    }
}
