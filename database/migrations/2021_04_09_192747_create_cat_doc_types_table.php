<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatDocTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_doc_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedbiginteger('unit_id');
            $table->unsignedBigInteger('file_id')->nullable();
            $table->softdeletes();
            $table->timestamps();

            $table->foreign('unit_id')->references('id')->on('cat_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_doc_types');
    }
}
