<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssistantStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistant_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('name');

            $table->unsignedBigInteger('unit_id');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('unit_id')->references('id')->on('cat_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistant_statuses');
    }
}
