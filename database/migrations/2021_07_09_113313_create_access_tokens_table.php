<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccessTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('access_tokens', function (Blueprint $table) {
            $table->id();
            $table->string('token')->nullable(); // temporaly nullable (only for save permissions)
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('file_id')->nullable(); // temporaly nullable (only for save permissions)
            $table->boolean('UserCanWrite');
            $table->boolean('HidePrintOption');
            $table->boolean('DisablePrint');
            $table->boolean('HideSaveOption');
            $table->boolean('HideExportOption');
            $table->boolean('DisableExport');
            $table->boolean('DisableCopy');
            $table->boolean('acceptCollabora')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('file_id')->references('id')->on('files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('access_tokens');
    }
}
