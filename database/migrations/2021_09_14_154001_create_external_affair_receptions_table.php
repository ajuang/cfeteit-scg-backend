<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExternalAffairReceptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('external_affair_receptions', function (Blueprint $table) {
        $table->id();
        $table->string('folio')->nullable();
        $table->string('subject')->nullable();
        $table->string('description')->nullable();
        $table->date('document_date')->nullable();
        $table->date('attention_date')->nullable();
        $table->date('reception_date');
        $table->time('reception_time');
        // $table->unsignedBigInteger('cat_sender_types_id')->nullable();
        $table->unsignedBigInteger('cat_recep_meds_id')->nullable();
        $table->unsignedBigInteger('cat_doc_types_id')->nullable();;
        $table->unsignedBigInteger('cat_priorities_id')->nullable();
        $table->unsignedBigInteger('people_id')->nullable();
        // $table->unsignedBigInteger('files_id')-
        $table->unsignedBigInteger('user_id');
        // $table->unsignedBigInteger('sender_unit_id')->nullable();
        // $table->unsignedBigInteger('sender_position_id')->nullable();
        // $table->unsignedBigInteger('sender_responsable_id')->nullable();


        
        // $table->foreign('cat_sender_types_id')->references('id')->on('cat_sender_types');
        $table->foreign('cat_recep_meds_id')->references('id')->on('cat_recep_meds');
        $table->foreign('cat_doc_types_id')->references('id')->on('cat_doc_types');
        $table->foreign('cat_priorities_id')->references('id')->on('cat_priorities');
        $table->foreign('people_id')->references('id')->on('people');
        $table->foreign('user_id')->references('id')->on('users');
        // $table->foreign('files_id')->references('id')->on('files');
        
        // $table->foreign('sender_unit_id')->references('id')->on('cat_units');
        // $table->foreign('sender_position_id')->references('id')->on('positions');
        // $table->foreign('sender_responsable_id')->references('id')->on('people');

        $table->timestamps();
      });
      DB::unprepared('
          CREATE TRIGGER tg_external_affair_receptions_log
            BEFORE DELETE OR INSERT OR UPDATE ON
            external_affair_receptions FOR EACH ROW
            EXECUTE FUNCTION tg_fn_user_log();
          ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('external_affair_receptions');
    }
}
