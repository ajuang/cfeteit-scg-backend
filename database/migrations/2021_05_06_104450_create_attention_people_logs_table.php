<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttentionPeopleLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attention_people_logs', function (Blueprint $table) {
            $table->id();
            $table->dateTime('dateHour_attention');
            $table->unsignedBigInteger('attention_type_id');
            $table->unsignedBigInteger('user_id');
            $table->timestamps();

            $table->foreign('attention_type_id')->references('id')->on('cat_attention_types');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attention_people_logs');
    }
}
