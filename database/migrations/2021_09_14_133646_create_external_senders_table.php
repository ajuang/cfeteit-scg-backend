<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExternalSendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('external_senders', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('external_people_id')->nullable();
            $table->unsignedBigInteger('external_company_id')->nullable();
            $table->unsignedBigInteger('sender_entity_id')->nullable();
            $table->unsignedBigInteger('sender_type_id')->nullable();

            $table->foreign('external_people_id')->references('id')->on('external_people');
            $table->foreign('external_company_id')->references('id')->on('external_companies');
            $table->foreign('sender_entity_id')->references('id')->on('entities');
            $table->foreign('sender_type_id')->references('id')->on('cat_sender_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('external_senders');
    }
}
