<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatThemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_themes', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('affair_reception_id');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('affair_reception_id')->references('id')->on('affair_receptions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_themes');
    }
}
