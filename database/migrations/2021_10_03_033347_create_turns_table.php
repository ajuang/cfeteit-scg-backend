<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTurnsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('turns', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('affair_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('instruction_id');
            $table->unsignedBigInteger('status_id')->default('1');
            $table->boolean('seen')->default(0);
            $table->unsignedBigInteger('parent_turn_id')->nullable();
            $table->timestamps();

            $table->foreign('affair_id')->references('id')->on('affair_receptions');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('instruction_id')->references('id')->on('cat_instructions');
            $table->foreign('status_id')->references('id')->on('cat_turn_statuses');
            $table->foreign('parent_turn_id')->references('id')->on('turns');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('turns');
    }
}
