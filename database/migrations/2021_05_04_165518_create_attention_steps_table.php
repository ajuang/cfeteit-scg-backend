<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttentionStepsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attention_steps', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('step_type_id');
            $table->string('name');
            $table->string('order');
            $table->unsignedBigInteger('attention_route_id');
            $table->boolean('is_finisher');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('step_type_id')->references('id')->on('cat_step_types');
            $table->foreign('attention_route_id')->references('id')->on('attention_routes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attention_steps');
    }
}
