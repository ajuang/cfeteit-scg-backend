<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('affair_id')->nullable();
            $table->string('filetype',5);
            $table->string('path', 500);
            $table->string('filename');
            $table->unsignedBigInteger('filesize');
            $table->string('originalname');
            $table->dateTime('LastModifiedTime');
            $table->boolean('is_secure')->default(0);
            $table->boolean('isActive')->default(1);
            $table->boolean('is_affair')->default(0);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('affair_id')->references('id')->on('affair_receptions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
}
