<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatUnitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_units', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('entity_id');
            $table->string('name');
            $table->string('code');
            $table->string('description')->nullable();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('responsible')->nullable();
            $table->unsignedBigInteger('parent_unit_id')->nullable();
            $table->time('entry')->nullable();
            $table->time('departure')->nullable();
            $table->boolean('operator')->nullable();
            $table->boolean('link_unit')->nullable();
            $table->boolean('isActive')->default(1);
            $table->softDeletes('deleted_at', 0);
            $table->string('nomenclature')->nullable();
            $table->string('acronym');
            $table->timestamps();
            /* FOREIGN KEYS */
            $table->foreign('entity_id')->references('id')->on('entities');
            $table->foreign('parent_unit_id')->references('id')->on('cat_units');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_units');
    }
}
