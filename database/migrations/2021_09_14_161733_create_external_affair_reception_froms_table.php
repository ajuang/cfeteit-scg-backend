<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExternalAffairReceptionFromsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('external_affair_reception_froms', function (Blueprint $table) {
        $table->id();
        $table->unsignedBigInteger('external_affair_receptions_id');
        $table->unsignedBigInteger('receiver_id');
        $table->unsignedBigInteger('participation_type_id');
        $table->unsignedBigInteger('sender_id')->nullable();
        $table->unsignedBigInteger('external_sender_id');
        $table->timestamps();

        $table->foreign('external_affair_receptions_id')->references('id')->on('external_affair_receptions');
        $table->foreign('receiver_id')->references('id')->on('users');
        $table->foreign('participation_type_id')->references('id')->on('participation_types');
        $table->foreign('sender_id')->references('id')->on('users');
        $table->foreign('external_sender_id')->references('id')->on('external_senders');
      });
      DB::unprepared('
        CREATE TRIGGER tg_external_affair_reception_froms_log
          BEFORE DELETE OR INSERT OR UPDATE ON
          external_affair_reception_froms FOR EACH ROW
          EXECUTE FUNCTION tg_fn_user_log();
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('external_affair_reception_froms');
    }
}
