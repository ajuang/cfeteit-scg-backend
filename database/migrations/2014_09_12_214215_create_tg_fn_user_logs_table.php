<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTgFnUserLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      DB::unprepared('
        CREATE OR REPLACE FUNCTION tg_fn_user_log() RETURNS TRIGGER AS $$
          BEGIN
            INSERT INTO general_logs VALUES (default, TG_RELID, TG_TABLE_NAME, TG_OP, NEW, OLD, current_timestamp);
            RETURN NEW;
          END;
          $$ LANGUAGE plpgsql;
        ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tg_fn_user_logs');
        Schema::dropIfExists('tg_user_logs');
    }
}
