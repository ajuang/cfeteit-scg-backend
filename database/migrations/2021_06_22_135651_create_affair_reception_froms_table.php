<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAffairReceptionFromsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affair_reception_froms', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('affair_receptions_id');
            $table->unsignedBigInteger('receiver_id');
            $table->unsignedBigInteger('participation_types_id');
            $table->unsignedBigInteger('sender_id');
            $table->boolean('historical')->nullable();
            $table->timestamp('seen_date')->nullable();
            $table->timestamps();

            $table->foreign('affair_receptions_id')->references('id')->on('affair_receptions');
            $table->foreign('receiver_id')->references('id')->on('users');
            $table->foreign('participation_types_id')->references('id')->on('participation_types');
            $table->foreign('sender_id')->references('id')->on('users');
        });
        DB::unprepared('
          CREATE TRIGGER tg_affair_reception_froms_log
            BEFORE DELETE OR INSERT OR UPDATE ON
            affair_reception_froms FOR EACH ROW
            EXECUTE FUNCTION tg_fn_user_log();
          ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affair_reception_froms');
    }
}
