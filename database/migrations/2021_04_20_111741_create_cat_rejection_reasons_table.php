<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatRejectionReasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_rejection_reasons', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('cat_unit_id');
            $table->string('description');
            $table->boolean('isActive')->default(1);
            $table->softDeletes('deleted_at', 0);
            $table->timestamps();

            $table->foreign('cat_unit_id')->references('id')->on('cat_units');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_rejection_reasons');
    }
}
