<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttentionPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attention_people', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('attention_step_id');
            $table->unsignedBigInteger('unit_id');
            $table->unsignedBigInteger('attention_type_id');
            $table->unsignedBigInteger('instruction_id');
            $table->unsignedBigInteger('user_id');
            $table->boolean('has_privileges');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('attention_step_id')->references('id')->on('attention_steps');
            $table->foreign('unit_id')->references('id')->on('cat_units');
            $table->foreign('attention_type_id')->references('id')->on('cat_attention_types');
            $table->foreign('instruction_id')->references('id')->on('cat_instructions');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attention_people');
    }
}
