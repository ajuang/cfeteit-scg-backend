<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAffairReceptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affair_receptions', function (Blueprint $table) {
            $table->id();
            $table->string('folio')->nullable();
            $table->string('subject')->nullable();
            $table->date('document_date')->nullable();
            $table->boolean('has_collaborators')->default(0);
            $table->unsignedBigInteger('status_id');
            $table->boolean('require_response')->default(1);
            $table->boolean('historical')->default(0);
            $table->unsignedBigInteger('cat_doc_types_id');
            $table->unsignedBigInteger('cat_priorities_id');
            $table->timestamp('date_send')->nullable();
            $table->unsignedBigInteger('user_id');

            $table->foreign('cat_doc_types_id')->references('id')->on('cat_doc_types');
            $table->foreign('cat_priorities_id')->references('id')->on('cat_priorities');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('status_id')->references('id')->on('affair_statuses');

            $table->timestamps();
        });
        DB::unprepared('
            CREATE TRIGGER tg_affair_receptions_log
              BEFORE DELETE OR INSERT OR UPDATE ON
              affair_receptions FOR EACH ROW
              EXECUTE FUNCTION tg_fn_user_log();
            ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affair_receptions');
    }
}
