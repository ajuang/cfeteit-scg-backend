<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatKeywordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cat_keywords', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedBigInteger('attention_route_id');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('attention_route_id')->references('id')->on('attention_routes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cat_keywords');
    }
}
