<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateExternalAffairCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('external_affair_comments', function (Blueprint $table) {
        $table->id();
        $table->unsignedBigInteger('external_affair_reception_id');
        $table->text('comment');
        $table->unsignedBigInteger('user_id');
        $table->timestamps();

        $table->foreign('user_id')->references('id')->on('users');
        $table->foreign('external_affair_reception_id')->references('id')->on('external_affair_receptions');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('external_affair_comments');
    }
}
