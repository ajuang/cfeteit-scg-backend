<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStructuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('structures', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('entity_id');
            $table->unsignedBigInteger('position_id');
            $table->unsignedBigInteger('position_to_report_id')->nullable();
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('entity_id')->references('id')->on('entities');
            $table->foreign('position_id')->references('id')->on('positions');
            $table->foreign('position_to_report_id')->references('id')->on('positions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('structures');
    }
}
