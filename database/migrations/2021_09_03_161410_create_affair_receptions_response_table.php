<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAffairReceptionsResponseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('affair_reception_responses', function (Blueprint $table) {
            $table->id();
            $table->string('folio_origin')->nullable();
            $table->string('folio_response')->nullable();
            // $table->unsignedBigInteger('files_id')->nullable();
            // $table->foreign('folio_origin')->references('folio')->on('affair_receptions');
            // $table->foreign('folio_response')->references('folio')->on('affair_receptions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affair_receptions');
    }
}
