<?php

namespace Database\Seeders;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->insert(
            [
                [
                    'id'                => 1,
                    'username'          =>'federico.gonzalez',
                    'password'          => Hash::make('12345678'),
                    'profile_id'        => 1,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 2,
                    'username'          =>'david.argente',
                    'password'          => Hash::make('12345678'),
                    'profile_id'        => 2,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 3,
                    'username'          =>'karen.gutierrez',
                    'password'          => Hash::make('12345678'),
                    'profile_id'        => 3,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 4,
                    'username'          =>'rafael.munoz',
                    'password'          => Hash::make('12345678'),
                    'profile_id'        => 3,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 5,
                    'username'          =>'marco.triana',
                    'password'          => Hash::make('12345678'),
                    'profile_id'        => 2,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 6,
                    'username'          =>'flor.hernandez',
                    'password'          => Hash::make('87654321'),
                    'profile_id'        => 2,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 7,
                    'username'          =>'norma.carrasco',
                    'password'          => Hash::make('87654321'),
                    'profile_id'        => 2,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 8,
                    'username'          =>'eutasio.sanchez',
                    'password'          => Hash::make('87654321'),
                    'profile_id'        => 3,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 9,
                    'username'          =>'adrian.moran',
                    'password'          => Hash::make('87654321'),
                    'profile_id'        => 4,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 10,
                    'username'          =>'leslie.neri',
                    'password'          => Hash::make('87654321'),
                    'profile_id'        => 5,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 11,
                    'username'          =>'ricardo.cambroni',
                    'password'          => Hash::make('87654321'),
                    'profile_id'        => 5,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 12,
                    'username'          =>'jesus.rios',
                    'password'          => Hash::make('87654321'),
                    'profile_id'        => 2,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 13,
                    'username'          =>'maria.luna',
                    'password'          => Hash::make('87654321'),
                    'profile_id'        => 5,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 14,
                    'username'          =>'nalleli.montoya',
                    'password'          => Hash::make('87654321'),
                    'profile_id'        => 5,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 15,
                    'username'          =>'carlos.lavandeira',
                    'password'          => Hash::make('87654321'),
                    'profile_id'        => 5,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 16,
                    'username'          =>'juan.vega',
                    'password'          => Hash::make('87654321'),
                    'profile_id'        => 5,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 17,
                    'username'          =>'luis.montaño',
                    'password'          => Hash::make('87654321'),
                    'profile_id'        => 5,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 18,
                    'username'          =>'edgar.gonalez',
                    'password'          => Hash::make('87654321'),
                    'profile_id'        => 5,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 19,
                    'username'          =>'jorge.galindo',
                    'password'          => Hash::make('87654321'),
                    'profile_id'        => 5,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 20,
                    'username'          =>'claudio.moran',
                    'password'          => Hash::make('87654321'),
                    'profile_id'        => 5,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 21,
                    'username'          =>'raul.perez',
                    'password'          => Hash::make('87654321'),
                    'profile_id'        => 5,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                // External users to registration test
                [
                    'id'                => 22,
                    'username'          =>'juan.ruiz',
                    'password'          => Hash::make('12345678'),
                    'profile_id'        => 1,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 23,
                    'username'          =>'yessica.mecatl',
                    'password'          => Hash::make('12345678'),
                    'profile_id'        => 1,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                [
                    'id'                => 24,
                    'username'          =>'adrian.muñoz',
                    'password'          => Hash::make('12345678'),
                    'profile_id'        => 3,
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                /*
                [
                    'id'                => 5,
                    'username'          =>'steve.jobs',
                    'password'          => Hash::make('12345678'),
                    'email'             =>'steve.jobs@apple.com',
                    'profile_id'        => 1,
                    'name'              => 'Steve',
                    'last_name'         => 'Jobs',
                    'second_lastname'   => '',
                    'active'            => 1,
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                ],
                */
           ]
        );
        \DB::statement('ALTER SEQUENCE public.users_id_seq RESTART WITH 25');
    }
}
