<?php

namespace Database\Seeders;

use App\Models\Catalogs\CatClasification;
use Illuminate\Database\Seeder;

class CatClasificationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatClasification::create([
            'id' => 1,
            'name' => 'Pública'
        ]);
        CatClasification::create([
            'id' => 2,
            'name' => 'Privada'
        ]);
    }
}
