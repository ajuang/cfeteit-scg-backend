<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Catalogs\CatTurnStatus;

class CatTurnStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatTurnStatus::create([
            'name' => 'Asignado',
        ]);
        CatTurnStatus::create([
            'name' => 'Turnado',
        ]);
        CatTurnStatus::create([
            'name' => 'Atendido',
        ]);
    }
}
