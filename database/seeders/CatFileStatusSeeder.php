<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CatFileStatus;

class CatFileStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('cat_file_statuses')->insert(
            [
                [
                    'id'     => 1,
                    'name'   => 'EN PROCESO',
                ],
                [
                    'id'     => 2,
                    'name'   => 'NOTIFICADO',
                ],
                [
                    'id'     => 3,
                    'name'   => 'TERMINADO',
                ],
            ]
    );
        \DB::statement('ALTER SEQUENCE cat_file_statuses_id_seq RESTART WITH 5');
    }
}
