<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CatSubjectType;

class CatSubjectTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('cat_subject_types')->insert(
            [
                [
                    'id'        => 1,
                    'unit_id'   => 10,
                    'subjectType'      => 'Confidencial',
                ]
            ]
            
        );
        \DB::statement('ALTER SEQUENCE cat_subject_types_id_seq RESTART WITH 2');
    }
}
