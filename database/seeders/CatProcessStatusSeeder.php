<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CatProcessStatus;

class CatProcessStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('cat_process_statuses')->insert(
           [     
                [
                    'id'     => 1,
                    'name'   => 'En Proceso',
                ],
                [
                    'id'     => 2,
                    'name'   => 'Finalizado',
                ]
           ]
        );
        \DB::statement('ALTER SEQUENCE cat_process_statuses_id_seq RESTART WITH 3');
    }
}
