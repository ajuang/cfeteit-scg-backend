<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AssistantStatus;

class AssistantStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('assistant_statuses')->insert(
            [
                [
                    'id'     => 1,
                    'name'   => 'Activo',
                    'unit_id'   => '1',
                ],
                [
                    'id'     => 2,
                    'name'   => 'Inactivo',
                    'unit_id'   => '2',
                ]   
            ]
        );
        \DB::statement('ALTER SEQUENCE assistant_statuses_id_seq RESTART WITH 3');
    }
}
