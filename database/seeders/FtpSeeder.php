<?php

namespace Database\Seeders;

use App\Models\Ftp;
use Illuminate\Database\Seeder;

class FtpSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Ftp::create([
        'id' => 1,
        'entity_id' => 18,
        'host' => 'ftp.rootheim.com',
        'username' => 'pruebainfotec@rootheim.com',
        'password' => 'prueba1234'
      ]);
    }
}
