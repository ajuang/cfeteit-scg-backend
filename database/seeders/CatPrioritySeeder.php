<?php

namespace Database\Seeders;

use App\Models\catalogs\CatPriority;
use Illuminate\Database\Seeder;

class CatPrioritySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatPriority::create([
            'id' => 1,
            'name' => 'Urgente',
            'days' => '1',
        ]);
        CatPriority::create([
            'id' => 2,
            'name' => 'Alta',
            'days' => '5',
        ]);
        CatPriority::create([
            'id' => 3,
            'name' => 'Baja',
            'days' => '7',
        ]);
        \DB::statement('ALTER SEQUENCE cat_priorities_id_seq RESTART WITH 4');
    }
}
