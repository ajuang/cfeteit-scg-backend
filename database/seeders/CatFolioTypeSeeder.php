<?php

namespace Database\Seeders;

use App\Models\Catalogs\CatFolioType;
use Illuminate\Database\Seeder;

class CatFolioTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatFolioType::create([
            'id' => 1,
            'name' => 'Consecutivo'
        ]);
        CatFolioType::create([
            'id' => 2,
            'name' => 'Sin folio'
        ]);
        CatFolioType::create([
            'id' => 3,
            'name' => 'Manual'
        ]);
    }
}
