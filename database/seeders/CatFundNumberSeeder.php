<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use app\Models\CatFundNumber;

class CatFundNumberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('cat_fund_numbers')->insert(
            [     
                 [
                     'id'       => 1,
                     'folio'    => 'FolioUno',
                 ],
                 [
                     'id'       => 2,
                     'folio'    => 'FolioDos',
                 ]
            ]
         );
         \DB::statement('ALTER SEQUENCE cat_fund_numbers_id_seq RESTART WITH 3');
    }
}
