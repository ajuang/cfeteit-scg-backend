<?php

namespace Database\Seeders;

use App\Models\Catalogs\CatStep;
use Illuminate\Database\Seeder;

class CatStepSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatStep::create([
            'id' => 1,
            'name' => 'Paso 1 (1)'
        ]);
        CatStep::create([
            'id' => 2,
            'name' => 'Paso 2 (2)'
        ]);
        CatStep::create([
            'id' => 3,
            'name' => 'Paso 3 (3)'
        ]);
    }
}
