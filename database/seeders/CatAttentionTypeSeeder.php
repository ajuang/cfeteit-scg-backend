<?php

namespace Database\Seeders;

use App\Models\Catalogs\CatAttentionType;
use Illuminate\Database\Seeder;

class CatAttentionTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatAttentionType::create([
            'id' => 1,
            'name' => 'Ejecución'
        ]);
        CatAttentionType::create([
            'id' => 2,
            'name' => 'Informativa'
        ]);
    }
}
