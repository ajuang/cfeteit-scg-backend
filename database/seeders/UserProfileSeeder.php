<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserProfile;

class UserProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserProfile::create([
            'id'     => 1,
            'key'    => 'administrator',
            'name'   => 'Administrador',
        ]);
        UserProfile::create([
            'id'     => 2,
            'key'    => 'assistant',
            'name'   => 'Asistente',
        ]);
        UserProfile::create([
            'id'     => 3,
            'key'    => 'attention',
            'name'   => 'Atención',
        ]);
        UserProfile::create([
            'id'     => 4,
            'key'    => 'reception',
            'name'   => 'Recepción',
        ]);
        UserProfile::create([
            'id'     => 5,
            'key'    => 'turnado',
            'name'   => 'Turnado',
        ]);
        \DB::statement('ALTER SEQUENCE user_profiles_id_seq RESTART WITH 6');
    }
}
