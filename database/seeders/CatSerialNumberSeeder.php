<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use app\Models\CatSerialNumber;

class CatSerialNumberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('cat_serial_numbers')->insert(
            [     
                 [
                     'id'     => 1,
                     'serial'   => 'Serie1',
                 ],
                 [
                     'id'     => 2,
                     'serial'   => 'Serie2',
                 ]
            ]
         );
        \DB::statement('ALTER SEQUENCE cat_serial_numbers_id_seq RESTART WITH 3');
    }
}
