<?php

namespace Database\Seeders;

use App\Models\Catalogs\CatStepType;
use Illuminate\Database\Seeder;

class CatStepTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatStepType::create([
            'id' => 1,
            'name' => 'Interno'
        ]);
        CatStepType::create([
            'id' => 2,
            'name' => 'Externo'
        ]);
    }
}
