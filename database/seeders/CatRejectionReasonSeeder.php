<?php

namespace Database\Seeders;

use App\Models\Catalogs\CatRejectionReason;
use Illuminate\Database\Seeder;

class CatRejectionReasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatRejectionReason::create([
            'description'           => 'Mala Ortografía',
            'cat_unit_id' => '1'
        ]);
        CatRejectionReason::create([
            'description'            =>  "Asunto Equivocado",
            'cat_unit_id'  =>  "1"
        ]);
    }
}
