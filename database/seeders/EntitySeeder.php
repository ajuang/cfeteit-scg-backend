<?php

namespace Database\Seeders;

use App\Models\Entity;
use Illuminate\Database\Seeder;

class EntitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Entity::create([
            'id' => 1,
            'name' => 'Centro de Investigación e Innovación en Tecnologías de la Información y Comunicación',
            'acronym' => 'INFOTEC'
        ]);
        Entity::create([
            'id' => 2,
            'name' => 'Consejo Nacional de Ciencia y Tecnología',
            'acronym' => 'Conacyt'
        ]);
        Entity::create([
            'id' => 3,
            'name' => 'Servicio de Administración Tributaria',
            'acronym' => 'SAT'
        ]);
        Entity::create([
            'id' => 4,
            'name' => 'Secretará de Hacienda y Crédito Público',
            'acronym' => 'SHCP'
        ]);
        Entity::create([
            'id' => 5,
            'name' => 'Instituto Nacional de Estadística y Geografía',
            'acronym' => 'INEGI'
        ]);
        Entity::create([
            'id' => 6,
            'name' => 'Secretaría de Gobernación',
            'acronym' => 'Segob'
        ]);
        Entity::create([
            'id' => 7,
            'name' => 'Secretaría de Relaciones Exteriores',
            'acronym' => 'SER'
        ]);
        Entity::create([
            'id' => 8,
            'name' => 'Secretaría de Economía',
            'acronym' => 'SE'
        ]);
        Entity::create([
            'id' => 9,
            'name' => 'Secretaría de Medio Ambiente',
            'acronym' => 'SEDEMA'
        ]);
        Entity::create([
            'id' => 10,
            'name' => 'Secretaría de Energía',
            'acronym' => 'SENER'
        ]);
        Entity::create([
            'id' => 11,
            'name' => 'Secretaría de la Función Pública',
            'acronym' => 'SFP'
        ]);
        Entity::create([
            'id' => 12,
            'name' => 'Organo Interno de Control',
            'acronym' => 'OIC'
        ]);
        Entity::create([
            'id' => 13,
            'name' => 'Guardia Nacional',
            'acronym' => 'GN'
        ]);
        Entity::create([
            'id' => 14,
            'name' => 'Auditoría Superior de la Federación',
            'acronym' => 'ASF'
        ]);
        Entity::create([
            'id' => 15,
            'name' => 'Instituto Nacional de Migración',
            'acronym' => 'INM'
        ]);
        Entity::create([
            'id' => 16,
            'name' => 'Amazon',
            'acronym' => 'amazon'
        ]);
        Entity::create([
            'id' => 17,
            'name' => 'Huawei',
            'acronym' => 'HUAWEI'
        ]);
        Entity::create([
            'id' => 18,
            'name' => 'Google',
            'acronym' => 'Google'
        ]);
        Entity::create([
            'id' => 19,
            'name' => 'Alianza FiiDEM',
            'acronym' => 'Alianza FiiDEM'
        ]);
        \DB::statement('ALTER SEQUENCE entities_id_seq RESTART WITH 20');
    }
}
