<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\CatTurnMovement;

class CatTurnMovementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatTurnMovement::create([
            'name' => 'Turnación',
        ]);
        CatTurnMovement::create([
            'name' => 'Documento leído',
        ]);
        CatTurnMovement::create([
            'name' => 'Modifico Documento',
        ]);
        CatTurnMovement::create([
            'name' => 'Adjunto archivo',
        ]);
        CatTurnMovement::create([
            'name' => 'Cierre de turno',
        ]);
    }
}
