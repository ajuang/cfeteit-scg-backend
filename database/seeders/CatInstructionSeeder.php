<?php

namespace Database\Seeders;

use App\Models\Catalogs\CatInstruction;
use Illuminate\Database\Seeder;

class CatInstructionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatInstruction::create([
            'id' => 1,
            'name' => 'Atender Solicitud',
            'unit_id' => 1
        ]);
        CatInstruction::create([
            'id' => 2,
            'name' => 'Atender a la Brevedad',
            'unit_id' => 2
        ]);
        CatInstruction::create([
            'id' => 3,
            'name' => 'Conocimiento',
            'unit_id' => 3
        ]);
        CatInstruction::create([
            'id' => 4,
            'name' => 'Solicitud de Información',
            'unit_id' => 4
        ]);
        \DB::statement('ALTER SEQUENCE cat_instructions_id_seq RESTART WITH 5');
    }
}
