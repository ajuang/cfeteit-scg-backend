<?php

namespace Database\Seeders;

use App\Models\Catalogs\CatDocType;
use Illuminate\Database\Seeder;

class CatDocTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatDocType::create([
            'id' => 1,
            'name' => 'Oficio',
            'unit_id' => 1
        ]);
        CatDocType::create([
            'id' => 2,
            'name' => 'Memorándum',
            'unit_id' => 2
        ]);
        CatDocType::create([
            'id' => 3,
            'name' => 'Atenta nota',
            'unit_id' => 3
        ]);
        CatDocType::create([
            'id' => 4,
            'name' => 'Volante',
            'unit_id' => 1
        ]);
        CatDocType::create([
            'id' => 5,
            'name' => 'Otro',
            'unit_id' => 1
        ]);
        \DB::statement('ALTER SEQUENCE cat_doc_types_id_seq RESTART WITH 6');
    }
}
