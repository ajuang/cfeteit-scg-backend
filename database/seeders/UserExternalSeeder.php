<?php

namespace Database\Seeders;

use App\Models\UserExternal;
use Illuminate\Database\Seeder;

class UserExternalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserExternal::create([
            'id' => 1,
            'name' => 'Juan Antonio',
            'lastname' => 'Ruíz',
            'second_lastname' => 'González',
            'unit_name' => 'Gerencia tecnológica',
            'position_name' => 'Gerente tecnológico',
            'email' => 'juan.ruiz@outlook.com',
            'phone' => '5564543467',
            'entity_id' => 2,
        ]);
    }
}
