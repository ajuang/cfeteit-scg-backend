<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Catalogs\CatUnit;

class CatUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatUnit::create([
            'id' => 1,
            'entity_id' => 1,
            'name' =>'Dirección Ejecutiva',
            'code' => '10-001-00',
            'nomenclature' => 'Oficio Núm.DE-XXX/2021',
            'acronym' =>'DE',

        ]);
        CatUnit::create([
            'id' => 2,
            'entity_id' => 1,
            'name' =>'Dirección Adjunta de Administración',
            'code' => '20-002-00',
            'nomenclature' => 'INFOTEC-DAA-XXX/2021',
            'acronym' =>'DAA',
        ]);
        CatUnit::create([
            'id' => 3,
            'entity_id' => 1,
            'name' =>'Dirección Adjunta de Innovación y Conocimiento',
            'code' => '30-003-00',
            'nomenclature' => 'INFOTEC-DAIC-XXX/2021',
            'acronym' =>'DAIC',
        ]);
        CatUnit::create([
            'id' => 4,
            'entity_id' => 1,
            'name' =>'Gerencia de Capital Humano',
            'code' => '31-004-00',
            'nomenclature' => 'INFOTEC-DAIC-GCH-XXX/2021',
            'acronym' =>'GCH',
        ]);
        CatUnit::create([
            'id' => 5,
            'entity_id' => 1,
            'name' =>'Gerencia de Innovación',
            'code' => '34-007-00',
            'nomenclature' => 'INFOTEC-DAIC-GI-XXX/2021',
            'acronym' =>'GI',
        ]);
        CatUnit::create([
            'id' => 6,
            'entity_id' => 1,
            'name' =>'Gerencia de Investigación',
            'code' => '36-006-00',
            'nomenclature' => '',
            'acronym' =>'',
        ]);
        CatUnit::create([
            'id' => 7,
            'entity_id' => 1,
            'name' =>'Dirección Adjunta de Desarrollo Tecnológico',
            'code' => '40-008-00',
            'nomenclature' => 'INFOTEC-DADT-XXX/2021',
            'acronym' =>'DADT',
        ]);
        CatUnit::create([
            'id' => 8,
            'entity_id' => 1,
            'name' => 'Gerencia de Administración Integral de Infraestructura',
            'code' => '42-010-00',
            'nomenclature' => 'INFOTEC-DADT-GAII-XXX/2021',
            'acronym' =>'GAII',
        ]);
        CatUnit::create([
            'id' => 9,
            'entity_id' => 1,
            'name' => 'Gerencia de Sistemas de Información Estratégicos',
            'code' => '43-011-00',
            'nomenclature' => 'INFOTEC-DADT-GSIE-XXX/2021',
            'acronym' =>'GSIE',
        ]);
        CatUnit::create([
            'id' => 10,
            'entity_id' => 1,
            'name' => 'Gerencia de Incubación Tecnológica y de Negocio',
            'code' => '44-016-00',
            'nomenclature' => '',
            'acronym' =>'',
        ]);
        CatUnit::create([
            'id' => 11,
            'entity_id' => 1,
            'name' => 'Dirección Adjunta de Administración de Proyectos',
            'code' => '50-012-00',
            'nomenclature' => 'INFOTEC-DAC-GITN-XXX/2021',
            'acronym' =>'GITN',
        ]);
        CatUnit::create([
            'id' => 12,
            'entity_id' => 1,
            'name' => 'Órgano Interno de Control',
            'code' => '60-013-00',
            'nomenclature' => '',
            'acronym' =>'',
        ]);
        CatUnit::create([
            'id' => 13,
            'entity_id' => 1,
            'name' => 'Dirección Adjunta de Competitividad',
            'code' => '70-014-00',
            'nomenclature' => 'INFOTEC-DAC-XXX/2021',
            'acronym' =>'DAC',
        ]);
        CatUnit::create([
            'id' => 14,
            'entity_id' => 1,
            'name' => 'Gerencia de Consultoría Estratégica',
            'code' => '72-017-00',
            'nomenclature' => 'INFOTEC-DAC-GCE-XXX/2021',
            'acronym' =>'GCE',
        ]);
        CatUnit::create([
            'id' => 15,
            'entity_id' => 1,
            'name' => 'Ventanilla Única Nacional',
            'code' => '75-2054-01',
            'nomenclature' => '',
            'acronym' =>'',
        ]);
        CatUnit::create([
            'id' => 16,
            'entity_id' => 1,
            'name' => 'Dirección Adjunta de Desarrollo de Software',
            'code' => '80-021-00',
            'nomenclature' => '',
            'acronym' =>'',
        ]);
        CatUnit::create([
            'id' => 17,
            'entity_id' => 1,
            'name' => 'Gerencia de Nuevos Productos',
            'code' => '82-024-00',
            'nomenclature' => '',
            'acronym' =>'',
        ]);
        CatUnit::create([
            'id' => 18,
            'entity_id' => 1,
            'name' => 'Servicios en Sitio',
            'code' => '75-023-00',
            'nomenclature' => '',
            'acronym' =>'',
        ]);
        CatUnit::create([
            'id' => 19,
            'entity_id' => 1,
            'name' =>'Gerencia de Asuntos Jurídicos',
            'code' => '24-028-00',
            'nomenclature' => 'INFOTEC-DAA-GAJ-XXX/2021',
            'acronym' =>'GAJ',
        ]);
        CatUnit::create([
            'id' => 20,
            'entity_id' => 1,
            'name' =>'Dirección Adjunta de Asuntos Jurídicos',
            'code' => '90-032-00',
            'nomenclature' => 'INFOTEC-DAAJ-XXX/2021',
            'acronym' =>'DAAJ',
        ]);
        CatUnit::create([
            'id' => 21,
            'entity_id' => 1,
            'name' =>'Gerencia de Coordinación de Estrategia Institucional',
            'code' => '',
            'nomenclature' => 'INFOTEC-GCEI-XXX/2021',
            'acronym' =>'DCEI',
        ]);
        CatUnit::create([
            'id' => 22,
            'entity_id' => 1,
            'name' =>'Gerencia de Evaluación de Proyectos',
            'code' => '',
            'nomenclature' => 'INFOTEC-DAAP-GEP-XXX/2021',
            'acronym' =>'GEP',
        ]);
        CatUnit::create([
            'id' => 23,
            'entity_id' => 1,
            'name' =>'Gerencia de Incubación y Tecnología de Negocios',
            'code' => '',
            'nomenclature' => 'INFOTEC-DAC-GITN-XXX/2021',
            'acronym' =>'GITN',
        ]);
        CatUnit::create([
            'id' => 24,
            'entity_id' => 1,
            'name' =>'Gerencia de Innovación',
            'code' => '',
            'nomenclature' => 'INFOTEC-DAIC-GI-XXX/2021',
            'acronym' =>'GI',
        ]);
        CatUnit::create([
            'id' => 25,
            'entity_id' => 1,
            'name' =>'Subgerencia de Administración de Proyectos',
            'code' => '',
            'nomenclature' => 'INFOTEC-DAAP-GEP-SAP-XXX/2021',
            'acronym' =>'SAP',
        ]);
        CatUnit::create([
            'id' => 26,
            'entity_id' => 1,
            'name' =>'Subgerencia de Asuntos Consultivos',
            'code' => '',
            'nomenclature' => 'INFOTEC-DAA-GAJ-SAC-XXX/2021',
            'acronym' =>'SAC',
        ]);
        CatUnit::create([
            'id' => 27,
            'entity_id' => 1,
            'name' =>'Subgerencia de Centro de Datos y Mesa de Servicios',
            'code' => '',
            'nomenclature' => 'INFOTEC-DADT-GAII-SCDMS-XXX/2021',
            'acronym' =>'SCDMS',
        ]);
        CatUnit::create([
            'id' => 28,
            'entity_id' => 1,
            'name' =>'Subgerencia de Desarrollo',
            'code' => '',
            'nomenclature' => 'INFOTEC-DADT-GSIE-SD-XXX/2021',
            'acronym' =>'SD',
        ]);
        CatUnit::create([
            'id' => 29,
            'entity_id' => 1,
            'name' =>'Subgerencia de Desarrollo Organizacional',
            'code' => '',
            'nomenclature' => 'INFOTEC-DAC-GCE-SDO-XXX/2021',
            'acronym' =>'SDO',
        ]);
        CatUnit::create([
            'id' => 30,
            'entity_id' => 1,
            'name' =>'Subgerencia de Docencia',
            'code' => '',
            'nomenclature' => 'INFOTEC-DAIC-GCH-SD-XXX/2021',
            'acronym' =>'SD',
        ]);
        CatUnit::create([
            'id' => 31,
            'entity_id' => 1,
            'name' =>'Subgerencia de Incubación de Negocios',
            'code' => '',
            'nomenclature' => 'INFOTEC-DAC-GITN-SIN-XXX/2021',
            'acronym' =>'SIN',
        ]);
        CatUnit::create([
            'id' => 32,
            'entity_id' => 1,
            'name' =>'Subgerencia de Incubación Tecnológica',
            'code' => '',
            'nomenclature' => 'INFOTEC-DAC-GITN-SIT-XXX/2021',
            'acronym' =>'SIT',
        ]);
        CatUnit::create([
            'id' => 33,
            'entity_id' => 1,
            'name' =>'Subgerencia de Innovación Gubernamental',
            'code' => '',
            'nomenclature' => 'INFOTEC-DAIC-GI-SIG-XXX/2021',
            'acronym' =>'SIG',
        ]);
        CatUnit::create([
            'id' => 34,
            'entity_id' => 1,
            'name' =>'Subgerencia de Innovación Tecnológica',
            'code' => '',
            'nomenclature' => 'INFOTEC-DADT-GSIE-SIT-XXX/2021',
            'acronym' =>'SIT',
        ]);
        CatUnit::create([
            'id' => 35,
            'entity_id' => 1,
            'name' =>'Subgerencia de Operación',
            'code' => '',
            'nomenclature' => 'INFOTEC-DAA-SO-XXX/2021',
            'acronym' =>'SO',
        ]);
        CatUnit::create([
            'id' => 36,
            'entity_id' => 1,
            'name' =>'Subgerencia de Recursos Financieros',
            'code' => '',
            'nomenclature' => 'INFOTEC-DAA-SRF-XXX/2021',
            'acronym' =>'SRF',
        ]);
        CatUnit::create([
            'id' => 37,
            'entity_id' => 1,
            'name' =>'Subgerencia de Recursos Humanos',
            'code' => '',
            'nomenclature' => 'INFOTEC-DAA-SRH-XXX/2021',
            'acronym' =>'SRH',
        ]);
        CatUnit::create([
            'id' => 38,
            'entity_id' => 1,
            'name' =>'Subgerencia de Seguridad Informática',
            'code' => '',
            'nomenclature' => 'INFOTEC-DADT-GAII-SSI-XXX/2021',
            'acronym' =>'SSI',
        ]);
        CatUnit::create([
            'id' => 39,
            'entity_id' => 1,
            'name' =>'Subgerencia de Telecomunicaciones',
            'code' => '',
            'nomenclature' => 'INFOTEC-DADT-ST-XXX/2021',
            'acronym' =>'ST',
        ]);
        CatUnit::create([
            'id' => 40,
            'entity_id' => 1,
            'name' =>'Por asignar',
            'code' => '',
            'nomenclature' => 'INFOTEC-DADT-ST-XXX/2021',
            'acronym' =>'AP',
        ]);
        // Units belogs to another entities to user registration test
        CatUnit::create([
            'id' => 41,
            'entity_id' => 18,
            'name' =>'Dirección de actividades académicas',
            'code' => '',
            'nomenclature' => 'GOOGLE-DAE-ST-XXX/2021',
            'acronym' =>'DAE',
        ]);
        \DB::statement('ALTER SEQUENCE cat_units_id_seq RESTART WITH 42');
    }
}
