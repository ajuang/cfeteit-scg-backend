<?php

namespace Database\Seeders;

use App\Models\ParticipationType;
use Illuminate\Database\Seeder;

class ParticipationTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ParticipationType::create([
            'id' => 1,
            'name' => 'From',
        ]);
        ParticipationType::create([
            'id' => 2,
            'name' => 'Ccp',
        ]);
    }
}
