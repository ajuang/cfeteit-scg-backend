<?php

namespace Database\Seeders\Catalogs;

use Illuminate\Database\Seeder;
use App\Models\Catalogs\CatSenderType;

class CatSenderTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatSenderType::create([
            'id' => 1,
            'name' => 'Empresa'
        ]);

        CatSenderType::create([
            'id' => 2,
            'name' => 'Persona'
        ]);

        CatSenderType::create([
            'id' => 3,
            'name' => 'Dependencia'
        ]);
    }
}
