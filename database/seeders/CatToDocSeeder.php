<?php

namespace Database\Seeders;

use App\Models\Catalogs\CatToDoc;
use Illuminate\Database\Seeder;

class CatToDocSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatToDoc::create([
            'id' => 1,
            'name' => 'Entrada - enviar el asunto (instrucciones) para atender en mi oficina'
        ]);
        CatToDoc::create([
            'id' => 2,
            'name' => 'Salida - enviar el asunto (instrucciones) para atender en otra oficina'
        ]);
    }
}
