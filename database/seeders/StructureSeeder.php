<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class StructureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('structures')->insert(
            [
                [
                    'id' => 1,
                    'entity_id' => 1,
                    'position_id' => 1,
                    'position_to_report_id' => null
                ],
                [
                    'id' => 2,
                    'entity_id' => 1,
                    'position_id' => 2,
                    'position_to_report_id' => 1
                ],
                [
                    'id' => 3,
                    'entity_id' => 1,
                    'position_id' => 3,
                    'position_to_report_id' => 1
                ],
                [
                    'id' => 4,
                    'entity_id' => 1,
                    'position_id' => 4,
                    'position_to_report_id' => 1                ],
                [
                    'id' => 5,
                    'entity_id' => 1,
                    'position_id' => 5,
                    'position_to_report_id' => 4
                ],
                [
                    'id' => 6,
                    'entity_id' => 1,
                    'position_id' => 6,
                    'position_to_report_id' => 5
                ],
                [
                    'id' => 7,
                    'entity_id' => 1,
                    'position_id' => 7,
                    'position_to_report_id' => 4
                ],
                [
                    'id' => 8,
                    'entity_id' => 1,
                    'position_id' => 8,
                    'position_to_report_id' => 4
                ],
                [
                    'id' => 9,
                    'entity_id' => 1,
                    'position_id' => 9,
                    'position_to_report_id' => 4
                ],
                [
                    'id' => 10,
                    'entity_id' => 1,
                    'position_id' => 10,
                    'position_to_report_id' => 1
                ],
                [
                    'id' => 11,
                    'entity_id' => 1,
                    'position_id' => 11,
                    'position_to_report_id' => 10
                ],
                [
                    'id' => 12,
                    'entity_id' => 1,
                    'position_id' => 12,
                    'position_to_report_id' => 11
                ],
                [
                    'id' => 13,
                    'entity_id' => 1,
                    'position_id' => 13,
                    'position_to_report_id' => 11
                ],
                [
                    'id' => 14,
                    'entity_id' => 1,
                    'position_id' => 14,
                    'position_to_report_id' => 10
                ],
                [
                    'id' => 15,
                    'entity_id' => 1,
                    'position_id' => 15,
                    'position_to_report_id' => 10
                ],
                [
                    'id' => 16,
                    'entity_id' => 1,
                    'position_id' => 16,
                    'position_to_report_id' => 14
                ],
                [
                    'id' => 17,
                    'entity_id' => 1,
                    'position_id' => 17,
                    'position_to_report_id' => 14
                ],
                [
                    'id' => 18,
                    'entity_id' => 1,
                    'position_id' => 18,
                    'position_to_report_id' => 1
                ],
                [
                    'id' => 19,
                    'entity_id' => 1,
                    'position_id' => 19,
                    'position_to_report_id' => 18
                ],
                [
                    'id' => 20,
                    'entity_id' => 1,
                    'position_id' => 20,
                    'position_to_report_id' => 19
                ],
                [
                    'id' => 21,
                    'entity_id' => 1,
                    'position_id' => 21,
                    'position_to_report_id' => 18
                ],
                [
                    'id' => 22,
                    'entity_id' => 1,
                    'position_id' => 22,
                    'position_to_report_id' => 21
                ],
                [
                    'id' => 23,
                    'entity_id' => 1,
                    'position_id' => 23,
                    'position_to_report_id' => 21
                ],
                [
                    'id' => 24,
                    'entity_id' => 1,
                    'position_id' => 24,
                    'position_to_report_id' => 1
                ],
                [
                    'id' => 25,
                    'entity_id' => 1,
                    'position_id' => 25,
                    'position_to_report_id' => 24
                ],
                [
                    'id' => 26,
                    'entity_id' => 1,
                    'position_id' => 26,
                    'position_to_report_id' => 25
                ],
                [
                    'id' => 27,
                    'entity_id' => 1,
                    'position_id' => 27,
                    'position_to_report_id' => 24
                ],
                [
                    'id' => 28,
                    'entity_id' => 1,
                    'position_id' => 28,
                    'position_to_report_id' => 27
                ],
                [
                    'id' => 29,
                    'entity_id' => 1,
                    'position_id' => 29,
                    'position_to_report_id' => 1
                ],
                [
                    'id' => 30,
                    'entity_id' => 1,
                    'position_id' => 30,
                    'position_to_report_id' => 29
                ],
                [
                    'id' => 31,
                    'entity_id' => 1,
                    'position_id' => 31,
                    'position_to_report_id' => 30
                ],
                // External users to registration test
                [
                    'id' => 32,
                    'entity_id' => 18,
                    'position_id' => 1,
                    'position_to_report_id' => null
                ],
                [
                    'id' => 33,
                    'entity_id' => 18,
                    'position_id' => 2,
                    'position_to_report_id' => 1
                ],
                [
                    'id' => 34,
                    'entity_id' => 18,
                    'position_id' => 3,
                    'position_to_report_id' => 1
                ],
            ]
        );
        \DB::statement('ALTER SEQUENCE structures_id_seq RESTART WITH 35');
    }
}
