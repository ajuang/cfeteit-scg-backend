<?php

namespace Database\Seeders;

use App\Models\catalogs\CatRecepMed;
use Illuminate\Database\Seeder;

class CatRecepMedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatRecepMed::create([
            'id' => 1,
            'name' => 'Fisico/Escrito'
        ]);
        CatRecepMed::create([
            'id' => 2,
            'name' => 'Correo'
        ]);
        CatRecepMed::create([
            'id' => 3,
            'name' => 'Paquete/Sobre'
        ]);
    }
}
