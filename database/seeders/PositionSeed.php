<?php

namespace Database\Seeders;

use App\Models\Position;
use Illuminate\Database\Seeder;

class PositionSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('positions')->insert(
            [
                [
                    'id' => 1,
                    'name' => 'Director Ejecutivo',
                    'cat_unit_id' => 1
                ],
                [
                    'id' => 2,
                    'name' => 'Director Adjunto de Asuntos Jurídicos',
                    'cat_unit_id' => 2
                ],
                [
                    'id' => 3,
                    'name' => 'Gerente de Coordinación de Estrategia Institucional',
                    'cat_unit_id' => 3
                ],
                [
                    'id' => 4,
                    'name' => 'Director Adjunto de Administración',
                    'cat_unit_id' => 4
                ],
                [
                    'id' => 5,
                    'name' => 'Gerente de Asuntos Jurídicos',
                    'cat_unit_id' => 5
                ],
                [
                    'id' => 6,
                    'name' => 'Subgerente de Asuntos Consultivos',
                    'cat_unit_id' => 6
                ],
                [
                    'id' => 7,
                    'name' => 'Subgerente de Operación',
                    'cat_unit_id' => 7
                ],
                [
                    'id' => 8,
                    'name' => 'Subgerente de Recursos Financieros',
                    'cat_unit_id' => 8
                ],
                [
                    'id' => 9,
                    'name' => 'Subgerente de Recursos Humanos',
                    'cat_unit_id' => 9
                ],
                [
                    'id' => 10,
                    'name' => 'Director Adjunto de Desarrollo Tecnológico',
                    'cat_unit_id' => 10
                ],
                [
                    'id' => 11,
                    'name' => 'Gerente de Sistemas de Información Estratégicos',
                    'cat_unit_id' => 11
                ],
                [
                    'id' => 12,
                    'name' => 'Subgerente de Innovación Tecnológica',
                    'cat_unit_id' => 12
                ],
                [
                    'id' => 13,
                    'name' => 'Subgerente de Desarrollo',
                    'cat_unit_id' => 13
                ],
                [
                    'id' => 14,
                    'name' => 'Gerente de Administración Integral de Infraestructura',
                    'cat_unit_id' => 14
                ],
                [
                    'id' => 15,
                    'name' => 'Subgerente de Telecomunicaciones',
                    'cat_unit_id' => 15
                ],
                [
                    'id' => 16,
                    'name' => 'Subgerente de Centro de Datos y Mesa de Servicios',
                    'cat_unit_id' => 16
                ],
                [
                    'id' => 17,
                    'name' => 'Subgerente de Seguridad Informática',
                    'cat_unit_id' => 17
                ],
                [
                    'id' => 18,
                    'name' => 'Director Adjunto de Competitividad',
                    'cat_unit_id' => 18
                ],
                [
                    'id' => 19,
                    'name' => 'Gerente de Consultoría Estratégica',
                    'cat_unit_id' => 19
                ],
                [
                    'id' => 20,
                    'name' => 'Subgerente de Desarrollo Organizacional',
                    'cat_unit_id' => 20
                ],
                [
                    'id' => 21,
                    'name' => 'Gerente de Incubación y Tecnología de Negocios',
                    'cat_unit_id' => 21
                ],
                [
                    'id' => 22,
                    'name' => 'Subgerente de Incubación de Negocios',
                    'cat_unit_id' => 22
                ],
                [
                    'id' => 23,
                    'name' => 'Subgerente de Incubación Tecnológica',
                    'cat_unit_id' => 23
                ],
                [
                    'id' => 24,
                    'name' => 'Director Adjunta de Innovación y Conocimiento',
                    'cat_unit_id' => 24
                ],
                [
                    'id' => 25,
                    'name' => 'Gerente de Innovación',
                    'cat_unit_id' => 25
                ],
                [
                    'id' => 26,
                    'name' => 'Subgerente de Innovación Gubernamental',
                    'cat_unit_id' => 26
                ],
                [
                    'id' => 27,
                    'name' => 'Gerente de Capital Humano',
                    'cat_unit_id' => 27
                ],
                [
                    'id' => 28,
                    'name' => 'Subgerente de Docencia',
                    'cat_unit_id' => 28
                ],
                [
                    'id' => 29,
                    'name' => 'Director Adjunto de Administración de Proyectos',
                    'cat_unit_id' => 29
                ],
                [
                    'id' => 30,
                    'name' => 'Gerente de Evaluación de Proyectos',
                    'cat_unit_id' => 30
                ],
                [
                    'id' => 31,
                    'name' => 'Subgerente de Administración de Proyectos',
                    'cat_unit_id' => 31
                ],
                [
                    'id' => 32,
                    'name' => 'Por asignar',
                    'cat_unit_id' => 40
                ],
                // Positions belogs to unit that belongs to another entities to user registration test
                [
                    'id' => 33,
                    'name' => 'Director de actividades académicas',
                    'cat_unit_id' => 41
                ],
            ]
            
        );
        \DB::statement('ALTER SEQUENCE positions_id_seq RESTART WITH 34');
        // Position::factory()->count(env('EMPLOYEE_SEEDER_COUNT',5))->create();
    }
}
