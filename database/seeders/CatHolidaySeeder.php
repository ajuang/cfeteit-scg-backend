<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Catalogs\CatHoliday;

class CatHolidaySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CatHoliday::create([
            'id' => 1,
            'holiday_date' => '2021-01-01',
            'description' =>'Año nuevo'
        ]);
        CatHoliday::create([
            'id' => 2,
            'holiday_date' => '2021-12-25',
            'description' =>'Navidad'
        ]);
        \DB::statement('ALTER SEQUENCE cat_holidays_id_seq RESTART WITH 3');
    }
}
