<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StructureUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('structure_users')->insert(
            [
                [
                    'id' => 1,
                    'structure_id' => 1,
                    'people_id' => 1,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 2,
                    'structure_id' => 2,
                    'people_id' => 2,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 3,
                    'structure_id' => 4,
                    'people_id' => 3,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 4,
                    'structure_id' => 5,
                    'people_id' => 4,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 5,
                    'structure_id' => 6,
                    'people_id' => 5,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 6,
                    'structure_id' => 7,
                    'people_id' => 6,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 7,
                    'structure_id' => 9,
                    'people_id' => 7,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 8,
                    'structure_id' => 10,
                    'people_id' => 8,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 9,
                    'structure_id' => 12,
                    'people_id' => 9,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 10,
                    'structure_id' => 16,
                    'people_id' => 10,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 11,
                    'structure_id' => 18,
                    'people_id' => 11,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 12,
                    'structure_id' => 19,
                    'people_id' => 12,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 13,
                    'structure_id' => 20,
                    'people_id' => 13,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 14,
                    'structure_id' => 22,
                    'people_id' => 14,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 15,
                    'structure_id' => 24,
                    'people_id' => 15,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 16,
                    'structure_id' => 26,
                    'people_id' => 16,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 17,
                    'structure_id' => 27,
                    'people_id' => 17,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 18,
                    'structure_id' => 28,
                    'people_id' => 18,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 19,
                    'structure_id' => 29,
                    'people_id' => 19,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 20,
                    'structure_id' => 30,
                    'people_id' => 20,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 21,
                    'structure_id' => 31,
                    'people_id' => 21,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                // External users to registration test
                [
                    'id' => 22,
                    'structure_id' => 32,
                    'people_id' => 22,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 23,
                    'structure_id' => 33,
                    'people_id' => 23,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],
                [
                    'id' => 24,
                    'structure_id' => 34,
                    'people_id' => 24,
                    'is_active' => true,
                    'active_date' => Carbon::now()
                    // 'active_date' => date("Y-m-d H:i:s")
                ],

            ]
        );
        \DB::statement('ALTER SEQUENCE structure_users_id_seq RESTART WITH 25');
    }
}
