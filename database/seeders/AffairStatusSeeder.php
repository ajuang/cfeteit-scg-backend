<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\AffairStatus;

class AffairStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AffairStatus::create([
            'name' => 'Guardado',
        ]);
        AffairStatus::create([
            'name' => 'Colaborativo',
        ]);
        AffairStatus::create([
            'name' => 'Enviado',
        ]);
        AffairStatus::create([
            'name' => 'Leído',
        ]);
        AffairStatus::create([
            'name' => 'Respondido',
        ]);
        AffairStatus::create([
            'name' => 'Rechazado',
        ]);
    }
}
