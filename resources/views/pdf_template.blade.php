<html>
    <head>
        <style>
            @page {
                margin: 0cm 0cm;
                font-family: "Montserrat";
            }

            @font-face {
                font-family: "Montserrat";
                font-style: normal;
                font-weight: normal;
                src: url('{{storage_path()}}/fonts/Montserrat/Montserrat-Regular.ttf') format('truetype');
            }

            @font-face {
                font-family: "Montserrat";
                font-style: normal;
                font-weight: bold;
                src: url('{{storage_path()}}/fonts/Montserrat/Montserrat-Bold.ttf') format('truetype');

            }

            @font-face {
                font-family: "Montserrat";
                src: url('{{storage_path()}}/fonts/Montserrat/Montserrat-Italic.ttf') format('truetype');
                font-style: italic;
            }

            @font-face {
                font-family: "Montserrat";
                src: url('{{storage_path()}}/fonts/Montserrat/Montserrat-BoldItalic.ttf') format('truetype');
                font-weight: bold;
                font-style: italic;
            }

            body {
                margin: 4.5cm 2cm 4.5cm;
                font-family: 'Montserrat';
            }
            header {
                position: fixed;
                top: 1.5cm;
                left: 1.5cm;
                right: 0cm;
                height: 2cm;
                text-align: left;
                line-height: 30px;
                margin: 0cm 1cm 1cm 0.3cm;
                font-family: 'Montserrat';
            }
            footer {
                position: fixed;
                bottom: 0cm;
                left: 0cm;
                right: 0cm;
                height: 4.5cm;
                color: white;
                text-align: center;
                line-height: 35px;
                font-family: 'Montserrat';
            }
        </style>
    </head>
    <body>
        <header>
            <img src="{{ $path_img_header }}" width="550" alt="" />
        </header>
        <footer>
            <img src="{{ $path_img_footer }}" alt="" width="750"/>
        </footer>
        <main>
        <div>
            {!! $header !!}
        </div>
        <br>
        <div style="font-family: 'Montserrat' !important">
            {!! $data !!}
        </div>
        </main>
    </body>
</html>