<?php

use App\Http\Controllers\API\Admin\StructureUserController;
use App\Http\Controllers\API\IsssteDeductionsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\Admin\UserController;
use App\Http\Controllers\API\Admin\PeopleController;
use App\Http\Controllers\API\Admin\ExternalPeopleController;
use App\Http\Controllers\API\Admin\ExternalCompanyController;
use App\Http\Controllers\API\Admin\ExternalSenderController;
use App\Http\Controllers\API\Admin\CatalogsController;
use App\Http\Controllers\API\Admin\Catalogs\DocumentController;
use App\Http\Controllers\API\Admin\Catalogs\CatHolidayController;
use App\Http\Controllers\API\Admin\Catalogs\CatRejectionReasonController;
use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\EmployeeController;
use App\Http\Controllers\API\ReportGroupsController;
use App\Http\Controllers\API\ReportManagerController;
use App\Http\Controllers\API\EmployeeDocumentsController;
use App\Http\Controllers\API\FileReceiverController;
use App\Http\Controllers\API\{
    AccessTokenController,
    ExternalAffairReceptionController,
    ExternalAffairReceptionFromController,
    AffairCommentController,
    ExternalAffairCommentController,
    AffairReceptionController,
    AffairReceptionResponseController,
    AffairReceptionFromController,
    AttentionPeopleController,
    AttentionPeopleLogController,
    AttentionRouteController,
    AttentionStepController,
    ModuleController,
    PermissionController,
    ProfileController,
    StatisticsController,
    FileUploadController,
    MovementController,
    UserCopyController,
    PeopleCopyController,
    TurnController,
};
use App\Http\Controllers\API\Admin\Catalogs\UnitController;
use App\Http\Controllers\API\Admin\Catalogs\CatContractTypeController;
use App\Http\Controllers\API\Admin\Catalogs\CatDocTypeController;
use App\Http\Controllers\API\Admin\EntityController;
use App\Http\Controllers\API\Admin\Catalogs\CatInstructionController;
use App\Http\Controllers\API\Admin\Catalogs\LocationController;
use App\Http\Controllers\API\Admin\Catalogs\TabulatorController;
use App\Http\Controllers\API\Admin\Catalogs\CustomFieldsController;
use App\Http\Controllers\API\Admin\Catalogs\CatSubjectTypeController;
use App\Http\Controllers\API\Admin\Catalogs\CatPriorityController;
use App\Http\Controllers\API\Admin\PayrollConfig\{
    IsrConfigController,
    EmploymentSubsidiesConfigController,
    ImssDeductionsController
};

use App\Http\Controllers\API\PositionController;
use App\Http\Controllers\API\ConceptController;
use App\Http\Controllers\API\ReceiptController;

use App\Http\Controllers\API\Psp\PaymentLayoutController;

use App\Http\Controllers\API\Provider\PaymentController;
use App\Http\Controllers\API\Provider\ForgotPasswordController;

use App\Http\Controllers\API\Psp\ActivityController;
use App\Http\Controllers\SystemSettingsController;
use App\Models\Catalogs\CatContractType;

use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\API\LawVacationsConfigController;
use App\Models\LawVacations;

use App\Http\Controllers\FiveYearBonusController;
use App\Http\Controllers\API\EmployeeVacationController;
use App\Http\Controllers\API\Admin\Catalogs\CatVacationsTypeController;
use App\Http\Controllers\API\Admin\ChecksignController;
use App\Http\Controllers\API\Admin\EmailConfigurationController;
use App\Http\Controllers\API\Admin\StructureController;
use App\Http\Controllers\API\Admin\UserExternalController;
use App\Http\Controllers\API\Psp\DashboardController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('test', function(){
    return response()->json([
        'success' => true,
    ], 200);
});


Route::post('test', function(Illuminate\Http\Request $request){
    $message = 'Hello ' . $request->input('name');
    return response()->json([
        'message' => $message,
        'success' => true,
    ], 200);
});
Route::get('all-people', [PeopleController::class, 'allPeople']);
Route::post('all-people-request', [PeopleController::class, 'allPeopleRequest']);
Route::get('affairsToCentralBack', [AffairReceptionController::class, 'affairsToCentral']);
Route::get('central-entities',function (GuzzleHttp\Client $client) {
    $response2 = $client->request('GET', 'api/admin/entities');
    $users = json_decode($response2->getBody());
    dd($users);
});
Route::get('checksignKey', [ChecksignController::class, 'getChecksignKey']);
Route::get('checksignPost', [ChecksignController::class, 'postChecksign']);

Route::post('login', [AuthController::class, 'login'])->name('login');
Route::get('logout', [AuthController::class, 'logout']);

Route::get('checkTOTP/{usr}', [AuthController::class,'checkTOTP']);
Route::get('activateOTP/{usr}',[AuthController::class,'activateTOTP']);

Route::post('forgot', [ForgotPasswordController::class, 'create']);
Route::get('find/{token}', [ForgotPasswordController::class, 'find']);
Route::post('reset', [ForgotPasswordController::class, 'reset']);

Route::get('admin/config/getConfig', [SystemSettingsController::class, 'getConfig']);

Route::post('userStore', [UserController::class, 'store']); // Users controller here to user register in login
Route::post('peopleStore', [PeopleController::class, 'store']); // people controller here to user register in login

Route::group(['middleware' => ['auth:api']], function () {
    /* Payment ROUTES */
    Route::get('admin/providers/payment/get-pending', [PaymentController::class, 'getPending']);

    //Route::resource('dates', DateController::class);
    Route::group(['prefix' => 'admin'], function () {
        //Ruta para obtener la sesión actual
        Route::get('getAuth', [UserController::class, 'getAuth']);
        // Route::post('render', [CustomFieldsController::class,'render']);

        Route::group(['prefix' => 'catalogs'], function () {
            Route::get('', [CatalogsController::class,'index']);
            Route::get('get-states/{id}', [CatalogsController::class, 'getStatesByCountry']);
            Route::get('get-responsible/{id}', [CatalogsController::class, 'getResponsibleByPosition']);
            Route::get('get-positions/{id}', [CatalogsController::class, 'getPositionsByUnit']);
            Route::get('get-units/{id}', [CatalogsController::class, 'getUnitsByEntity']);
            Route::resource('documents', DocumentController::class);
            Route::resource('units', UnitController::class);
            Route::resource('entities',EntityController::class);
            Route::resource('tabulator', UnitController::class);
            Route::resource('location', LocationController::class);
            Route::resource('custom-fields', CustomFieldsController::class);
            Route::resource('contracts', CatContractTypeController::class);
            Route::resource('tabulators', TabulatorController::class);
            Route::resource('vacations-types', CatVacationsTypeController::class);
            Route::resource('holidays', CatHolidayController::class);
            Route::resource('rejection-reasons', CatRejectionReasonController::class);
            Route::resource('subject-types',CatSubjectTypeController::class);
            Route::resource('priorities',CatPriorityController::class);
            Route::resource('instructions',CatInstructionController::class);
            Route::get('getInstructions',[CatInstructionController::class, 'getInstructions']);
            Route::resource('doc-types',CatDocTypeController::class);

        });

        Route::group(['prefix' => 'config'], function() {
            Route::apiResource('law-vacations', LawVacationsConfigController::class);
            Route::apiResource('five-year-bonus', FiveYearBonusController::class);
        });

        Route::resource('users', UserController::class);
        Route::resource('user-externals', UserExternalController::class);
        Route::resource('people', PeopleController::class);
        Route::get('getAssistantByResponsable/{responsable}', [PeopleController::class, 'getAssistantByResponsable']);
        Route::get('getPeopleAuth', [PeopleController::class, 'getPeopleAuth']);
        Route::resource('profile', ProfileController::class);
        Route::resource('permissions', PermissionController::class);
        Route::resource('modules', ModuleController::class);
        Route::resource('isr-config', IsrConfigController::class);

        Route::get('calculate-isr', [IsrConfigController::class, 'calculate']);
        Route::resource('employment-subsidies-config', EmploymentSubsidiesConfigController::class);
        Route::resource('imss-deductions', ImssDeductionsController::class);
        Route::resource('issste-deductions', IsssteDeductionsController::class);
        // ruta para Recepción de asuntos - Control de gestión
        Route::resource('affairReceptions', AffairReceptionController::class);
        Route::get('affairReceptions/getAffairReceptionByFolio/{folio}', [AffairReceptionController::class, 'getAffairReceptionByFolio']);
        Route::get('affairReceptions', [AffairReceptionController::class, 'index']);
        Route::get('getAffairReceptionByIdUser', [AffairReceptionController::class, 'getAffairReceptionByIdUser']);
        Route::get('affairReceptions/showHistory/{folio}', [AffairReceptionController::class, 'showHistory']);
        Route::get('affairReceptionsDraft', [AffairReceptionController::class, 'draft']);
        Route::get('affairReceptionsCollabora', [AffairReceptionController::class, 'collabora']);
        Route::get('affairReceptionsInbox',[AffairReceptionController::class, 'inbox']);
        Route::get('affairReceptionsInboxByIdUser',[AffairReceptionController::class, 'inboxbyIdUser']);
        Route::get('affairReceptionsTurn',[AffairReceptionController::class, 'turn']);
        Route::get('affairReceptionsInboxHistory', [AffairReceptionController::class, 'inboxHistory']);
        Route::get('affairReceptionsPriorities', [AffairReceptionController::class, 'priorityBox']);
        Route::get('affairReceptionsGenerateFolio', [AffairReceptionController::class, 'generateFolio']);
        Route::get('affairReceptionsScheduleDocument', [AffairReceptionController::class, 'scheduleDocument']);
        Route::resource('affairComments', AffairCommentController::class);
        Route::post('affairComments/add-comment', [AffairCommentController::class, 'addComment']);
        Route::get('affairReceptions/getCollaborators/{id}', [AffairReceptionController::class, 'getCollaborators']);
        //Rutas External Comments Affairs
        Route::resource('externalAffairComments', ExternalAffairCommentController::class);
        Route::post('externalAffairComments/add-comment', [ExternalAffairCommentController::class, 'addComment']);
        // rutas para entidades, estructura y estructura-usuarios
        Route::resource('affairReceptionsFrom', AffairReceptionFromController::class);
        Route::get('affairReceptionsFrom/getAffairFromByIdAffair/{id}', [AffairReceptionFromController::class, 'getAffairFromByIdAffair']);
        // Ruta External Affair Receptions Froms
        Route::resource('externalAffairReceptionsFrom', ExternalAffairReceptionFromController::class);

        Route::resource('structures',StructureController::class);
        Route::resource('structureUsers',StructureUserController::class);
        //Affair Response
        Route::resource('affairReceptionsResponse', AffairReceptionResponseController::class);
        Route::get('affairReceptionsResponse/getAffairResponseByFolioResponse/{folioResponse}', [AffairReceptionResponseController::class, 'getAffairResponseByFolioResponse']);
        // Rutas External Affairs
        Route::resource('externalAffairReceptions', ExternalAffairReceptionController::class);
        Route::get('externalAffairReceptionsInbox',[ExternalAffairReceptionController::class, 'inbox']);
        // Rutas External Companies
        Route::resource('externalCompany', ExternalCompanyController::class);
        // Rutas External People
        Route::resource('externalPeople', ExternalPeopleController::class);
        // Rutas Get Entity
        Route::get('getEntitybyAuth', [EntityController::class, 'getEntityByAuth']);
        // Rutas External Sender
        Route::resource('externalSender', ExternalSenderController::class);
        //Affair Reject
        Route::resource('affairRejects', \App\Http\Controllers\API\AffairRejectController::class);
    });

    Route::resource('turn', TurnController::class);
    Route::get('turn/{id}/getLog', [TurnController::class, 'getLog']);

    // rutas para Rutas de atención - Control de gestión
    Route::resource('attentionRoutes', AttentionRouteController::class);
    Route::resource('attentionSteps', AttentionStepController::class);
    Route::resource('attentionPeople', AttentionPeopleController::class);
    Route::resource('attentionPeopleLogs', AttentionPeopleLogController::class);

    Route::post('positions/get-positions-by', [PositionController::class, 'getPositionsBy']);
    Route::post('');
    Route::resource('positions', PositionController::class);
    Route::apiResource('movements', MovementController::class);
    /* Actualizar  ruta a nivel employee */
    Route::get('getUserInfo', [AuthController::class,'userInfo']);
    Route::get('verify', [AuthController::class, 'verify']);
    Route::post('mobile-authorize', [AuthController::class, 'mobileAuthorize']);
    Route::get('get-statistics', [StatisticsController::class, 'getStatistics']);
    // destroyEntry

    Route::post('employee-report', [EmployeeController::class, 'employeeReport']);
    Route::get('get-catalog-employee-report', [EmployeeController::class, 'getCatalogEmployeeReport']);

    Route::post('employees/entries/{id}', [EmployeeController::class, 'destroyEntry']);
    Route::post('employees/massive-load', [EmployeeController::class, 'massiveLoad']);
    Route::get('employees/by-contract-type/{id}', [EmployeeController::class, 'getAllByContractType']);
    Route::get('employees/search-by-curp', [EmployeeController::class, 'searchByCURP']);
    Route::resource('employees/{id}/vacations', EmployeeVacationController::class);
    Route::resource('employees', EmployeeController::class);

    // /api/admin/config/edit
    Route::get('admin/config/edit', [SystemSettingsController::class, 'edit']);
    Route::apiResource('admin/config', SystemSettingsController::class);

    Route::patch('concepts/{id}/status', [ConceptController::class, 'patch']);
    Route::get('concepts/get-extraordinary', [ConceptController::class, 'getExtraordinary']);
    Route::resource('concepts', ConceptController::class);
    Route::resource('employees-document', EmployeeDocumentsController::class);



    Route::get('payment/order/export', [PaymentLayoutController::class, 'export']);
    Route::get('payment/order/{id}/export-txt', [PaymentLayoutController::class, 'exportTxtLayout']);
    Route::get('payment/order/{id}/export-txt-bbva', [PaymentLayoutController::class, 'exportTxtLayoutBbva']);
    Route::get('payment/order/{id}/export-txt-bbva-account', [PaymentLayoutController::class, 'exportTxtLayoutBbvaAccount']);

    /* PAYMENT ACTIVITIES */
    Route::get('/psp/reportActivity', [ActivityController::class, 'index']);
    Route::get('/psp/reportActivity/indexValidated', [ActivityController::class, 'indexValidated']);

    Route::post('/psp/reportActivity/validate', [ActivityController::class, 'checkPSP']);
    Route::post('/psp/reportActivity/validated', [ActivityController::class, 'check']);
    Route::post('/psp/reportActivity/decline', [ActivityController::class, 'decline']);
    Route::post('/psp/reportActivity/declined', [ActivityController::class, 'declined']);

    /* DASHBOARD PSP */
    Route::post('/psp/dashboard/serviceChart', [DashboardController::class, 'serviceChart']);
    Route::post('/psp/dashboard/serviceProject', [DashboardController::class, 'serviceProject']);
    Route::get('/psp/dashboard/serviceFilters', [DashboardController::class, 'serviceFilters']);
    Route::group(['prefix' => 'files'], function () {
        Route::post('upload', [FileUploadController::class, 'upload']);
        Route::post('{id}/generate', [FileUploadController::class, 'getCustomTemplate']);
        Route::get('affair/{id}', [FileUploadController::class, 'showAffair']);
    });

    Route::group(['prefix' => 'externalFiles'], function () {
        Route::post('upload', [\App\Http\Controllers\API\ExternalFileUploadController::class, 'upload']);
        Route::post('{id}/generate', [\App\Http\Controllers\API\ExternalFileUploadController::class, 'getCustomTemplate']);
    });
    Route::resource('accessToken', AccessTokenController::class);
    Route::get('access-token/get-token/{id}', [AccessTokenController::class, 'getToken']);
    Route::get('accessToken/getCollaborators/{id}', [AccessTokenController::class, 'getCollaborators']);
    Route::get('accessToken/getPermissions/{userid}/{affairid}', [AccessTokenController::class, 'getPermissions']);
    Route::post('access-token/share/{id}', [AccessTokenController::class, 'shareFile']);
});

Route::get('files/show/pdf/{id}', [FileUploadController::class, 'showFile']);
Route::get('files/image/{id}', [FileUploadController::class, 'getFile']);
Route::get('files/template', [FileUploadController::class, 'getTemplate']);
Route::get('files/{id}/annexes', [FileUploadController::class, 'fileAnnexes']);
Route::get('files/{id}/show/template', [FileUploadController::class, 'showCustomTemplate']);

//ExternalFiles
Route::get('externalFiles/show/{type}/{id}', [\App\Http\Controllers\API\ExternalFileUploadController::class, 'showFile']);
Route::get('externalFiles/image/{id}', [\App\Http\Controllers\API\ExternalFileUploadController::class, 'getFile']);
Route::get('externalFiles/template', [\App\Http\Controllers\API\ExternalFileUploadController::class, 'getTemplate']);
Route::get('externalFiles/{id}/exist', [\App\Http\Controllers\API\ExternalFileUploadController::class, 'fileExist']);
Route::get('externalFiles/{id}/show/template', [\App\Http\Controllers\API\ExternalFileUploadController::class, 'showCustomTemplate']);

Route::get('generatePDF', [ReceiptController::class, 'generatePDF']);

/** Reports */
Route::group(['middleware' => ['auth:api']], function () {
Route::resource('report-manager', ReportManagerController::class);
});

Route::resource('report-groups', ReportGroupsController::class);

/**Receive Files from BackCentral**/
Route::post('receiveaffair', [FileReceiverController::class, 'receive']);
