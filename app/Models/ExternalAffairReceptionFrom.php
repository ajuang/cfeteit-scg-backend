<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExternalAffairReceptionFrom extends Model
{
    use HasFactory;

    protected $fillable = [ 'affair_receptions_id', 'receiver_id', 'participation_types_id', 'sender_id', 'external_sender_id' ];
  
    public function externalAffairReception() {
      return $this->belongsTo('App\Models\ExternalAffairReception','id','external_affair_receptions_id');
    }

    public function sender() {
      return $this->belongsTo('App\Models\User','id','sender_id');
    }
  
    public function externalSender() {
      return $this->belongsTo('App\Models\ExternalSender','id','external_sender_id');
    }
  
    public function receiver() {
      return $this->belongsTo('App\Models\User','id','receiver_id');
    }
  
    public function participant() {
      return $this->belongsTo('App\Models\ParticipationType','id','participation_types_id');
    }
}
