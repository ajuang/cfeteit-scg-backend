<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AffairReceptionResponse extends Model
{
    use HasFactory;
    protected $fillable = [ 'folio_origin', 'folio_response' ];

}
