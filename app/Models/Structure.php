<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Structure extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [ 'entity_id', 'position_id', 'position_to_report_id' ];

    public function entity(){
        return $this->hasOne('App\Models\Entity','id','entity_id');
    }

    public function position(){
        return $this->hasOne('App\Models\Position','id','position_id');
    }

    public function positionsReport(){
        return $this->hasOne('App\Models\Position','id','position_to_report_id');
    }

    public function structureUser()
    {
        return $this->hasMany('App\Models\StructureUser');
    }

    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('id', 'like', '%' . $search . '%');
                    $q->orWhereHas('position', function (Builder $query ) use($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    });
                }
            });
        });
    }
}
