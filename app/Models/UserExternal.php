<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserExternal extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;
    protected $guard = 'external';

    const ACTIVATED = 1;
    const DESACTIVE = 0;

    protected $with = ['Entity'];

    protected $fillable = [
        'name',
        'lastname',
        'second_lastname',
        'unit_name',
        'position_name',
        'email',
        'phone',
        'entity_id',
        // 'active'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        // 'password'
    ];

    public function entity(){
        return $this->hasOne('App\Models\Entity','id','entity_id');
    }

    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('name', 'like', '%' . $search . '%');
                    $q->orWhere('lastname', 'like', '%' . $search . '%');
                }
            });
        });
    }

}
 