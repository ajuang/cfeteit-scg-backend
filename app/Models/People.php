<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;
use App\Models\Catalogs\CatUnit;
use App\Models\Position;


class People extends Model
{
    use HasFactory, SoftDeletes;

    protected $with = ['User'];

    protected $fillable = [
        'user_id',
        'name',
        'last_name',
        'second_lastname',
        'email',
        'phone',
        'entity_id',
        'position_id',
        'responsable',
        'assistant_id',
        'fullname'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function user()
    {
        return $this->hasOne(User::class,'id','user_id');
    }

    public function position(){
        return $this->hasOne('App\Models\Position','id','position_id');
    }

    public function assistant()
    {
        return $this->hasOne(User::class,'id','assistant_id');
    }

    public function structureUser(){
        return $this->belongsTo('App\Models\StructureUser');
    }

    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('email', 'like', '%' . $search . '%');
                    $q->orWhere('last_name', 'like', '%' . $search . '%');
                    $q->orWhere('second_lastname', 'like', '%' . $search . '%');
                }
            });
        });
    }

}
