<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TurnLog extends Model
{
    use HasFactory;
    protected $fillable = ['affair_id', 'user_id', 'turn_movement_id', 'turn_id'];
}
