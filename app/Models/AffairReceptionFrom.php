<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AffairReceptionFrom extends Model
{
    use HasFactory;

    protected $fillable = [ 'affair_receptions_id', 'receiver_id', 'participation_types_id', 'sender_id' ];

    public function affairReception() {
        return $this->belongsTo('App\Models\AffairReception','id','affair_receptions_id');
    }

    public function sender() {
        return $this->belongsTo('App\Models\User','id','sender_id');
    }

    public function receiver() {
        return $this->belongsTo('App\Models\User','id','receiver_id');
    }

    public function participationType() {
        return $this->belongsTo('App\Models\ParticipationType','id','participation_types_id');
    }
}
