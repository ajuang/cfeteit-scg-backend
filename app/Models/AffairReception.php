<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AffairReception extends Model
{
    use HasFactory;

    protected $fillable = [
        'folio',
        'document_date',
        'cat_doc_types_id',
        'cat_priorities_id',
        'date_send',
        'user_id',
        'files_id',
        'subject',
        'comments',
        'status_id',
        'historical',
        'has_collaborators',
        'require_response'
    ];
    
    public function docType() {
        return $this->hasOne('App\Models\Catalogs\CatDocType','id','cat_doc_types_id');
    }

    public function priority() {
        return $this->hasOne('App\Models\Catalogs\CatPriority','id','cat_priorities_id');
    }

    public function user() {
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function files() {
        return $this->hasMany('App\Models\File');
    }

    public function comments() {
        return $this->hasMany('App\Models\AffairComment');
    }

    // public function ccp() {
    //     return $this->hasOne('App\Models\User','id','ccp_id');
    // }

    public function receptionFroms() {
        return $this->hasMany('App\Models\AffairReceptionFrom');
    }

    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('id', 'like', '%' . $search . '%');
                    $q->orWhere('folio', 'like', '%' . $search . '%');
                }
            });
        });
    }
}
