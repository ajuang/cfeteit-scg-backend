<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatSerialNumber extends Model
{
    use HasFactory;
    protected $table="cat_serial_numbers";

    protected $fillable = [
        'serial',
      ];
}
