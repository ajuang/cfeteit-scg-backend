<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttentionPeople extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [ 'attention_step_id', 'unit_id', 'attention_type_id', 'instruction_id', 'user_id', 'has_privileges' ];
    
    public function step(){
        return $this->hasOne('App\Models\AttentionStep','id','attention_step_id');
    }

    public function unit(){
        return $this->hasOne('App\Models\Catalogs\CatUnit','id','unit_id');
    }

    public function attentionType(){
        return $this->hasOne('App\Models\Catalogs\CatAttentionType','id','attention_type_id');
    }

    public function instruction(){
        return $this->hasOne('App\Models\Catalogs\CatInstruction','id','instruction_id');
    }

    public function user(){
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('id', 'like', '%' . $search . '%');
                }
            });
        });
    }
}
