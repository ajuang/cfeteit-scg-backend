<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StructureUser extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [ 'people_id' ];

    public function structure(){
        return $this->hasOne('App\Models\Structure', 'id', 'structure_id');
    }

    public function people(){
        return $this->hasOne('App\Models\People', 'id', 'people_id');
    }

    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('id', 'like', '%' . $search . '%');
                    $q->orWhereHas('people', function (Builder $query ) use($search) {
                        $query->where('name', 'like', '%' . $search . '%');
                    });
                }
            });
        });
    }
}
