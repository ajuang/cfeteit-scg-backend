<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AffairReject extends Model
{
    use HasFactory;

    protected $with = ['CatRejectionReason'];

    protected $fillable = [
        'description',
        'cat_rejection_reason_id',
        'affair_reception_id',
    ];

    public function CatRejectionReason()
    {
        return $this->hasOne(CatRejectionReason::class,'id','cat_rejection_reason_id');
    }
    
    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->Where('description', 'like', '%' . $search . '%');
                    $q->Where('cat_rejection_reason_id', 'like', '%' . $search . '%');
                }
            });
        });
    }
}
