<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class ExternalCompany extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'bussiness_name',
        'rfc',
        'email',
        'phone',
        'adress',
    ];
    
    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('id', 'like', '%' . $search . '%');
                    $q->orWhere('name', 'like', '%' . $search . '%');
                    $q->orWhere('last_name', 'like', '%' . $search . '%');
                }
            });
        });
    }

}
