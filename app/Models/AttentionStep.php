<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttentionStep extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [ 'step_type_id', 'name', 'order', 'is_finisher' ];

    public function stepType(){
        return $this->hasOne('App\Models\Catalogs\CatStepType', 'id', 'step_type_id');
    }

    public function attentionRoute(){
        return $this->hasOne('App\Models\AttentionRoute', 'id', 'attention_route_id');
    }

    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('name', 'like', '%' . $search . '%');
                }
            });
        });
    }
}
