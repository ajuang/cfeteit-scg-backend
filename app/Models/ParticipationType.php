<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ParticipationType extends Model
{
    use HasFactory;

    public function receptionFroms() {
        return $this->hasMany('App\Models\AffairReceptionFrom');
    }
}
