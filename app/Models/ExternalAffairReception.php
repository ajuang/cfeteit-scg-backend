<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExternalAffairReception extends Model
{
  protected $fillable = [
    'folio',
    'subject',
    'description',
    'document_date',
    'attention_date',
    'reception_date',
    'reception_time',
    'cat_doc_types_id', /* doc_type_id */
    'cat_priorities_id', /* priority_id */
    // 'cat_sender_types_id',
    'cat_recep_meds_id',
    // 'sender_entity_id',
    // 'sender_unit_id',
    // 'sender_position_id',
    // 'sender_responsable_id',
    'people_id',
    'files_id',
//    'comments',
  ];

  use HasFactory;
  public function docType() {
    return $this->hasOne('App\Models\Catalogs\CatDocType','id','cat_doc_types_id');
  }

  public function senderType() {
    return $this->hasOne('App\Models\Catalogs\CatSenderType', 'id', 'cat_sender_types_id');
  }

  public function recepMed() {
    return $this->hasOne('App\Models\Catalogs\CatRecepMed', 'id', 'cat_recep_meds_id');
  }
  
  public function priority() {
      return $this->hasOne('App\Models\Catalogs\CatPriority','id','cat_priorities_id');
  }

  public function senderEntity() {
    return $this->hasOne('App\Models\Entity','id','sender_entity_id');
  }

  public function senderUnit() {
    return $this->hasOne('App\Models\Catalogs\CatUnit','id','sender_unit_id');
  }

  public function senderPosition() {
    return $this->hasOne('App\Models\Position','id','sender_position_id');
  }

  public function senderResponsable(){
    return $this->hasOne('App\Models\People', 'id', 'sender_responsable_id');
  }

  
  public function user() {
    return $this->hasOne('App\Models\User','id','user_id');
  }

  public function files() {
    return $this->hasMany('App\Models\File');
  }

  public function comments() {
    return $this->hasMany('App\Models\AffairComment');
  }

  // public function ccp() {
  //     return $this->hasOne('App\Models\User','id','ccp_id');
  // }

  public function receptionFroms() {
    return $this->hasMany('App\Models\ExternalAffairReceptionFrom');
  }

  public function scopeSearch($query, $search)
  {
    return $query->when(! empty ($search), function ($query) use ($search) {

      return $query->where(function($q) use ($search)
      {
        if (isset($search) && !empty($search)) {
          $q->orWhere('id', 'like', '%' . $search . '%');
          $q->orWhere('folio', 'like', '%' . $search . '%');
        }
      });
    });
  }
}
