<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Entity extends Model
{
    use HasFactory, SoftDeletes;

    // protected $with = ['unit'];

    protected $fillable = [ 'name', 'acronym' ];

    // public function clasification()
    // {
    //     return $this->hasOne('App\Models\Catalogs\CatClasification', 'id', 'clasification_id');
    // }

    public function units(){
        return $this->hasMany('App\Models\Catalogs\CatUnit');
    }

    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('name', 'like', '%' . $search . '%');
                    $q->orWhere('acronym', 'like', '%' . $search . '%');
                }
            });
        });
    }
}
