<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Catalogs\CatUnit;

class CatSubjectType extends Model
{
    use HasFactory, SoftDeletes;
    
    protected $fillable = [
        'id',
        'unit_id',
        'subjectType',
      ];

    public function Unit()
    {
        return $this->hasOne(CatUnit::class,'id','unit_id');
    }

    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('subjectType', 'like', '%' . $search . '%');
                    $q->orWhere('unit_id', 'like', '%' . $search . '%');
                }
            });
        });
    }
}
