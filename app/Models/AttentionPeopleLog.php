<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AttentionPeopleLog extends Model
{
    use HasFactory;

    protected $fillable = [ 'dateHour_attention', 'attention_type_id', 'unit_id' ];

    public function attentionType(){
        return $this->hasOne('App\Models\Catalogs\CatAttentionType', 'id', 'attention_type_id');
    }

    public function unit(){
        return $this->hasOne('App\Models\Catalogs\CatUnit', 'id', 'unit_id');
    }

    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('id', 'like', '%' . $search . '%');
                }
            });
        });
    }
}
