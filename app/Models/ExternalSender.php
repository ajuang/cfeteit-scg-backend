<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ExternalSender extends Model
{
    use HasFactory;

    protected $fillable = [
        'external_people_id',
        'external_company_id',
        'sender_entity_id',
        'sender_type_id'
    ];

    public function externalPeople() {
        return $this->hasOne('App\Models\ExternalPeople', 'id', 'external_people_id');
    }

    public function externalCompany() {
        return $this->hasOne('App\Models\ExternalCompany', 'id', 'external_company_id');
    }

    public function senderEntity() {
        return $this->hasOne('App\Models\Entity','id','sender_entity_id');
    }

    public function senderType() {
        return $this->hasOne('App\Models\Catalogs\CatSenderType', 'id', 'sender_type_id');
      }
}
