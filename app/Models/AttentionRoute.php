<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AttentionRoute extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [ 'name', 'created_by_user_id', 'description' ];

    public function user(){
        return $this->hasOne('App\Models\User','id','created_by_user_id');
    }

    public function keywords(){
        return $this->hasMany('App\Models\Catalogs\CatKeyword');
    }

    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('name', 'like', '%' . $search . '%');
                }
            });
        });
    }
}
