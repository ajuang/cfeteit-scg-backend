<?php

namespace App\Models\Catalogs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CatInstruction extends Model
{
    use HasFactory, SoftDeletes;

    protected $with = ['unit'];
    
    protected $fillable = [ 'name', 'unit_id' ];

    public function unit()
    {
        return $this->hasOne('App\Models\Catalogs\CatUnit', 'id', 'unit_id');
    }

    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('name', 'like', '%' . $search . '%');
                    $q->orWhere('unit_id', 'like', '%' . $search . '%');
                }
            });
        });
    }
}
