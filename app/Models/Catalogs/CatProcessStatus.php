<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatProcessStatus extends Model
{
    use HasFactory;
    protected $table="cat_process_statuses";

    protected $fillable = [
        'name',
      ];
}
