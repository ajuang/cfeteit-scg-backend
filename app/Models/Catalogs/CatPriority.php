<?php

namespace App\Models\catalogs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CatPriority extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'name',
        'days',
      ];
      
    public function Unit()
    {
        return $this->hasOne(CatUnit::class,'id','unit_id');
    }

    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('name', 'like', '%' . $search . '%');
                    $q->orWhere('days', 'like', '%' . $search . '%');
                }
            });
        });
    }

}
