<?php

namespace App\Models\Catalogs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CatTheme extends Model
{
    use HasFactory,SoftDeletes;

    protected $fillable = [ 'name', 'affair_reception_id' ];

    public function affairReception(){
        return $this->hasOne('App\Models\AffairReception','id','affair_reception_id');
    }

    public function subtheme(){
        return $this->hasMany('App\Models\Catalogs\catSubtheme');
    }

    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('name', 'like', '%' . $search . '%');
                }
            });
        });
    }
}
