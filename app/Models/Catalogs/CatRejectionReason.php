<?php

namespace App\Models\Catalogs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Catalogs\CatUnit;
use Illuminate\Database\Eloquent\SoftDeletes;

class CatRejectionReason extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $with = ['CatUnit'];

    protected $fillable = [
        'description',
        'cat_unit_id',
    ];

    public function CatUnit()
    {
        return $this->hasOne(CatUnit::class,'id','cat_unit_id');
    }
    
    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->Where('description', 'like', '%' . $search . '%');
                    $q->Where('cat_unit_id', 'like', '%' . $search . '%');
                }
            });
        });
    }
}
