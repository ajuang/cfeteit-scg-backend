<?php

namespace App\Models\Catalogs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CatUnit extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'entity_id',
        'name',
        'code',
        'description',
        'nomenclature',
        'acronym',
        'user_id',
        'responsible',
        'parent_unit_id',
        'entry',
        'departure',
        'operator',
        'link_unit'
    ];

    public function entity(){
        return $this->belongsTo('App\Models\Entity', 'id', 'entity_id');
    }

    public function user(){
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }

    public function CatUnit(){
        return $this->hasOne('App\Models\Catalogs\CatUnit', 'id', 'parent_unit_id');
    }

    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('entity_id', 'like', '%' . $search . '%');
                    $q->orWhere('name', 'like', '%' . $search . '%');
                    $q->orWhere('code', 'like', '%' . $search . '%');
                    $q->orWhere('acronym', 'like', '%' . $search . '%');
                    $q->orWhere('nomenclature', 'like', '%' . $search . '%');
                }
            });
        });
    }
}
