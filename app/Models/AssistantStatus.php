<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AssistantStatus extends Model
{
    use HasFactory;
    protected $table="assistant_status";

    protected $fillable = [
        'name',
      ];
}
