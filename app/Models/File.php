<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'affair_id',
    ];

    protected $appends  = ['hash'];

    public function getHashAttribute()
    {
        return encrypt( $this->id );
    }

    public function user() {
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function affair() {
        return $this->belongsTo('App\Models\AffairReception','id','affair_id');
    }

}
