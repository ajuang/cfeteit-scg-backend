<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AffairComment extends Model
{
    use HasFactory;

    protected $fillable = ['affair_reception_id', 'comment', 'user_id'];

    public function affair() {
        return $this->belongsTo('App\Models\AffairReception','id','affair_reception_id');
    }

    public function user() {
        return $this->hasOne('App\Models\User','id','user_id');
    }

    public function scopeSearch($query, $search)
    {
        return $query->when(! empty ($search), function ($query) use ($search) {

            return $query->where(function($q) use ($search)
            {
                if (isset($search) && !empty($search)) {
                    $q->orWhere('id', 'like', '%' . $search . '%');
                    $q->orWhere('affair_reception_id', 'like', '%' . $search . '%');
                    $q->orWhere('comment', 'like', '%' . $search . '%');
                }
            });
        });
    }
}
