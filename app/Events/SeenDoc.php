<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Models\AffairReception;

class SeenDoc implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $username;
    public $affair;
    public $username_sender;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($username, AffairReception $affair, $username_sender)
    {
        $this->username = $username;
        $this->affair = $affair;
        $this->username_sender = $username_sender;
    }

    public function broadcastWith()
    {
        return [
            'username' => $this->username,
            'affair' => $this->affair,
            'username_sender' => $this->username_sender,
            'type' => 'Asunto leido'
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('noti.'.$this->username);
    }
}
