<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Models\AffairComment;
use App\Models\People;
use App\Models\Position;

class CommentCreated implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $affair_id;
    public $comment;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(AffairComment $commentadd, $affair_id)
    {
        $this->affair_id = $affair_id;
        $this->comment = $commentadd;

    }

    public function broadcastWith()
    {
		$people = People::find($this->comment->user_id);
		$position = Position::find($people->position_id);
        $this->comment->name = $people->name;
        $this->comment->last_name = $people->last_name;
        $this->comment->second_lastname = $people->second_lastname;
        $this->comment->position_name = $position->name;
        return [
            'comment' => $this->comment
        ];
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('Comment.'.$this->affair_id);
    }
}
