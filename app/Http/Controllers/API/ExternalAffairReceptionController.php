<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\API\FileUploadController;
use App\Models\Catalogs\CatUnit;
use App\Models\Entity;
use App\Models\Ftp;
use App\Models\Position;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\ExternalAffairReception;
use App\Models\ExternalAffairReceptionFrom;
use Illuminate\Support\Facades\Auth;
use App\Models\File;
use App\Models\AccessToken;
use App\Models\People;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Str;
use Illuminate\Support\Facades\Storage;
use App\Events\AffairReceived;
use App\Models\User;

class ExternalAffairReceptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      // return 1;
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');

            $user_id = Auth::user()->id;

            $affairs = ExternalAffairReception::search($search)
						->join('external_affair_reception_froms','external_affair_receptions.id','=','external_affair_reception_froms.external_affair_receptions_id')
						->join('users','external_affair_reception_froms.receiver_id','=','users.id')
                        ->join('external_senders', 'external_affair_reception_froms.external_sender_id', '=', 'external_senders.id')
                        ->join('cat_sender_types', 'external_senders.sender_type_id', '=', 'cat_sender_types.id')
						->join('people','users.id','=','people.user_id')
						->join('positions','people.position_id','=','positions.id')
						->join('cat_units','positions.cat_unit_id','=','cat_units.id')
						->join('entities','cat_units.entity_id','=','entities.id')
            ->join('cat_doc_types', 'cat_doc_types.id', '=', 'external_affair_receptions.cat_doc_types_id')
            ->join('cat_priorities', 'cat_priorities.id', '=', 'external_affair_receptions.cat_priorities_id')
            ->select('people.name as receiver_name',
                     'people.last_name',
                     'people.second_lastname',
                     'entities.acronym as entity_name',
					 'positions.name as position_name',
                     'external_affair_receptions.id as id',
                     'external_affair_receptions.folio',
                     'external_affair_receptions.subject',
                     'external_affair_receptions.document_date',
                     'cat_sender_types.name as type_sender',
                     'cat_priorities.name as priorities',
					 'cat_doc_types.name as doc_type')
            ->orWhere('external_affair_receptions.user_id', $user_id)
            ->orderBy('external_affair_receptions.created_at', 'desc')
            ->paginate($rowsPerPage);

            // $affairs = DB::table('affair_receptions')
            // ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
            // ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
            // ->select('affair_receptions.folio','cat_priorities.name as priorities','cat_doc_types.name as doc_type','affair_receptions.document_date')
            // ->get();
            
            return response()->json([
                'success' => true,
                'affairs' => $affairs,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
// 
//         $list = AffairReception::all();
//         return response()->json(['success'=>true,'data'=>['schedules'=>$list]]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // return $request->all();
      // $responsable = People::where('user_id', Auth::user()->id);
      $idUser = Auth::user()->id;
      // if($responsable->responsable != null) {
      //  $idUser = $responsable->responsable;
      
        try { 
            DB::beginTransaction();
            // dd($request->all());
            $externalAffair = new ExternalAffairReception();
            $externalAffair->fill($request->all());
            $externalAffair->folio = $request->input('folio');
            $externalAffair->subject = $request->input('subject');
            $externalAffair->description = $request->input('description');
            $externalAffair->document_date = $request->input('document_date');
            $externalAffair->attention_date = $request->input('attention_date');
            $externalAffair->reception_date = $request->input('reception_date');
            $externalAffair->reception_time = $request->input('reception_time');
            // $externalAffair->external_people_id = $request->input('external_people_id');
            // $externalAffair->external_company_id = $request->input('external_company_id');
            $externalAffair->cat_doc_types_id = $request->input('cat_doc_types_id');
            $externalAffair->cat_priorities_id = $request->input('cat_priorities_id');
            // $externalAffair->cat_sender_types_id = $request->input('cat_sender_types_id');
            $externalAffair->cat_recep_meds_id = $request->input('cat_recep_meds_id');
            // $externalAffair->sender_entity_id = $request->input('sender_entity_id');
            // $externalAffair->sender_unit_id = $request->input('sender_unit_id');
            // $externalAffair->sender_position_id = $request->input('sender_position_id');
            // $externalAffair->sender_responsable_id = $request->input('sender_responsable_id');
            $externalAffair->user_id = $idUser;
            $externalAffair->save();
            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
                'externalAffair'  => $externalAffair
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $affair = ExternalAffairReception::where('id',$id)->first();
            return response()->json([
                'success' => true,
                'affair' => $affair,
			]);
        }catch (\Exception $e) {
            DB::rollback();
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $idUser = Auth::user()->id;
        try { 
            DB::beginTransaction();
            // dd($request->all());
            $externalAffair = ExternalAffairReception::find($id);
            $externalAffair->fill($request->all());
            $externalAffair->folio = $request->input('folio');
            $externalAffair->subject = $request->input('subject');
            $externalAffair->description = $request->input('description');
            $externalAffair->document_date = $request->input('document_date');
            $externalAffair->attention_date = $request->input('attention_date');
            $externalAffair->reception_date = $request->input('reception_date');
            $externalAffair->reception_time = $request->input('reception_time');
            // $externalAffair->external_people_id = $request->input('external_people_id');
            // $externalAffair->external_company_id = $request->input('external_company_id');
            $externalAffair->cat_doc_types_id = $request->input('cat_doc_types_id');
            $externalAffair->cat_priorities_id = $request->input('cat_priorities_id');
            // $externalAffair->cat_sender_types_id = $request->input('cat_sender_types_id');
            $externalAffair->cat_recep_meds_id = $request->input('cat_recep_meds_id');
            // $externalAffair->sender_entity_id = $request->input('sender_entity_id');
            // $externalAffair->sender_unit_id = $request->input('sender_unit_id');
            // $externalAffair->sender_position_id = $request->input('sender_position_id');
            // $externalAffair->sender_responsable_id = $request->input('sender_responsable_id');
            $externalAffair->user_id = $idUser;
            $externalAffair->save();
            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
                'externalAffair'  => $externalAffair
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
        /* try {
            DB::beginTransaction();
            $affair = ExternalAffairReception::find($id);
            $affair->draft = $request->input('draft');
            if ($request->input('draft') === false) {
                $affair_from = ExternalAffairReceptionFrom::where('external_affair_receptions_id', $id)->first();
                $file = File::where('affair_id', $id)->first();
                $token = new AccessToken();
                $token->token = Str::random(40);
                $token->user_id = $affair_from->receiver_id;
                $token->file_id = $file->id;
                $token->UserCanWrite = false;
                $token->HidePrintOption = false;
                $token->DisablePrint = false;
                $token->HideSaveOption = true;
                $token->HideExportOption = false;
                $token->DisableExport = false;
                $token->DisableCopy = false;
                $token->save();

                $sign = $request->input('efirma');
                $chain = $request->input('cadena');
                $url = $request->input('url');

                FileUploadController::signDoc($file, $sign, $chain, $url);

                $user = User::find($affair_from->receiver_id);
                broadcast(new AffairReceived($user->username, $affair, Auth::user()->username));
                
                //******************************Enviar a RabbitMQ***************************************
                $connection = new AMQPStreamConnection(env('RABBITMQ_HOST'), env('RABBITMQ_PORT'), env('RABBITMQ_USER'), env('RABBITMQ_PASSWORD'));
                $channel = $connection->channel();
  
                $channel->queue_declare(env('RABBITMQ_QUEUE'), false, false, false, false);
  
                $msg = new AMQPMessage('affair='.$affair_from->id.'&file='.$file->filename.'&authID='.Auth::user()->id);
                $channel->basic_publish($msg, '', env('RABBITMQ_QUEUE'));
                echo 'affair='.$affair_from->receiver_id.'&file='.$file->filename.'&authID='.Auth::user()->id;
                $channel->close();
                $connection->close();
                
//                //Entidad de origen
//                $peopleOrigin = People::where('user_id', Auth::user()->id)->first();
//                $positionOrigin = Position::where('id', $peopleOrigin->position_id)->first();
//                $unitOrigin = CatUnit::where('id', $positionOrigin->cat_unit_id)->first();
//              
//                //Entidad de envío
//                $people = People::where('id', $affair_from->receiver_id)->first();    
//                $position = Position::where('id', $people->position_id)->first();
//                $unit = CatUnit::where('id', $position->cat_unit_id)->first();
//                $entity = Entity::where('id', $unit->entity_id)->first();
//                $ftp = Ftp::where('entity_id', $entity->id)->first();
//                
//                if($unitOrigin->entity_id != $entity->id) {
//                  // carpeta = "nombre-de-tu-carpeta";
//                  $files =  storage_path('app/affair/' . $file->filename);
//                  //    $nombre_archivo = $files->getClientOriginalName();
//                  config(['FTP_HOST' => $ftp->host]);
//                  config(['FTP_USERNAME' => $ftp->username]);
//                  config(['FTP_PASSWORD' => $ftp->password]);
//                  Storage::disk('ftp')->put($file->filename, \File::get($files)); 
//                }
            }
            $affair->save();

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }*/ 
    }

    public function updateForm(Request $request, $id)
    {
        $idUser = Auth::user()->id;
        try { 
            DB::beginTransaction();
            // dd($request->all());
            $externalAffair = ExternalAffairReception::find($id);
            $externalAffair->fill($request->all());
            $externalAffair->folio = $request->input('folio');
            $externalAffair->subject = $request->input('subject');
            $externalAffair->description = $request->input('description');
            $externalAffair->document_date = $request->input('document_date');
            $externalAffair->attention_date = $request->input('attention_date');
            $externalAffair->reception_date = $request->input('reception_date');
            $externalAffair->reception_time = $request->input('reception_time');
            $externalAffair->external_people_id = $request->input('external_people_id');
            $externalAffair->external_company_id = $request->input('external_company_id');
            $externalAffair->cat_doc_types_id = $request->input('cat_doc_types_id');
            $externalAffair->cat_priorities_id = $request->input('cat_priorities_id');
            $externalAffair->cat_sender_types_id = $request->input('cat_sender_types_id');
            $externalAffair->cat_recep_meds_id = $request->input('cat_recep_meds_id');
            $externalAffair->sender_entity_id = $request->input('sender_entity_id');
            // $externalAffair->sender_unit_id = $request->input('sender_unit_id');
            // $externalAffair->sender_position_id = $request->input('sender_position_id');
            // $externalAffair->sender_responsable_id = $request->input('sender_responsable_id');
            $externalAffair->user_id = $idUser;
            $externalAffair->save();
            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
                'externalAffair'  => $externalAffair
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $affair = AffairReception::where('id',$id)->first();
            //error_log(print_r($scheduleDay, true));
            $affair->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            error_log($e->getMessage());
            return response('',500);
        }
    }

    public function inbox(Request $request)
    {
      // return 1;
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');
//            $responsable = $request->input('responsable');

            
$user_id = Auth::user()->id;

$affairsInbox = People::search($search)->join('positions','people.position_id','=','positions.id')
  ->join('cat_units','positions.cat_unit_id','=','cat_units.id')
  ->join('entities','cat_units.entity_id','=','entities.id')
  ->join('users','people.user_id','=','users.id')
  ->join('external_affair_reception_froms','users.id','=','external_affair_reception_froms.sender_id')
  ->join('external_affair_receptions','external_affair_reception_froms.external_affair_receptions_id','=','external_affair_receptions.id')
  ->join('participation_types','external_affair_reception_froms.participation_type_id','=','participation_types.id')
  ->join('cat_priorities','external_affair_receptions.cat_priorities_id','=','cat_priorities.id')
  ->select('people.name as people_name', 'bussiness_name', DB::raw("CONCAT(external_people.name,' ',external_people.last_name,' ',external_people.second_lastname) as fullname"), 'people.last_name', 'people.second_lastname','positions.name as position_name',
                'entities.acronym as entity_name','external_affair_receptions.id as id','external_affair_receptions.folio','external_affair_receptions.subject',
                'cat_priorities.name as priority','external_affair_receptions.document_date','participation_types.name as attentionType')
              ->paginate($rowsPerPage);
           /* $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');
//            $responsable = $request->input('responsable');

            $user_id = Auth::user()->id;

            $affairsInbox = People::search($search)
              ->join('positions','people.position_id','=','positions.id')
              ->join('cat_units','positions.cat_unit_id','=','cat_units.id')
              // ->join('entities','cat_units.entity_id','=','entities.id')
              ->join('users','people.user_id','=','users.id')
              // ->join('external_affair_reception_froms','users.id','=','external_affair_reception_froms.sender_id')
              ->join('external_affair_receptions','external_affair_reception_froms.external_affair_receptions_id','=','external_affair_receptions.id')
              // ->join('users','external_affair_reception_froms.receiver_id','=','receiver_id')
              ->join('participation_types','external_affair_reception_froms.participation_types_id','=','participation_types.id')
              // ->join('external_senders','external_affair_reception_froms.external_sender_id','=','external_sender.id')
              ->join('cat_priorities','external_affair_receptions.cat_priorities_id','=','cat_priorities.id')
              // ->join('cat_sender_types','external_affair_receptions.cat_sender_types_id','=','cat_sender_types.id')
              // ->leftjoin('external_companies','external_affair_receptions.external_company_id','=','external_companies.id')
              // ->leftjoin('external_people','external_affair_receptions.external_people_id','=','external_people.id')
              ->select('people.name as people_name', 
                        'people.last_name', 
                        'people.second_lastname',
                        'positions.name as position_name',
						'external_affair_receptions.id as id',
                        'external_affair_receptions.folio',
                        'external_affair_receptions.subject',
						'cat_priorities.name as priority',
                        'external_affair_receptions.document_date',
                        'participation_types.name as attentionType')
//              ->where('external_affair_reception_froms.receiver_id', $user_id)
              ->orderBy('external_affair_receptions.created_at', 'desc')
              ->paginate($rowsPerPage); */

            // $affairsSearch = AffairReception::search($search)->orderBy('created_at', 'desc')->paginate($rowsPerPage);
            
            return response()->json([
                'success' => true,
                'affairsInbox' => $affairsInbox,
                // 'affairsSearch' => $affairsSearch
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

  public function draft(Request $request)
  {
    // return 1;
    try {
        $rowsPerPage = $request->input('rowsPerPage');
        $search = $request->input('search');

        $user_id = Auth::user()->id;
        $files = AccessToken::where('user_id', $user_id)->get()->pluck('file_id')->toArray();
        $affairs_id = File::whereIn('id', $files)->get()->pluck('affair_id')->toArray();

        $affairs = AffairReception::search($search)
            ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
            ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
            ->join('users', 'users.id', '=', 'affair_receptions.user_id')
            ->join('people', 'people.user_id', '=', 'users.id')
            ->join('positions', 'positions.id', '=', 'people.position_id')
            ->select('affair_receptions.id as id','affair_receptions.folio','cat_priorities.name as priorities','cat_doc_types.name as doc_type','affair_receptions.document_date', 'subject','people.name as people_name', 'people.last_name', 'people.second_lastname', 'positions.name as position_name')
            ->whereIn('affair_receptions.id', $affairs_id)
            ->where('affair_receptions.draft',true)
            ->orderBy('affair_receptions.created_at', 'desc')
            ->paginate($rowsPerPage);

        return response()->json([
            'success' => true,
            'affairs' => $affairs,
        ]);
    } catch (\Exception $e) {
        DB::rollback();
        return response()->json([
            'success' => false,
            'message' => $e->getMessage()
        ]);
    }
  }

  public function priorityBox(Request $request)
    {
      // return 1;
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');

            $user_id = Auth::user()->id;

            $priorityAffairs = People::search($search)
							->join('positions','people.position_id','=','positions.id')
							->join('cat_units','positions.cat_unit_id','=','cat_units.id')
							->join('entities','cat_units.entity_id','=','entities.id')
              ->join('users','people.user_id','=','users.id')
              ->join('affair_reception_froms','users.id','=','affair_reception_froms.sender_id')
              ->join('affair_receptions','affair_reception_froms.affair_receptions_id','=','affair_receptions.id')
              ->join('participation_types','affair_reception_froms.participation_type_id','=','participation_types.id')
              ->join('cat_priorities','affair_receptions.cat_priorities_id','=','cat_priorities.id')
              ->select('people.name as people_name', 'people.last_name', 'people.second_lastname','positions.name as position_name',
							'entities.acronym as entity_name','affair_receptions.id as id','affair_receptions.folio','affair_receptions.subject',
							'cat_priorities.name as priority','affair_receptions.document_date','participation_types.name as attentionType')
              ->where('affair_reception_froms.receiver_id', $user_id)
              ->where('affair_receptions.draft', false)
              ->where('cat_priorities.name', 'Urgente')
              ->orderBy('affair_receptions.created_at', 'desc')
              ->paginate($rowsPerPage);
            
            return response()->json([
                'success' => true,
                'priorityAffairs' => $priorityAffairs
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function affairsToCentral(Request $request)
    {
      // return 1;
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');

            $affairInfoSender = AffairReception::search($search)
						->join('affair_reception_froms','affair_receptions.id','=','affair_reception_froms.affair_receptions_id')
						->join('users','affair_reception_froms.sender_id','=','users.id')
						->join('people','users.id','=','people.user_id')
						->join('positions','people.position_id','=','positions.id')
						->join('cat_units','positions.cat_unit_id','=','cat_units.id')
						->join('entities','cat_units.entity_id','=','entities.id')
            ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
            ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
            ->select('affair_receptions.id as affair_id','affair_receptions.folio','affair_reception_froms.sender_id',
            'entities.id as origin_entity_id','entities.acronym as origin_entity_name','people.name as sender_name',
            'people.last_name','people.second_lastname','positions.name as position_name',
            'cat_priorities.name as priorities','cat_doc_types.name as doc_type','affair_receptions.document_date')
            ->where('affair_receptions.draft', false)
            ->orderBy('affair_receptions.created_at', 'desc')
            ->paginate($rowsPerPage);
            
            $affairInfoReceiver = AffairReception::search($search)
						->join('affair_reception_froms','affair_receptions.id','=','affair_reception_froms.affair_receptions_id')
						->join('users','affair_reception_froms.receiver_id','=','users.id')
						->join('people','users.id','=','people.user_id')
						->join('positions','people.position_id','=','positions.id')
						->join('cat_units','positions.cat_unit_id','=','cat_units.id')
						->join('entities','cat_units.entity_id','=','entities.id')
            ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
            ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
            ->select('entities.id as destiny_entity_id','entities.acronym as destiny_entity_name','people.name as receiver_name',
            'people.last_name','people.second_lastname','positions.name as position_name')
            ->where('affair_receptions.draft', false)
            ->orderBy('affair_receptions.created_at', 'desc')
            ->paginate($rowsPerPage);

            // $affairs = DB::table('affair_receptions')
            // ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
            // ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
            // ->select('affair_receptions.folio','cat_priorities.name as priorities','cat_doc_types.name as doc_type','affair_receptions.document_date')
            // ->get();
            
            return response()->json([
                'success' => true,
                'affairInfoSender' => $affairInfoSender,
                'affairInfoReceiver' => $affairInfoReceiver,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
// 
//         $list = AffairReception::all();
//         return response()->json(['success'=>true,'data'=>['schedules'=>$list]]);
    }
}
