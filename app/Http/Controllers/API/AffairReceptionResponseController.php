<?php

namespace App\Http\Controllers\API;

use App\Models\AffairReceptionResponse;
use App\Models\AffairReception;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class AffairReceptionResponseController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
      try {
        $rowsPerPage = $request->input('rowsPerPage');
        $search = $request->input('search');
        $affairs = AffairReceptionResponse::search($search)->orderBy('created_at','desc')->paginate($rowsPerPage);
        return response()->json([
          'success' => true,
          'affairs' => $affairs,
        ]);
      }catch (\Exception $e) {
        DB::rollback();
        return response()->json([
          'success' => false,
          'message' => $e->getMessage()
        ]);
      }
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
        DB::beginTransaction();
        $folio_origin = $request->input('folio_origin');
        $folio_response = $request->input('folio_response');
        error_log(print_r($folio_origin, true));

        $affairOrigin = AffairReceptionResponse::where('folio_response', $folio_origin)->first();

        if(isset($affairOrigin)) {
            $affair = AffairReception::where('folio', $affairOrigin->folio_origin)->first();
            $affair->historical = true;
            $affair->save();
        }

        $affairResponse = new AffairReceptionResponse();
        $affairResponse->folio_origin = $folio_origin;
        $affairResponse->folio_response = $folio_response;

        $affairResponse->save();

        DB::commit();


        return response()->json([
        'success' => true,
        'message' => '',
        'affairResponse'  => $affairResponse
        ], 200);
    } catch (\Exception $e) {
        DB::rollback();
        return response()->json([
        'success' => false,
        'message' => $e->getMessage()
        ]);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    try {
      $affair = AffairReceptionResponse::where('id',$id)->first();
      return response()->json([
        'success' => true,
        'affair' => $affair,
      ]);
    }catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      DB::beginTransaction();
      $affair = AffairReceptionResponse::find($id);
      $affair->fill($request->all());
      $affair->save();

      DB::commit();

      return response()->json([
        'success' => true,
        'message' => '',
      ], 200);
    } catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      DB::beginTransaction();
      $affair = AffairReceptionResponse::where('id',$id)->first();
      //error_log(print_r($scheduleDay, true));
      $affair->delete();

      DB::commit();
    } catch (\Exception $e) {
      DB::rollBack();
      error_log($e->getMessage());
      return response('',500);
    }
  }

  public function getAffairResponseByFolioResponse($folioResponse)
  {
    try {
      $affair = AffairReceptionResponse::where('folio_response',$folioResponse)->first();
      return response()->json([
        'success' => true,
        'affair' => $affair,
      ]);
    }catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
  }
}
