<?php

namespace App\Http\Controllers\API;

use App\Models\People;
use App\Models\AccessToken;
use App\Models\File;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\AffairReception;
use App\Models\AffairReceptionResponse;
use App\Models\User;
use App\Events\modifiedDoc;
use Illuminate\Support\Facades\DB;
use App\Models\Turn;
use App\Models\TurnLog;
use DateTime;

class WopiController extends Controller
{
    public function checkFileInfo(Request $request, $fileId) {
        $token = AccessToken::where('token', $request->input('access_token'))->first();
        $people = People::find($token->user_id);
        $username = $people->name;
        $UserId = $people->id;
        $file = File::find($fileId);
        $date = new DateTime($file->LastModifiedTime);
        $date->modify('+5 hour');

        return response()->json([
            'BaseFileName' => $file->filename,
			'OwnerId' => 1,
			'Size' => $file->filesize,
			'UserId' => $UserId,
			'UserFriendlyName' => $username,
			'UserCanWrite' => $token->UserCanWrite,
			'PostMessageOrigin' => '*',
            'HidePrintOption' => $token->HidePrintOption,
			'DisablePrint' => $token->DisablePrint,
            'HideSaveOption' => false,
            'HideExportOption' => $token->HideExportOption,
			'DisableExport' => false,
			'DisableCopy' => $token->DisableCopy,
			'EnableOwnerTermination' => false,
            'LastModifiedTime' => $date->format('c'),
        ], 200);
    }

    public function getFile(Request $request, $fileId) {
        $file = File::find($fileId);
        $pathToFile = storage_path("app/" . $file->path);
        $headers = [
            "Content-Type: application/octet-stream",
        ];

        return response()->download($pathToFile, $file->originalname, $headers);
    }

    public function putFile(Request $request, $fileId) {
        $token = AccessToken::where('token', $request->input('access_token'))->first();
        if ($token->UserCanWrite) {
            $file = File::find($fileId);
            $pathToFile = storage_path("app/" . $file->path);
            file_put_contents($pathToFile, file_get_contents('php://input'));
            $Modify = new DateTime('NOW');
            $file->LastModifiedTime = $Modify;
            $file->save();
            $Modify->modify('+5 hour');
            if (isset($file->affair_id))
            {
                $affair = AffairReception::find($file->affair_id);
                $noti_users = AccessToken::join('users', 'users.id', '=', 'access_tokens.user_id')
                    ->select('users.username', 'access_tokens.token')
                    ->where('file_id', $fileId)
                    ->get()
                    ->toArray();
                $user = User::find($token->user_id);
                foreach ($noti_users as $notiuser)
                {
                    if ($notiuser['token'] != $request->input('access_token'))
                    {
                        $username = $notiuser['username'];
                        broadcast(new modifiedDoc($username, $affair, $user->username));
                    }
                }

                $affair_response = AffairReceptionResponse::where('folio_response', $affair->folio)
                    ->first();
                if ($affair_response) {
                    $affair_origin_id = AffairReception::where('folio', $affair_response->folio_origin)->first()->id;
                    $turn = Turn::where('affair_id', $affair_origin_id)
                    ->where('user_id', $token->user_id)
                    ->first();

                    if (isset($turn)) {
                        $turn_log = new TurnLog();
                        $turn_log->user_id = $token->user_id;
                        $turn_log->turn_movement_id = 3;
                        $turn_log->turn_id = $turn->id;
                        $turn_log->save();
                    }
                }
            }
            return response()->json([
                'LastModifiedTime' => $Modify->format('c'),
            ], 200);
        } else {
            return response()->json([
                'message' => "Unauthenticated.",
            ], 401);
        }
    }

}
