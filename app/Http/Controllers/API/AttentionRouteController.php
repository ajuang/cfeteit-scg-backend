<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\AttentionRoute;
use App\Models\AttentionStep;
use App\Models\AttentionPeople;
use App\Models\User;
use App\Models\Catalogs\CatKeyword;
use Illuminate\Http\Request;
use Auth;

class AttentionRouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');
            $routes = AttentionRoute::search($search)->orderBy('created_at', 'desc')->paginate($rowsPerPage);
            return response()->json([
                'success' => true,
                'routes' => $routes,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            DB::beginTransaction();
            $route = new AttentionRoute();
            $route->fill($request->all());
            $route->created_by_user_id = Auth::user()->id;
            $route->save();

            foreach ($request['keyWords'] as $key) {
                $keyword = new CatKeyword();
                $keyword->name = $key;
                $keyword->attention_route_id = $route->id;
                $keyword->save();
            }

            foreach ($request['steps'] as $step) {
                $steps = new AttentionStep();
                $steps->fill($step);
                $steps->attention_route_id = $route->id;
                $steps->save();

                foreach ($step['tasks'] as $task) {
                    $tasks = new AttentionPeople();
                    $tasks->fill($task);
                    $tasks->unit_id = $task['unit']['id'];
                    $tasks->attention_step_id = $steps->id;
                    $tasks->save();
                }
            }

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $route = AttentionRoute::with(['keywords'])->where('id',$id)->first();
            $steps = AttentionStep::where('attention_route_id',$route->id)->get();
            $i = 0;
            foreach($steps as $step) {
                $tasks = AttentionPeople::where('attention_step_id',$step->id)->get();
                $steps[$i]->tasks = $tasks;
                $i = $i + 1;
            }
            $route['columns'] = $steps;
            return response()->json([
                'success' => true,
                'route' => $route,
			]);
        }catch (\Exception $e) {
            DB::rollback();
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $route = AttentionRoute::find($id);
            $route->fill($request->all());
            $route->save();

            $taskIDs = [];
            $stepIDs = []; 
            foreach ($request['steps'] as $step) {
                if (array_key_exists('created_at', $step)) {
                    $steps = AttentionStep::find($step['id']);
                } else {
                    $steps = new AttentionStep();
                }
                $steps->fill($step);
                $steps->attention_route_id = $route->id;
                array_push($stepIDs, $steps->id);
                $steps->save();
                foreach ($step['tasks'] as $task) {
                    if (array_key_exists('created_at', $task)) {
                        $tasks = AttentionPeople::find($task['id']);
                    } else {
                        $tasks = new AttentionPeople();
                    }
                    $tasks->fill($task);
                    $tasks->attention_step_id = $steps->id;
                    array_push($taskIDs, $tasks->id);
                    $tasks->save();
                }
            }

            $arrayStep = AttentionStep::where('attention_route_id', $route->id)->pluck('id')->toArray();
            $deleteTasks = AttentionPeople::whereIn('attention_step_id', $arrayStep)->whereNotIn('id', $taskIDs)->delete();
            $deleteSteps = AttentionStep::where('attention_route_id', $route->id)->whereNotIn('id', $stepIDs)->delete();
            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $route = AttentionRoute::find($id);
            $route->delete();
            DB::commit();
            

            return response()->json([
                'success' => true,
            ]);
        } catch (\Exception $e) {
            DB::rollBack();
            error_log($e->getMessage());
            return response('',500);
        }
    }
}
