<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Validation\Validator;
use App\Models\EmailConfiguration;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\People;
use App\Models\Position;
use App\Models\Structure;
use App\Models\User;
use App\Models\Entity;


class PeopleCopyController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $user = Auth::user()->id;
            $entityData = Entity::join('cat_units','entities.id','=','cat_units.entity_id')
                ->join('positions','cat_units.id','=','positions.cat_unit_id')
                ->join('people','positions.id','=','people.position_id')
                ->where('people.id','=',$user)->select('entities.id')->get();
            $decode = json_decode($entityData,true);
            $entityId = $decode[0]['id'];
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');
            $peopleE = People::search($search)
                ->join('positions','people.position_id','=','positions.id')
                ->join('cat_units','positions.cat_unit_id','=','cat_units.id')
                ->join('entities','cat_units.entity_id','=','entities.id')
                ->where('entities.id',$entityId)
                ->select('people.name','people.last_name', 'people.second_lastname','people.email')
                ->orderBy('people.created_at', 'asc')->paginate($rowsPerPage);
            return response()->json([
                'success' => true,
                'people' => $peopleE,
                // 'peopleE' => $peopleE,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        // $list = CatSchedule::all();
        // return response()->json(['success'=>true,'data'=>['schedules'=>$list]]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            DB::beginTransaction();
            $people = new People();
            $people->fill($request->all());
            $user1 = $request->input('username');
            $user = User::where('username',$user1)->first();
            // error_log(print_r($user, true));
            $people->user_id = $user->id;
            $people->save();
            // $people->user()->sync($user);
           
            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $people = People::find($id);
            return response()->json([
                'success' => true,
                'people' => $people,
			]);
        }catch (\Exception $e) {
            DB::rollback();
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $people = People::find($id);
            // error_log(print_r($people, true));
            $people->fill($request->all());
            $people->save();   
            // $user = $request->input('user');
            // $people->user()->sync($user);

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $people = People::where('id',$id)->first();
            $people->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            error_log($e->getMessage());
            return response('',500);
        }
    }

    public function allPeople(Request $request)
    {
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');
            $allPeople = People::join('users','people.user_id','=','users.id')
            ->select('users.id',DB::raw("CONCAT(people.name,' ',people.last_name,' ',people.second_lastname) as fullname"),
            'people.email','users.is_active','users.created_at')->search($search)->orderBy('users.id')->paginate($rowsPerPage);
            return response()->json([
                'success' => true,
                'allPeople' => $allPeople,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function allPeopleRequest(Request $request)
    {
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');
            $allPeople = People::join('users','people.user_id','=','users.id')
            ->join('positions','people.position_id','=','positions.id')
            ->join('cat_units','positions.cat_unit_id','=','cat_units.id')
            ->join('entities','cat_units.entity_id','=','entities.id')
            ->where('entities.id', $request->input('entity_id'))
            ->select('users.id',DB::raw("CONCAT(people.name,' ',people.last_name,' ',people.second_lastname) as fullname"),
            'people.email','users.is_active','users.created_at')->orderBy('created_at')->paginate(100);
            return response()->json([
                'success' => true,
                'allPeople' => $allPeople,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
    
}
