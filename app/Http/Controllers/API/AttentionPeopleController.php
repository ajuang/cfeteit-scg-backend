<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\AttentionPeople;
use App\Models\AttentionStep;
use App\Models\Catalogs\CatAttentionType;
use App\Models\Catalogs\CatInstruction;
use App\Models\Catalogs\CatUnit;
use App\Models\User;
use Illuminate\Http\Request;

class AttentionPeopleController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');
            $attentionsPeople = AttentionPeople::search($search)->orderBy('created_at', 'desc')->paginate($rowsPerPage);
            return response()->json([
                'success' => true,
                'attentionsPeople' => $attentionsPeople,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            DB::beginTransaction();
            $attentionPeople = new AttentionPeople();
            $attentionPeople->fill($request->all());

            $step1 = $request->input('name');
            $step = AttentionStep::where('name',$step1)->first();
            // error_log(print_r($step, true));
            $attentionPeople->attention_step_id = $step->id;

            $unit1 = $request->input('name');
            $unit = CatUnit::where('name',$unit1)->first();
            // error_log(print_r($unit, true));
            $attentionPeople->unit_id = $unit->id;

            $attentionType1 = $request->input('name');
            $attentionType = CatAttentionType::where('name',$attentionType1)->first();
            // error_log(print_r($attentionType, true));
            $attentionPeople->attention_type_id = $attentionType->id;

            $instruction1 = $request->input('name');
            $instruction = CatInstruction::where('name',$instruction1)->first();
            // error_log(print_r($instruction, true));
            $attentionPeople->instruction_id = $instruction->id;

            $user1 = $request->input('name');
            $user = User::where('name',$user1)->first();
            // error_log(print_r($user, true));
            $attentionPeople->user_id = $user->id;

            $attentionPeople->save();

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $attentionPeople = AttentionPeople::with([
                'unit_id',
                'attention_type_id',
                'instruction_id',
                'user_id',
                'has_privileges'])
                ->where('id',$id)->first();
            return response()->json([
                'success' => true,
                'attentionPeople' => $attentionPeople,
			]);
        }catch (\Exception $e) {
            DB::rollback();
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $attentionPeople = AttentionPeople::find($id);
            $attentionPeople->fill($request->all());
            $attentionPeople->save();

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $attentionPeople = AttentionPeople::with([
                'unit_id',
                'attention_type_id',
                'instruction_id',
                'user_id',
                'has_privileges'])
                ->where('id',$id)->first();
            //error_log(print_r($scheduleDay, true));
            $attentionPeople->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            error_log($e->getMessage());
            return response('',500);
        }
    }
}
