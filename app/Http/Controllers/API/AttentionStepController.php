<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\AttentionRoute;
use App\Models\AttentionStep;
use App\Models\Catalogs\CatStepType;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AttentionStepController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');
            $attentionSteps = AttentionStep::search($search)->orderBy('created_at', 'desc')->paginate($rowsPerPage);
            return response()->json([
                'success' => true,
                'attentionSteps' => $attentionSteps,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            DB::beginTransaction();
            $attentionStep = new AttentionStep();
            $attentionStep->fill($request->all());

            $stepType1 = $request->input('name');
            $stepType = CatStepType::where('name',$stepType1)->first();
            // error_log(print_r($stepType, true));
            $attentionStep->step_type_id = $stepType->id;

            $attentionRoute1 = $request->input('name');
            $attentionRoute = AttentionRoute::where('name',$attentionRoute1)->first();
            // error_log(print_r($attentionRoute, true));
            $attentionStep->attention_route_id = $attentionRoute->id;

            $attentionStep->save();

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $attentionStep = AttentionStep::with([
                'step_type_id',
                'name',
                'order',
                'attention_route_id',
                'is_finisher'])
                ->where('id',$id)->first();
            return response()->json([
                'success' => true,
                'attentionStep' => $attentionStep,
			]);
        }catch (\Exception $e) {
            DB::rollback();
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $attentionStep = AttentionStep::find($id);
            $attentionStep->fill($request->all());
            $attentionStep->save();

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $attentionStep = AttentionStep::with([
                'step_type_id',
                'name',
                'order',
                'attention_route_id',
                'is_finisher'])
                ->where('id',$id)->first();
            //error_log(print_r($scheduleDay, true));
            $attentionStep->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            error_log($e->getMessage());
            return response('',500);
        }
    }
}
