<?php

namespace App\Http\Controllers\API;

use App\Models\People;
use App\Models\UserLog;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\AffairReception;
use App\Models\AffairReceptionFrom;
use App\Models\GeneralLog;
use App\Models\File;
use Illuminate\Support\Facades\Auth;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


class AffairReceptionFromController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    // return 1;
    try {
      $rowsPerPage = $request->input('rowsPerPage');
      $search = $request->input('search');

      $affairs = AffairReception::search($search)
        ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
        ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
        ->select('affair_receptions.folio','cat_priorities.name as priorities','cat_doc_types.name as doc_type','affair_receptions.document_date')
        ->orderBy('affair_receptions.created_at', 'desc')
        ->paginate($rowsPerPage);

      // $affairs = DB::table('affair_receptions')
      // ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
      // ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
      // ->select('affair_receptions.folio','cat_priorities.name as priorities','cat_doc_types.name as doc_type','affair_receptions.document_date')
      // ->get();

      return response()->json([
        'success' => true,
        'affairs' => $affairs,
      ]);
    } catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
// 
//         $list = AffairReception::all();
//         return response()->json(['success'=>true,'data'=>['schedules'=>$list]]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
    try {
      DB::beginTransaction();
      $affairId = AffairReception::latest('id')->first()->id;
      $toPeople = $request->input('to');
      
      //RESPONSABLE Copia Espejo
      $responsable = People::where('user_id', \Illuminate\Support\Facades\Auth::user()->id)->first();
      $idUser = Auth::user()->id;
      if($responsable->responsable != null) {
        $idUser = $responsable->responsable;
      }
      
      if (isset($toPeople)) {
        foreach ($toPeople as $people) {
          $affairFrom = new AffairReceptionFrom();
          $affairFrom->affair_receptions_id = $affairId;
          $affairFrom->receiver_id = $people['id'];
          $affairFrom->participation_types_id = 1;
          $affairFrom->sender_id = $idUser;
          $affairFrom->save();
        }
      }
      $ccpPeople = $request->input('ccp');
      if (isset($ccpPeople)) {
        foreach ($ccpPeople as $people) {
          $affairFrom = new AffairReceptionFrom();
          $affairFrom->affair_receptions_id = $affairId;
          $affairFrom->receiver_id = $people['id'];
          $affairFrom->participation_types_id = 2;
          $affairFrom->sender_id = $idUser;
          $affairFrom->save();
        }
      }

      FileUploadController::generateDoc($affairId);

      $idGeneralLog = GeneralLog::latest('id')->first()->id;
      $userLog = new UserLog();
      $userLog->user_id = $idUser;
      $userLog->general_log_id = $idGeneralLog;
      $userLog->save();
      DB::commit();

      return response()->json([
        'success' => true,
        'message' => '',
        'affairFrom'  => $affairFrom
      ], 200);
    } catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    try {
      $affair = AffairReception::where('id',$id)->first();
      return response()->json([
        'success' => true,
        'affair' => $affair,
      ]);
    }catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      DB::beginTransaction();
      $affair = AffairReception::find($id);
      $affair->fill($request->all());
      $affair->save();

      DB::commit();

      return response()->json([
        'success' => true,
        'message' => '',
      ], 200);
    } catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      DB::beginTransaction();
      $affair = AffairReception::where('id',$id)->first();
      //error_log(print_r($scheduleDay, true));
      $affair->delete();

      DB::commit();
    } catch (\Exception $e) {
      DB::rollBack();
      error_log($e->getMessage());
      return response('',500);
    }
  }

  public function getAffairFromByIdAffair($id)
  {
    try {
      $affairFromData = AffairReceptionFrom::join('people','people.user_id','=','affair_reception_froms.receiver_id')
        ->join('positions','positions.id', '=', 'people.position_id')
        ->where('affair_receptions_id','=',$id)
        ->where('participation_types_id','=',1)
        ->select(DB::raw("CONCAT(people.name, ' ', people.last_name) AS full_name"), 'positions.name as position_name', 'affair_reception_froms.receiver_id as id', DB::raw("false AS hover"))
        ->get();
      $affairFromDataccp = AffairReceptionFrom::join('people','people.user_id','=','affair_reception_froms.receiver_id')
        ->join('positions','positions.id', '=', 'people.position_id')
        ->where('affair_receptions_id','=',$id)
        ->where('participation_types_id','=',2)
        ->select(DB::raw("CONCAT(people.name, ' ', people.last_name) AS full_name"), 'positions.name as position_name', 'affair_reception_froms.receiver_id as id', DB::raw("false AS hover"))
        ->get();
      return response()->json([
        'success' => true,
        'affairsTo' => $affairFromData,
        'affairsCcp' => $affairFromDataccp
      ]);
    } catch (\Exception $e) {
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
  }
}
