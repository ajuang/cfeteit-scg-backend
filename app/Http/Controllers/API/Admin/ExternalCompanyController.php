<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Models\EmailConfiguration;
use App\Models\ExternalCompany;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ExternalCompanyController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');
            $externalCompany = ExternalCompany::search($search)
            ->select('externalcompanies.*')->orderBy('externalcompanies.created_at', 'asc')->paginate($rowsPerPage);
            return response()->json([
                'success' => true,
                'externalcompany' => $externalCompany,
                // 'peopleE' => $peopleE,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }

        // $list = CatSchedule::all();
        // return response()->json(['success'=>true,'data'=>['schedules'=>$list]]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            DB::beginTransaction();
            $externalCompany = new ExternalCompany();
            $externalCompany->fill($request->all());
            $externalCompany->save();
            // $people->user()->sync($user);
           
            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
                'externalcompany' => $externalCompany
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $people = People::find($id);
            return response()->json([
                'success' => true,
                'people' => $people,
			]);
        }catch (\Exception $e) {
            DB::rollback();
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $externalCompany = ExternalCompany::find($id);
            // error_log(print_r($people, true));
            $externalCompany->fill($request->all());
            $externalCompany->save();   
            // $user = $request->input('user');
            // $people->user()->sync($user);

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $externalCompany = ExternalCompany::where('id',$id)->first();
            $externalCompany->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            error_log($e->getMessage());
            return response('',500);
        }
    }
}
