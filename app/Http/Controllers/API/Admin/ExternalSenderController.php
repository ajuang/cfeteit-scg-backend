<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Models\ExternalSender;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ExternalSenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            // dd($request->all());
            $externalSender = new ExternalSender();
            $externalSender->external_people_id = $request->input('external_people_id');
            $externalSender->external_company_id = $request->input('external_company_id');
            $externalSender->sender_entity_id = $request->input('sender_entity_id');
            $externalSender->sender_type_id = $request->input('sender_type_id');
            $externalSender->save();
            // $people->user()->sync($user);
            DB::commit();
           
            return response()->json([
                  'success' => true,
                  'message' => '',
                  'data' => $externalSender
              ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ExternalSender  $externalSender
     * @return \Illuminate\Http\Response
     */
    public function show(ExternalSender $externalSender)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ExternalSender  $externalSender
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $externalSender = ExternalSender::find($id);
            return response()->json([
                'success' => true,
                'externalSender' => $externalSender,
			]);
        }catch (\Exception $e) {
            DB::rollback();
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ExternalSender  $externalSender
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $externalSender = ExternalSender::find($id);
            $externalSender->sender_type_id = $request->input('sender_type_id');
            if($request->input('sender_type_id') == 1){
                $externalSender->external_company_id = $request->input('external_company_id');
                $externalSender->external_people_id = null;
                $externalSender->sender_entity_id = null;
            }
            else if($request->input('sender_type_id') == 2){
                $externalSender->external_people_id = $request->input('external_people_id');
                $externalSender->external_company_id = null;
                $externalSender->sender_entity_id = null;
            }
            else if($request->input('sender_type_id') == 3){
                $externalSender->sender_entity_id = $request->input('sender_entity_id');
                $externalSender->external_people_id = null;
                $externalSender->external_company_id = null;
            }
            // $externalSender->external_people_id = $request->input('external_people_id');
            // $externalSender->external_company_id = $request->input('external_company_id');
            // $externalSender->sender_entity_id = $request->input('sender_entity_id');
            $externalSender->save();
            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
                'externalSender' => $externalSender
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ExternalSender  $externalSender
     * @return \Illuminate\Http\Response
     */
    public function destroy(ExternalSender $externalSender)
    {
        //
    }
}
