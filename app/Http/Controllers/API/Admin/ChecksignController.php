<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use \GuzzleHttp\Client;

class ChecksignController extends Controller
{
  public function getChecksignKey(Client $client)
  {
    try {
      $response = $client->request('GET', 'https://alfa.gob.mx/checksign/api/v2/step-one/obtenerLlave', [
        'headers' => [
          'Content-Type'     => 'application/json',
          'Authorization'      => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpa3MiOiI5IiwicGtzIjoiYjc1ZDQ1YzBkMjk1MDk4ODI3N2I0MzY5YThhOTk4YjEiLCJjYXQiOjE2MzA2MDE0MjEsImV4cCI6MTY2MjEzNzQyMX0.rYj_PKFqMYdvjWSgjAxYeInqDpiZIwxRlHO2QmqugTU'
        ]
      ]);
      $data = json_decode($response->getBody()->getContents());
      return response()->json([
        'success' => true,
        'checksignKey' => $data,
      ]);
    } catch (\Exception $e) {
      DB::rollback();
      return response()->json([
          'success' => false,
          'message' => $e->getMessage()
      ]);
    }
  }

  public function postChecksign(Client $client, Request $request)
  {
    try {
      $idVinculo = $request->input('idVinculo');
      $url = 'https://alfa.gob.mx/checksign/api/v2/step-two/firmaElectronica/' . $idVinculo;
      $response = $client->request('POST', $url, [
        'headers' => [
          'Content-Type'     => 'application/json',
          'Authorization'      => 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpa3MiOiI5IiwicGtzIjoiYjc1ZDQ1YzBkMjk1MDk4ODI3N2I0MzY5YThhOTk4YjEiLCJjYXQiOjE2MzA2MDE0MjEsImV4cCI6MTY2MjEzNzQyMX0.rYj_PKFqMYdvjWSgjAxYeInqDpiZIwxRlHO2QmqugTU'
        ],
        'body' => json_encode([
          'serial' => $request->input('serialBase64'),
          'rfc' => $request->input('rfc'),
          'cadena_original' => $request->input('cadena'),
          'firma_electronica' => $request->input('Efirma')
        ])
      ]);

      $checksignResponse = json_decode($response->getBody()->getContents());
      $urlVerify = $checksignResponse->url_verificacion;
      $qrCode = \QrCode::size(600)
                ->format('png')
                ->generate($urlVerify, storage_path('app/test.png'));


      return response()->json([
        'success' => true,
        'checksignResponse' => $checksignResponse,
      ]);

    } catch (\Exception $e) {
      DB::rollback();
      return response()->json([
          'success' => false,
          'message' => $e->getMessage()
      ]);
    }
  }
}
