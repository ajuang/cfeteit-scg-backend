<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Entity;

class EntityController extends Controller
{
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');
            $entities = Entity::search($search)->orderBy('created_at', 'desc')->paginate($rowsPerPage);
            
            return response()->json([
                'success' => true,
                'entities' => $entities,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $entity = new Entity();
            $entity->fill($request->all());
            $entity->save();
            DB::commit();

            return response()->json([
				'success' => true,
				'message' => '',
			], 200);
        } catch (\Exception $e) {
            DB::rollback();
            error_log($e->getMessage());
            return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $entity = Entity::find($id);
            return response()->json([
                'success' => true,
                'entity' => $entity,
			      ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
              'success' => false,
              'message' => $e->getMessage()
            ]); 
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $entity = Entity::find($id);
            $entity->fill($request->all());
            $entity->save();

            DB::commit();

            return response()->json([
				'success' => true,
				'message' => '',
			], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        } 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $entity = Entity::where('id',$id)->first();
            $entity->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            error_log($e->getMessage());
            return response('',500);
        }
    }
    
    public function getEntityByAuth() {
      try {
        $user = Auth::user()->id;
        $entity = DB::table('entities')
          ->join('cat_units','entities.id','=','cat_units.entity_id')
          ->join('positions','cat_units.id','=','positions.cat_unit_id')
          ->join('people','positions.id','=','people.position_id')
//          ->join('user','people.user_id','=','user.id')
          ->where('people.user_id', $user)
          ->select('entities.id')->get();
        return response()->json([
          'success' => true,
          'entity' => $entity,
        ]);
      } catch (\Exception $e) {
        DB::rollback();
        return response()->json([
          'success' => false,
          'message' => $e->getMessage()
        ]);
      }
    }
    
}
