<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Models\Entity;
use App\Models\Position;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\People;
use App\Models\Structure;
use App\Models\StructureUser;

class StructureController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $structure = new Structure();
            $structure->fill($request->all());
            // $entity1 = $request->input('name');
            // $entity = Entity::where('name',$entity1)->first();
            // $structure->position_id = $entity->id;
            // $position1 = $request->input('name');
            // $position = Position::where('name',$position1)->first();
            // $structure->position_id = $position->id;
            // $positionR1 = $request->input('name');
            // $positionR = Position::where('name',$positionR1)->first();
            // $structure->position_to_report_id = $positionR->id;
            $structure->save();
            DB::commit();

            return response()->json([
				'success' => true,
				'message' => '',
			], 200);
        } catch (\Exception $e) {
            DB::rollback();
            error_log($e->getMessage());
            return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $structure = Structure::with(['position'])->where('id', $id)->first();
            $structureUser = StructureUser::where('structure_id', $structure->id)->first();
            $structure->people = isset($structureUser) ? People::select('id', 'name','last_name','second_lastname')->where('id', $structureUser->people_id)->first() : ['name' => 'Sin asignar'];

            $structure['children'] = static::child($structure);
            return response()->json([
                'success' => true,
                'structure' => $structure,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public static function child($father)
    {
        $father['children'] = Structure::with(['position'])->where('position_to_report_id', $father->position_id)->get();
        if (isset($father['children'])){
            foreach ($father['children'] as $node) {
                $structureUser = StructureUser::where('structure_id', $node->id)->first();
                $node->people = isset($structureUser) ? People::select('id', 'name','last_name','second_lastname')->where('id', $structureUser->people_id)->first() : ['name' => 'Sin asignar'];
                $node['children'] = static::child($node);
            }
            return $father['children'];
        } else {
            return [];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $nodeIDs = static::updateNode($request);
            array_push($nodeIDs, 1);
            $deleteStructure = StructureUser::whereNotIn('structure_id', $nodeIDs)->delete();
            $deleteNodes = Structure::whereNotIn('id', $nodeIDs)->delete();
            $structureUser = StructureUser::where('structure_id', $id)->first();
            if (isset($structureUser)) {
                error_log(print_r($request->people['name'], true));
                if ($request->people['name'] == 'Sin asignar') {
                    $structureUser->delete();
                } else {
                    $structureUser->people_id = $request->people['id'];
                    $structureUser->save();
                }
            } else {
                if (isset($request->people['id'])) {
                    $structureUser = new StructureUser();
                    $structureUser->people_id = $request->people['id'];
                    $structureUser->is_active = true;
                    $structureUser->active_date = date("Y-m-d H:i:s");
                    $structureUser->structure_id = $id;
                    $structureUser->save();
                }
            }
            StructureController::newNode($request);
            DB::commit();

            return response()->json([
				'success' => true,
				'message' => '',
			], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        } 

    }

    public static function updateNode($father, $nodeIDs=[])
    {
        if(isset($father['children'])) {
            foreach($father['children'] as $node) {
                if (array_key_exists('created_at', $node)) {
                    array_push($nodeIDs, $node['id']);
                    
                    $nodeIDs = $nodeIDs + static::updateNode($node, $nodeIDs);
                }
            }
        }
        return $nodeIDs;
    }

    public static function newNode($father)
    {
        if(isset($father['children'])) {
            foreach($father['children'] as $node) {
                if (array_key_exists('created_at', $node)) {
                    $structureUser = StructureUser::where('structure_id', $node['id'])->first();
                    if (isset($structureUser)) {
                        if ($node['people']['name'] == 'Sin asignar') {
                            $structureUser->delete();
                        } else {
                            $structureUser->people_id = $node['people']['id'];
                            $structureUser->save();
                        }
                    } else {
                        if (isset($node['people']['id'])) {
                            $structureUser = new StructureUser();
                            $structureUser->people_id = $node['people']['id'];
                            $structureUser->is_active = true;
                            $structureUser->active_date = date("Y-m-d H:i:s");
                            $structureUser->structure_id = $node['id'];
                            $structureUser->save();
                        }
                    }
                    StructureController::newNode($node);
                } else {
                    $newNode = new Structure();
                    $newNode->entity_id = 1;
                    $newNode->position_id = $node['position_id'];
                    $newNode->position_to_report_id = $father['position_id'];
                    $newNode->save();
                    if ($node['people']['name'] != 'Sin asignar') {
                        $newStrNode = new StructureUser();
                        $newStrNode->people_id = $node['people']['id'];
                        $newStrNode->is_active = true;
                        $newStrNode->active_date = date("Y-m-d H:i:s");
                        $newStrNode->structure_id = $newNode->id;
                        $newStrNode->save();
                    }
                    StructureController::newNode($node);
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $structure = Structure::where('id',$id)->first();
            $structure->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            error_log($e->getMessage());
            return response('',500);
        }
    }
}
