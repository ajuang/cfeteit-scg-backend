<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\File;
use App\Models\AccessToken;
use App\Models\AffairReception;
use App\Models\AffairReceptionFrom;
use App\Models\AffairReceptionResponse;
use App\Models\Turn;
use App\Models\User;
use App\Models\People;
use App\Models\StructureUser;
use App\Models\Structure;
use App\Models\Position;
use App\Models\TemplateLog;
use App\Models\Catalogs\CatDocType;
use Carbon\Carbon;
use Auth;
use Str;
use PDF;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Http;
use App\Events\SeenDoc;
use App\Models\TurnLog;
use Illuminate\Support\Facades\DB;

class FileUploadController extends Controller
{
  	static $DOC_FILE_TYPE = "pdf";
	static $IMG_FILE_TYPE = "img";
	static $XML_FILE_TYPE = "xlsx";
	static $MODULES = ['employee','employee_img','hiringMemo','contracts','invoice','provider','signureReport','template','affair'];

	public function upload(Request $request) {

    // return 1;
			$file = $request->file('document');
			$module = $request->input('module');
			$fileType = $request->input('fileType');
      // echo $file;
      // echo $fileType;
      // echo $doc_id;
			/* Se verifica que exista el valor en el array para una manipulación del almacenamiento*/
			if(in_array($module, static::$MODULES)) {
				$fileUploaded = static::storeFile($file, $module, $fileType, $request);
				return response()->json([
					'success' => true,
					'file' => $fileUploaded,
				], 200);
			}
		try{
		}catch (Exception $e) {
			return response()->json([
				'message' => 'No se pudo completar la acción',
				'error' => $e
			], 500);
		}
	}
	
    public static function storeFile($fileInput, $module, $fileType, Request $request){
		if (!isset($fileInput)) {
			return null;
		}
		if ($module == 'template') {
			$fileName = Str::random(10) . "plantilla" . date('Ymdhh') . "." . $fileInput->getClientOriginalExtension();
			$doc_id = $request->input('doc_id');
			if (isset($doc_id)) {
				$cat_doc = CatDocType::find($doc_id);
				if (isset($cat_doc->file_id)) {
					$file = File::find($cat_doc->file_id);
					Storage::delete($file->path);
				} else {
					$file = new File;
				}
			} else {
				$file = new File;
			}
			$file->user_id = Auth::user()->id;
			$file->filetype = $fileType;
			$path = $fileInput->storeAs($module, $fileName);
			$file->filename = $fileName;
			$file->originalname = $fileInput->getClientOriginalName();
			$file->path = $path;
			$file->filesize = $fileInput->getSize();
			$file->LastModifiedTime = Carbon::now();
			$file->save();
			$log = new TemplateLog();
			$log->dateHour_template = Carbon::now();
			$log->user_id = Auth::user()->id;
			$log->save();
			return $file;
		}
		$fileName =  Str::random(40) . date('Ymdhh')  . "." . $fileInput->getClientOriginalExtension();

		$file = new File;
		$file->user_id = Auth::user()->id;
		$affair1 = $request->input('folio');
		$affair = AffairReception::where('folio',$affair1)->first();
		$file->affair_id = $affair->id;
		$path = $fileInput->storeAs($module, $fileName);
		$file->filetype = $fileType;
		$file->path = $path;
		$file->filename = $fileName;
		$file->originalname = $fileInput->getClientOriginalName();
		$file->filesize = $fileInput->getSize();
		$file->LastModifiedTime = Carbon::now();
		$file->save();

		$folio_origin = AffairReceptionResponse::where('folio_response', $affair->folio)->first();
		if ($folio_origin) {
			$affair_origin = AffairReception::where('folio', $folio_origin->folio_origin)->first();
			$turn = Turn::where('affair_id', $affair_origin->id)
				->where('user_id', Auth::user()->id)
				->first();
			error_log(print_r($turn, true));
			if (isset($turn)) {
				$turn_log = new TurnLog();
				$turn_log->user_id = Auth::user()->id;
				$turn_log->turn_movement_id = 4;
				$turn_log->turn_id = $turn->id;
				$turn_log->save();
			}
		}

		if ($module == "affair" && $fileType != 'pdf') {
			$token = new AccessToken();
			$token->token = Str::random(40);
			$token->user_id = Auth::user()->id;
			$token->file_id = $file->id;
			$token->UserCanWrite = true;
			$token->HidePrintOption = false;
			$token->DisablePrint = false;
			$token->HideSaveOption = false;
			$token->HideExportOption = false;
			$token->DisableExport = false;
			$token->DisableCopy = false;
			$token->save();
		}
		return $file;

    }
    
    public function showFile(Request $request,$id){
		$file = File::find($id);
		$pathToFile = storage_path("app/" . $file->path);
		$fileName= $file->originalname;

		if($request->input('download') == "1"){
			return response()->download($pathToFile);
		}
		
		return response()->download(
			$pathToFile,
			$fileName,
			[
				'Content-Type' => 'application/pdf',
				'Content-Disposition' => 'inline; filename="'.$fileName.'"'
			],
			'inline'
		);
	}

	public function showAffair(Request $request,$id){
		$file = File::where('affair_id', $id)->where('is_affair', true)->first();
		$pathToFile = storage_path("app/" . $file->path);
		$fileName= $file->originalname;
		try {
			DB::beginTransaction();
			$affair = AffairReception::find($id);
			if (Auth::user()->id != $affair->user_id) {
				$affair->status_id = 4;
				$affair->save();
				$affair_people = AffairReceptionFrom::where('affair_receptions_id', $id)
				->where('receiver_id', Auth::user()->id)
				->first();
				
				if (isset($affair_people)) {
					if ($affair_people->seen_date == null) {
						$affair_people->seen_date = Carbon::now();
						$affair_people->save();
						$user_sender = User::find($affair->user_id);
						broadcast(new SeenDoc($user_sender->username, $affair, Auth::user()->username));
					}
				} else {
					$turn = Turn::where('affair_id', $id)
						->where('user_id', Auth::user()->id)
						->where('status_id', 1)
						->first();
					if ($turn->seen == false) {
						$turn->seen = true;
						$turn->save();

						$turn_log = new TurnLog();
						$turn_log->user_id = Auth::user()->id;
						$turn_log->turn_movement_id = 2;
						$turn_log->turn_id = $turn->id;
						$turn_log->save();

						$parent_turn = Turn::find($turn->parent_turn_id);
						$user_sender = User::find($parent_turn->user_id);
						broadcast(new SeenDoc($user_sender->username, $affair, Auth::user()->username));
					}
				}
			}
			DB::commit();
			if($request->input('download') == "1"){
				return response()->download($pathToFile);
			}
			
			return response()->download(
				$pathToFile,
				$fileName,
				[
					'Content-Type' => 'application/pdf',
					'Content-Disposition' => 'inline; filename="'.$fileName.'"'
				],
				'inline'
			);
		}catch (\Exception $e) {
			error_log(print_r($e->getMessage(), true));
            DB::rollback();
        }
	}
	
	public function getFile(Request $request, $id){
		$file = File::find($id);
		$pathToFile = storage_path("app/" . $file->path);
		$width = $request->input('w'); 
		$height = $request->input('h');
		$img = \Image::make($pathToFile);
		if($width != '' && $height != ''){
			$img->resize($request->input('w'), $request->input('h'));
		}
		return $img->response('jpg');
	}
	
	public function getTemplate(Request $request){
		try{
			$file = File::find(1);
			$pathToFile = storage_path("app/" . $file->path);
			$template = new \PhpOffice\PhpWord\TemplateProcessor( $pathToFile );
			$tempFile = tempnam(sys_get_temp_dir(), 'PHPWord');
			$template->saveAs($tempFile);

			$headers = [
				"Content-Type: application/octet-stream",
			];

			return response()->download($tempFile, 'plantilla.docx', $headers)->deleteFileAfterSend( true);
		} catch (\PhpOffice\PhpWord\Exception\Exception $e) {
			return response()->json([
				'message' => 'No se pudo completar la acción',
				'error' => $e
			], 500);
		}
	}

	public function getCustomTemplate($id){
		try{
			$affair = AffairReception::find($id);
			$cat_doc = CatDocType::find($affair->cat_doc_types_id);
			$file = File::find($cat_doc->file_id);
			$pathToFile = storage_path("app/" . $file->path);
			$fileName =  Str::random(40) . date('Ymdhh')  . "." . "docx";
			$path = storage_path("app/" . "affair/" . $fileName);
			$template = new \PhpOffice\PhpWord\TemplateProcessor( $pathToFile );

			$from = AffairReceptionFrom::where('affair_receptions_id', $id)->first();
			$people = People::find($from->receiver_id);
			$name = $people->name . " " . $people->last_name . " " . $people->second_lastname;
			$structure_user = StructureUser::where('people_id', $from->receiver_id)->first();
			$structure = Structure::find($structure_user->structure_id);
			$position = Position::with(['unit'])->find($structure->position_id);
			$date = Carbon::createFromFormat('Y-m-d', $affair->document_date);
			$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
			$mes = $meses[($date->format('n')) - 1];
			$dateFormat = $date->day . " de " . $mes . " de " . $date->year;
			$template->setValue( 'FECHA', $dateFormat);
			$template->setValue( 'PUESTO', $position->name);
			$template->setValue( 'FOLIO', $affair->folio);
			$template->setValue( 'AREA', $position['unit']->name);
			$template->setValue('NOMBRE', $name);

			$template->saveAs($path);

			$fileCustom = new File();
			$fileCustom->user_id = Auth::user()->id;
			$fileCustom->affair_id = $id;
			$fileCustom->filetype = "docx";
			$fileCustom->path = "affair/" . $fileName;
			$fileCustom->filename = $fileName;
			$fileCustom->originalname = $file->originalname;
			$fileCustom->filesize = filesize($path);
			$fileCustom->LastModifiedTime = Carbon::now();
			$fileCustom->save();

			$token = new AccessToken();
			$token->token = Str::random(40);
			$token->user_id = Auth::user()->id;
			$token->file_id = $fileCustom->id;
			$token->UserCanWrite = true;
			$token->HidePrintOption = false;
			$token->DisablePrint = false;
			$token->HideSaveOption = false;
			$token->HideExportOption = false;
			$token->DisableExport = false;
			$token->DisableCopy = false;
			$token->save();

			return response()->json([
				'success' => true,
				'file' => $fileCustom,
			], 200);
		} catch (\PhpOffice\PhpWord\Exception\Exception $e) {
			return response()->json([
				'message' => 'No se pudo completar la acción',
				'error' => $e
			], 500); 
		}
	}

	public function showCustomTemplate($id){
		try{
			$pdf = PDF::loadView('pdf_preview_template');
			$pdf->setPaper('A4');

			return $pdf->download('mi-archivo.pdf');

			
		} catch (\PhpOffice\PhpWord\Exception\Exception $e) {
			return response()->json([
				'message' => 'No se pudo completar la acción',
				'error' => $e
			], 500); 
		}
	}

	public function fileAnnexes($id){
		$annexes = File::where('affair_id', $id)
			->where('is_affair', false)
			->get();

		return response()->json([
			'success' => true,
			'annexes' => $annexes,
		], 200);
	}

	public static function generateDoc($id){
		$affair = AffairReception::find($id);
		$cat_doc = CatDocType::find($affair->cat_doc_types_id);
		$file = File::find($cat_doc->file_id);
		$pathToFile = storage_path("app/" . $file->path);
		$fileName =  Str::random(40) . date('Ymdhh')  . "." . "docx";
		$path = storage_path("app/" . "affair/" . $fileName);
		$template = new \PhpOffice\PhpWord\TemplateProcessor( $pathToFile );

		$from = AffairReceptionFrom::where('affair_receptions_id', $id)->first();
		$people = People::find($from->receiver_id);
		$name = $people->name . " " . $people->last_name . " " . $people->second_lastname;
		$structure_user = StructureUser::where('people_id', $from->receiver_id)->first();
		$structure = Structure::find($structure_user->structure_id);
		$position = Position::with(['unit'])->find($structure->position_id);
		$date = Carbon::createFromFormat('Y-m-d', $affair->document_date);
		$meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
		$mes = $meses[($date->format('n')) - 1];
		$dateFormat = $date->day . " de " . $mes . " de " . $date->year;
		$template->setValue( 'FECHA', $dateFormat);
		$template->setValue( 'PUESTO', $position->name);
		$template->setValue( 'FOLIO', $affair->folio);
		$template->setValue( 'AREA', $position['unit']->name);
		$template->setValue('NOMBRE', $name);

		$template->saveAs($path);

		$fileCustom = new File();
		$fileCustom->user_id = Auth::user()->id;
		$fileCustom->affair_id = $id;
		$fileCustom->filetype = "docx";
		$fileCustom->path = "affair/" . $fileName;
		$fileCustom->filename = $fileName;
		$fileCustom->originalname = $file->originalname;
		$fileCustom->filesize = filesize($path);
		$fileCustom->is_affair = true;
		$fileCustom->LastModifiedTime = Carbon::now();
		$fileCustom->save();

		//RESPONSABLE Copia Espejo
    $responsable = People::where('user_id', \Illuminate\Support\Facades\Auth::user()->id)->first();
    if($responsable->responsable != null) {
      $idUser = $responsable->responsable;
      $token = new AccessToken();
      $token->token = Str::random(40);
      $token->user_id = $idUser;
      $token->file_id = $fileCustom->id;
      $token->UserCanWrite = true;
      $token->HidePrintOption = false;
      $token->DisablePrint = false;
      $token->HideSaveOption = false;
      $token->HideExportOption = false;
      $token->DisableExport = false;
      $token->DisableCopy = false;
      $token->save();
    }
		
		$token = new AccessToken();
		$token->token = Str::random(40);
		$token->user_id = Auth::user()->id;
		$token->file_id = $fileCustom->id;
		$token->UserCanWrite = true;
		$token->HidePrintOption = false;
		$token->DisablePrint = false;
		$token->HideSaveOption = false;
		$token->HideExportOption = false;
		$token->DisableExport = false;
		$token->DisableCopy = false;
		$token->save();
	}

	public static function signDoc($file, $sign, $chain, $url){
		$path = storage_path("app/" . $file->path);
		error_log(print_r($path, true));
		$pathQR = storage_path("app/test.png");
		$sign = str_replace("/", "/&#8288;", $sign);


		$doc = \PhpOffice\PhpWord\IOFactory::load($path);
		$section = $doc->getSections();
		$section[0]->addTextBreak();
		$section[0]->addText('Firma electronica:', array('name' => 'Montserrat Regular', 'bold' => true));
		\PhpOffice\PhpWord\Shared\Html::addHtml($section[0], "<p style='font-size: 11pt; font-family: Montserrat Regular;'>" . $sign. "</p>");
		$section[0]->addTextBreak();
		$section[0]->addText('Cadena Original:', array('name' => 'Montserrat Regular', 'bold' => true));    
		$section[0]->addText($chain, array('name' => 'Montserrat Regular')); 
		$section[0]->addTextBreak();
		$section[0]->addText('Si desea verificar la firma electronica escanee el siguiente codigo QR:', array('name' => 'Montserrat Regular'));   
		$section[0]->addTextBreak();
		$section[0]->addImage(
			$pathQR,
			array(
				'width'         => 100,
				'height'        => 100,
				'marginTop'     => -1,
				'marginLeft'    => -1,
				'wrappingStyle' => 'behind',
				'positioning'      => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE,
				'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_CENTER,
				'posHorizontalRel' => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_COLUMN,
				'posVertical'      => \PhpOffice\PhpWord\Style\Image::POSITION_VERTICAL_TOP,
				'posVerticalRel'   => \PhpOffice\PhpWord\Style\Image::POSITION_RELATIVE_TO_LINE,
			)
		);
		$doc->save($path);
		$pathToFile = $path;
		$name = $file->filename;
        $response = Http::attach(
            'data', file_get_contents($pathToFile), $file->filename
        )->post(env('SERVER_COLLABORA').'lool/convert-to/pdf');
		$pathToFile2 = str_replace(".docx", ".pdf", $pathToFile);
        file_put_contents($pathToFile2, $response->body());
		$file->filename = str_replace(".docx", ".pdf", $file->filename);
		$file->path = str_replace(".docx", ".pdf", $file->path);
		$file->filetype = 'pdf';
		$file->save();
	}
}
