<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\AccessToken;
use App\Models\File;
use App\Models\AffairReception;
use App\Models\AffairReceptionResponse;
use Illuminate\Http\Request;
use App\Models\Turn;
use App\Models\TurnLog;
use App\Events\TurnReceived;
use App\Models\User;
use Auth;
use Str;

class TurnController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            if ($request->input('user_id') != Auth::user()->id) {
                $affair_id = $request->input('affair_id');
                $turn = new Turn();
                $turn->affair_id = $affair_id;
                $turn->user_id = $request->input('user_id');
                $turn->instruction_id = $request->input('instruction_id');
                $parent_turn = Turn::where('user_id', Auth::user()->id)
                    ->where('status_id', 1)
                    ->where('affair_id', $affair_id)
                    ->first();
                $turn_log = new TurnLog();
                $turn_log->user_id = Auth::user()->id;
                $turn_log->turn_movement_id = 1;
                if (isset($parent_turn)) {
                    $parent_turn->status_id = 2;
                    $parent_turn->save();
                    $turn->parent_turn_id = $parent_turn->id;
                    $turn_log->turn_id = $parent_turn->id;
                } else {
                    $first_turn = new Turn();
                    $first_turn->affair_id = $affair_id;
                    $first_turn->user_id = Auth::user()->id;
                    $first_turn->instruction_id = 1;
                    $first_turn->status_id = 2;
                    $first_turn->save();
                    $turn->parent_turn_id = $first_turn->id;
                    $turn_log->turn_id = $first_turn->id;
                }
                $turn_log->save();
                $turn->save();
                $affair_origin = AffairReception::find($affair_id);
                if ($affair_origin->require_response == true) {
                    $folio_response = AffairReceptionResponse::where('folio_origin', $affair_origin->folio)->first();
                    $affair_response = AffairReception::where('folio', $folio_response->folio_response)->first();
                    $file = File::where('affair_id', $affair_response->id)->where('is_affair', true)->first();

                    $token = new AccessToken();
                    $token->token = Str::random(40);
                    $token->user_id = $request->input('user_id');
                    $token->file_id = $file->id;
                    $token->UserCanWrite = true;
                    $token->HidePrintOption = false;
                    $token->DisablePrint = false;
                    $token->HideSaveOption = false;
                    $token->HideExportOption = false;
                    $token->DisableExport = false;
                    $token->DisableCopy = false;
                    $token->save();
                }

                $user = User::find($request->input('user_id'));
                broadcast(new TurnReceived($user->username, $affair_origin, Auth::user()->username));
            }
            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
                'turn'  => $turn
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $turn = Turn::where('user_id', Auth::user()->id)
                ->where('status_id', 1)
                ->where('affair_id', $id)
                ->first();
            $turn->status_id = 3;
            $turn->save();
            if (isset($turn->parent_turn_id)) {
                $parent_turn = Turn::find($turn->parent_turn_id);
                $parent_turn->status_id = 1;
                $parent_turn->save();
            }

            $turn_log = new TurnLog();
            $turn_log->user_id = Auth::user()->id;
            $turn_log->turn_movement_id = 5;
            $turn_log->turn_id = $turn->id;
            $turn_log->save();

            $affair_origin = AffairReception::find($id);
            $user = User::find($parent_turn->user_id);
            broadcast(new TurnReceived($user->username, $affair_origin, Auth::user()->username));

            DB::commit();

            return response()->json([
				'success' => true,
				'message' => '',
			], 200);

        } catch (\Exception $e) {
            DB::rollback();
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getLog($id)
    {
        try {
            $affair = AffairReception::join('users', 'affair_receptions.user_id', '=', 'users.id')
                ->join('people', 'users.id', '=', 'people.user_id')
                ->join('positions', 'people.position_id', '=', 'positions.id')
                ->select('affair_receptions.folio as folio',
                    'people.name as name',
                    'people.last_name',
                    'people.second_lastname',
                    'positions.name as position_name',
                    'affair_receptions.subject',
                    'affair_receptions.date_send')
                ->where('affair_receptions.id', $id)
                ->first();
            $turns = Turn::join('users', 'users.id', '=', 'turns.user_id')
                ->join('people', 'people.user_id', '=', 'users.id')
                ->join('positions', 'positions.id', '=', 'people.position_id')
                ->select('turns.id',
                    'people.name as name',
                    'people.last_name as lastname',
                    'people.second_lastname as second_lastname',
                    'positions.name as position_name',
                    'turns.status_id')
                ->where('turns.affair_id', $id)->orderBy('turns.id', 'asc')->get();
            foreach ($turns as $turn) {
                $turn->events = TurnLog::join('cat_turn_movements', 'turn_logs.turn_movement_id', '=', 'cat_turn_movements.id')
                ->select('turn_logs.id',
                    'turn_logs.created_at',
                    'cat_turn_movements.name')
                ->where('turn_logs.turn_id', $turn->id)
                ->orderBy('turn_logs.created_at', 'asc')
                ->get();
            }
            return response()->json([
                'success' => true,
                'turns' => $turns,
                'affair' => $affair,
			]);
        }catch (\Exception $e) {
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }
}
