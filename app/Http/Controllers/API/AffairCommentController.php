<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\AffairComment;
use App\Models\AffairReception;
use App\Events\CommentCreated;
use App\Events\newComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\AccessToken;
use App\Models\File;

class AffairCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $affair_id = $request->input('affair');
            $comments = AffairComment::join('users', 'affair_comments.user_id', '=' ,'users.id')
                ->join('people', 'users.id', '=', 'people.user_id')
                ->join('positions', 'people.position_id', '=', 'positions.id')
                ->select('affair_comments.id as id',
                    'affair_comments.comment as comment',
                    'users.id as user_id',
                    'people.name as name',
                    'people.last_name as last_name',
                    'people.second_lastname as second_lastname',
                    'positions.name as position_name',
                    'affair_comments.created_at as created_at')
                ->where('affair_comments.affair_reception_id', $affair_id)
                ->orderBy('affair_comments.created_at', 'desc')
                ->get();
            
            return response()->json([
                'success' => true,
                'comments' => $comments,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
// 
//         $list = AffairReception::all();
//         return response()->json(['success'=>true,'data'=>['schedules'=>$list]]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // return $request->all();

        try {
            DB::beginTransaction();
            $item = $request->input('comments');
            $count = count($request->input('comments'));
            // error_log(print_r($item, true));
            // error_log(print_r($count, true));
            for( $i=0;$i<$count;$i++)
            {
                $comment = new AffairComment();
                $comment->user_id = Auth::user()->id; 
                $comment->fill($request->all());
                $affair1 = $request->input('folio');
                $affair = AffairReception::where('folio',$affair1)->first();
                $comment->affair_reception_id = $affair->id;
                $commentItem = implode($item[$i]); // convert array element to string
                $comment->comment = $commentItem;
                // error_log(print_r($comment->comment, true));
                // error_log(print_r($comment->affair_reception_id, true));
                $comment->save();
            }
            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
                'comment'  => $comment
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $comment = AffairComment::where('id',$id)->first();
            return response()->json([
                'success' => true,
                'comment' => $comment,
			]);
        }catch (\Exception $e) {
            DB::rollback();
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $comment = AffairComment::find($id);
            $comment->fill($request->all());
            $comment->save();

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $comment = AffairComment::where('id',$id)->first();
            //error_log(print_r($scheduleDay, true));
            $comment->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            error_log($e->getMessage());
            return response('',500);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function addComment(Request $request)
    {
        try {
            $affair_id = $request->input('affair');
            $comment = $request->input('comment');
            DB::beginTransaction();
            $commentadd = new AffairComment();
            $commentadd->user_id = Auth::user()->id;
            $commentadd->affair_reception_id = $affair_id;
            $commentadd->comment = $comment;
            $commentadd->save();
            DB::commit();
            $file = File::where('affair_id', $affair_id)->first();
            $affair = AffairReception::find($affair_id);
            $noti_users = AccessToken::join('users', 'users.id', '=', 'access_tokens.user_id')
                ->select('users.username', 'access_tokens.token')
                ->where('file_id', $file->id)
                ->get()
                ->toArray();
            $user = User::find($affair->user_id);
            foreach ($noti_users as $notiuser)
            {
                if ($notiuser['username'] != Auth::user()->username)
                {   
                    $username = $notiuser['username'];
                    broadcast(new newComment($username, $affair, $user->username));
                }
            }
            broadcast(new CommentCreated($commentadd, $affair_id));

            return response()->json([
                'success' => true,
                'message' => '',
                'comment'  => $comment
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }
}
