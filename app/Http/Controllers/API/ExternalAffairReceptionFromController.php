<?php

namespace App\Http\Controllers\API;

use App\Models\UserLog;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\ExternalAffairReceptionFrom;
use App\Models\ExternalSender;
use App\Models\GeneralLog;
use App\Models\File;
use Illuminate\Support\Facades\Auth;
use App\Models\ExternalAffairReception;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


class ExternalAffairReceptionFromController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
    // return 1;
    try {
      $rowsPerPage = $request->input('rowsPerPage');
      $search = $request->input('search');

      $affairs = ExternalAffairReception::search($search)
        ->join('cat_doc_types', 'cat_doc_types.id', '=', 'external_affair_receptions.cat_doc_types_id')
        ->join('cat_priorities', 'cat_priorities.id', '=', 'external_affair_receptions.cat_priorities_id')
        ->select('external_affair_receptions.folio','cat_priorities.name as priorities','cat_doc_types.name as doc_type','external_affair_receptions.document_date')
        ->orderBy('external_affair_receptions.created_at', 'desc')
        ->paginate($rowsPerPage);

      // $affairs = DB::table('affair_receptions')
      // ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
      // ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
      // ->select('affair_receptions.folio','cat_priorities.name as priorities','cat_doc_types.name as doc_type','affair_receptions.document_date')
      // ->get();

      return response()->json([
        'success' => true,
        'affairs' => $affairs,
      ]);
    } catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
// 
//         $list = AffairReception::all();
//         return response()->json(['success'=>true,'data'=>['schedules'=>$list]]);
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function create()
  {
    //
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function store(Request $request)
  {
   try {
     DB::beginTransaction();
     $item = $request->input('people');
     $itemReceiver = $request->input('receiver_id');
     $itemParticipation = $request->input('participants_id');
     $affairId = ExternalAffairReception::latest('id')->first()->id;
     $externalSenderId = ExternalSender::latest('id')->first()->id;
     $externalAffairFrom = "";
     if (isset($item)) {
       $count = count($request->input('people'));
       for( $i=0;$i<$count;$i++)
       {
         $externalAffairFrom = new ExternalAffairReceptionFrom();
         $externalAffairFrom->external_affair_receptions_id = $affairId;
         $externalAffairFrom->external_sender_id = $externalSenderId;
         $receiverItem = implode($itemReceiver[$i]);
         $externalAffairFrom->receiver_id = $receiverItem;
         $participationItem = implode($itemParticipation[$i]);
         $externalAffairFrom->participation_type_id = $participationItem;
         $externalAffairFrom->sender_id = Auth::user()->id;
         $externalAffairFrom->save();
//         ExternalFileUploadController::generateDoc($affairId);

         $idGeneralLog = GeneralLog::latest('id')->first()->id;
         $userLog = new UserLog();
         $userLog->user_id = Auth::user()->id;
         $userLog->general_log_id = $idGeneralLog;
         $userLog->save();
       }
     } else {
       $externalAffairFrom = new ExternalAffairReceptionFrom();
       $externalAffairFrom->external_affair_receptions_id = $affairId;
       $externalAffairFrom->external_sender_id = $externalSenderId;
       $externalAffairFrom->receiver_id = $itemReceiver;
       $externalAffairFrom->participation_type_id = $itemParticipation;
       $externalAffairFrom->sender_id = Auth::user()->id;
       $externalAffairFrom->save();
//       ExternalFileUploadController::generateDoc($affairId);

       $idGeneralLog = GeneralLog::latest('id')->first()->id;
       $userLog = new UserLog();
       $userLog->user_id = Auth::user()->id;
       $userLog->general_log_id = $idGeneralLog;
       $userLog->save();
     }
     DB::commit();


     return response()->json([
       'success' => true,
       'message' => '',
       'affairFrom'  => $externalAffairFrom
     ], 200);
   } catch (\Exception $e) {
     DB::rollback();
     return response()->json([
       'success' => false,
       'message' => $e->getMessage()
     ]);
   }
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function show($id)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function edit($id)
  {
    try {
      $affair = ExternalAffairReceptionFrom::find($id);
      return response()->json([
        'success' => true,
        'affair' => $affair,
      ]);
    }catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  \Illuminate\Http\Request  $request
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function update(Request $request, $id)
  {
    try {
      DB::beginTransaction();
      $affair = ExternalAffairReceptionFrom::find($id);
      $affair->fill($request->all());
      $affair->save();

      DB::commit();

      return response()->json([
        'success' => true,
        'message' => '',
      ], 200);
    } catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return \Illuminate\Http\Response
   */
  public function destroy($id)
  {
    try {
      DB::beginTransaction();
      $affair = ExternalAffairReception::where('id',$id)->first();
      //error_log(print_r($scheduleDay, true));
      $affair->delete();

      DB::commit();
    } catch (\Exception $e) {
      DB::rollBack();
      error_log($e->getMessage());
      return response('',500);
    }
  }
}
