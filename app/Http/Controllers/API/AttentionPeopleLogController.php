<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\AttentionPeopleLog;
use App\Models\Catalogs\CatAttentionType;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class AttentionPeopleLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');
            $attentionPeopleLogs = AttentionPeopleLog::search($search)->orderBy('created_at', 'desc')->paginate($rowsPerPage);
            return response()->json([
                'success' => true,
                'attentionPeopleLogs' => $attentionPeopleLogs,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {

            DB::beginTransaction();
            $attentionPeopleLog = new AttentionPeopleLog();
            $attentionPeopleLog->fill($request->all());

            $attentionType1 = $request->input('name');
            $attentionType = CatAttentionType::where('name',$attentionType1)->first();
            // error_log(print_r($attentionType, true));
            $attentionPeopleLog->attention_type_id = $attentionType->id;

            $user1 = $request->input('name');
            $user = User::where('name',$user1)->first();
            // error_log(print_r($user, true));
            $attentionPeopleLog->user_id = $user->id;

            $attentionPeopleLog->save();

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $attentionPeopleLog = AttentionPeopleLog::with([
                'dateHour_attention',
                'attention_type_id',
                'user_id'])
                ->where('id',$id)->first();
            return response()->json([
                'success' => true,
                'attentionPeopleLog' => $attentionPeopleLog,
			]);
        }catch (\Exception $e) {
            DB::rollback();
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $attentionPeopleLog = AttentionPeopleLog::find($id);
            $attentionPeopleLog->fill($request->all());
            $attentionPeopleLog->save();

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $attentionPeopleLog = AttentionPeopleLog::with([
                'dateHour_attention',
                'attention_type_id',
                'user_id'])
                ->where('id',$id)->first();
            //error_log(print_r($attentionPeopleLog, true));
            $attentionPeopleLog->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            error_log($e->getMessage());
            return response('',500);
        }
    }
}
