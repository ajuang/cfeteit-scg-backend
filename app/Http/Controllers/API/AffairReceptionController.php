<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\API\FileUploadController;
use App\Models\Catalogs\CatUnit;
use App\Models\Entity;
use App\Models\Ftp;
use App\Models\Position;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\AffairReception;
use App\Models\AffairReceptionFrom;
use App\Models\AffairReceptionResponse;
use Illuminate\Support\Facades\Auth;
use App\Models\File;
use App\Models\AccessToken;
use App\Models\People;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Str;
use Illuminate\Support\Facades\Storage;
use App\Events\AffairReceived;
use App\Events\CloseCollabs;
use App\Models\User;
use App\Models\Turn;

class AffairReceptionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      // return 1;
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');

            $user_id = Auth::user()->id;

            $affairs = People::search($search)->join('positions','people.position_id','=','positions.id')
                ->join('cat_units','positions.cat_unit_id','=','cat_units.id')
                ->join('entities','cat_units.entity_id','=','entities.id')
                ->join('users','people.user_id','=','users.id')
                ->join('affair_reception_froms','users.id','=','affair_reception_froms.sender_id')
                ->join('affair_receptions','affair_reception_froms.affair_receptions_id','=','affair_receptions.id')
                ->join('participation_types','affair_reception_froms.participation_types_id','=','participation_types.id')
                ->join('cat_priorities','affair_receptions.cat_priorities_id','=','cat_priorities.id')
                ->join('affair_statuses', 'affair_statuses.id', '=', 'affair_receptions.status_id')
                ->leftjoin('affair_reception_responses','affair_receptions.folio','=','affair_reception_responses.folio_origin')
                ->select('affair_receptions.require_response', 'people.name as people_name', 'folio_origin','people.last_name', 'people.second_lastname', 'affair_reception_froms.receiver_id as user_affair_id', 'positions.name as position_name',
                'entities.acronym as entity_name','affair_receptions.id as id','affair_receptions.folio','affair_receptions.subject','affair_receptions.date_send', 'affair_reception_froms.seen_date as seen_affair',
                'cat_priorities.name as priority','affair_receptions.document_date','participation_types.name as attentionType', 'affair_reception_responses.folio_response as folio_response', 'affair_statuses.name as affair_status_name', 'affair_statuses.id as status_id')
                ->where('affair_receptions.user_id', $user_id)
                ->where('affair_receptions.historical', false)
                ->orderBy('affair_receptions.created_at', 'desc')
                ->paginate($rowsPerPage);

            foreach ($affairs as $affair) {
                $affair->annexes = File::select('id', 'originalname')->where('affair_id', $affair->id)->where('is_affair', false)->get();
                $affair->to = AffairReceptionFrom::join('users', 'affair_reception_froms.receiver_id', '=', 'users.id')
                    ->join('people', 'users.id', '=', 'people.user_id')
                    ->join('positions','people.position_id','=','positions.id')
                    ->select('affair_reception_froms.id as id', 'people.name as name', 'people.last_name as lastname', 'people.second_lastname', 'positions.name as position_name')
                    ->where('affair_reception_froms.affair_receptions_id', $affair->id)
                    ->where('participation_types_id', 1)
                    ->get();
                $affair->ccp = AffairReceptionFrom::join('users', 'affair_reception_froms.receiver_id', '=', 'users.id')
                    ->join('people', 'users.id', '=', 'people.user_id')
                    ->join('positions','people.position_id','=','positions.id')
                    ->select('affair_reception_froms.id as id', 'people.name as name', 'people.last_name as lastname', 'people.second_lastname', 'positions.name as position_name')
                    ->where('affair_reception_froms.affair_receptions_id', $affair->id)
                    ->where('participation_types_id', 2)
                    ->get();
                if (isset($affair->folio_response)) {
                    $affair->response_id = AffairReception::select('id')->where('folio', $affair->folio_response)->first()->id;
                }
            }  
            // $affairs = DB::table('affair_receptions')
            // ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
            // ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
            // ->select('affair_receptions.folio','cat_priorities.name as priorities','cat_doc_types.name as doc_type','affair_receptions.document_date')
            // ->get();
            
            return response()->json([
                'success' => true,
                'affairs' => $affairs,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
// 
//         $list = AffairReception::all();
//         return response()->json(['success'=>true,'data'=>['schedules'=>$list]]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // return $request->all();
      //RESPONSABLE Copia Espejo
      $responsable = People::where('user_id', Auth::user()->id)->first();
      $idUser = Auth::user()->id;
      if($responsable->responsable != null) {
        $idUser = $responsable->responsable;
      }
        try { 
            DB::beginTransaction();
            $affair = new AffairReception();
            $affair->fill($request->all());
            $affair->subject = $request->input('subject');
            $affair->cat_doc_types_id = $request->input('cat_doc_types_id');
            $affair->cat_priorities_id = $request->input('cat_priorities_id');
            $affair->status_id = $request->input('status_id');
            $affair->document_date = Carbon::now();
            $affair->user_id = $idUser;
            $affair->save();
            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
                'affair'  => $affair
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $affair = AffairReception::where('id',$id)->first();
            return response()->json([
                'success' => true,
                'affair' => $affair,
			]);
        }catch (\Exception $e) {
            DB::rollback();
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $affair = AffairReception::find($id);
            $file = File::where('affair_id', $id)->where('is_affair', true)->first();
            $affair->fill($request->all());
            if ($request->input('historical') != null) {
                $origin = AffairReceptionResponse::where('folio_origin', $affair->folio)->first();
                if (isset($origin)) {
                    $affair_response = AffairReception::where('folio', $origin->folio_response)->first();
                    $affair_response->historical = true;
                    $affair_response->save();
                }
            }
            if ($request->input('status_id') == 3) {
                $affair->date_send = Carbon::now(); 
                $response = AffairReceptionResponse::where('folio_response', $affair->folio)->first();
                if (isset($response)) {
                    $affair_origin = AffairReception::where('folio', $response->folio_origin)->first();
                    $affair_origin->status_id = 5;
                    $affair_origin->save();
                } 
                $noti_users = AccessToken::join('users', 'users.id', '=', 'access_tokens.user_id')
                    ->select('users.username', 'access_tokens.token')
                    ->where('file_id', $file->id)
                    ->get()
                    ->toArray();
                $user = User::find($affair->user_id);
                foreach ($noti_users as $notiuser)
                {   
                    $username = $notiuser['username'];
                    broadcast(new CloseCollabs($username, $affair, $user->username));
                } 
                $affair_from = AffairReceptionFrom::where('affair_receptions_id', $id)->first();

                $sign = $request->input('efirma');
                $chain = $request->input('cadena');
                $url = $request->input('url');

                FileUploadController::signDoc($file, $sign, $chain, $url);

                $user = User::find($affair_from->receiver_id);
                broadcast(new AffairReceived($user->username, $affair, Auth::user()->username));

                /***************************** Validación diferentes Entidad ************************/
                //Entidad de origen
                $peopleOrigin = People::where('user_id', Auth::user()->id)->first();
                $positionOrigin = Position::where('id', $peopleOrigin->position_id)->first();
                $unitOrigin = CatUnit::where('id', $positionOrigin->cat_unit_id)->first();

                //Entidad de envío
                $people = People::where('id', $affair_from->receiver_id)->first();
                $position = Position::where('id', $people->position_id)->first();
                $unit = CatUnit::where('id', $position->cat_unit_id)->first();
                $entity = Entity::where('id', $unit->entity_id)->first();
                if ($unitOrigin->entity_id != $entity->id) {
                    //******************************Enviar a RabbitMQ***************************************
                    $connection = new AMQPStreamConnection(env('RABBITMQ_HOST'), env('RABBITMQ_PORT'), env('RABBITMQ_USER'), env('RABBITMQ_PASSWORD'));
                    $channel = $connection->channel();

                    $channel->queue_declare(env('RABBITMQ_QUEUE'), false, false, false, false);

                    $msg = new AMQPMessage('affair=' . $affair_from->id . '&file=' . $file->filename . '&authID=' . Auth::user()->id);
                    $channel->basic_publish($msg, '', env('RABBITMQ_QUEUE'));
                    echo 'affair=' . $affair_from->receiver_id . '&file=' . $file->filename . '&authID=' . Auth::user()->id;
                    $channel->close();
                    $connection->close();
                }
            }
            $affair->save();

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => $affair,
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $affair = AffairReception::where('id',$id)->first();
            //error_log(print_r($scheduleDay, true));
            $affair->delete();

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            error_log($e->getMessage());
            return response('',500);
        }
    }

    public function inbox(Request $request)
    {
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');
            $responsable = $request->input('responsable');
            $user_id = Auth::user()->id;

            $turns = Turn::whereIn('user_id', [$user_id, $responsable])
              ->where('status_id', 1)
              ->get()
              ->pluck('affair_id')
              ->toArray();

            $turns2 = Turn::whereIn('user_id', [$user_id, $responsable])
                ->whereIn('status_id', [2, 3])
                ->get()
                ->pluck('affair_id')
                ->toArray();
            $folios_response = AffairReceptionResponse::get()
                ->pluck('folio_response')
                ->toArray();

            $affairsInbox = People::search($search)->join('positions','people.position_id','=','positions.id')
                ->join('cat_units','positions.cat_unit_id','=','cat_units.id')
                ->join('entities','cat_units.entity_id','=','entities.id')
                ->join('users','people.user_id','=','users.id')
                ->join('affair_reception_froms','users.id','=','affair_reception_froms.sender_id')
                ->join('affair_receptions','affair_reception_froms.affair_receptions_id','=','affair_receptions.id')
                ->join('participation_types','affair_reception_froms.participation_types_id','=','participation_types.id')
                ->join('cat_priorities','affair_receptions.cat_priorities_id','=','cat_priorities.id')
                ->leftjoin('affair_reception_responses','affair_receptions.folio','=','affair_reception_responses.folio_origin')
                ->select('affair_receptions.require_response', 'people.name as people_name', 'folio_origin', 'status_id' ,'people.last_name', 'people.second_lastname', 'affair_reception_froms.receiver_id as user_affair_id', 'positions.name as position_name',
                'entities.acronym as entity_name','affair_receptions.id as id','affair_receptions.folio','affair_receptions.subject','affair_receptions.date_send', 'affair_reception_froms.seen_date as seen_affair',
                'cat_priorities.name as priority','affair_receptions.document_date','participation_types.name as attentionType', 'affair_reception_responses.folio_response as folio_response')
                ->where('affair_receptions.historical', false)
                ->whereIn('affair_reception_froms.receiver_id', [$user_id, $responsable])
                ->whereIn('affair_receptions.status_id', [3, 4])
                ->whereNotIn('affair_receptions.id', $turns2)
                ->whereNotIn('affair_receptions.folio', $folios_response)
                ->orWhere(function($query) use ($turns) {
                    $query->whereIn('affair_receptions.id', $turns)
                        ->where('affair_receptions.historical', false);
                })
                ->orderBy('affair_receptions.created_at', 'desc')
                ->paginate($rowsPerPage);
          
            // $affairsSearch = AffairReception::search($search)->orderBy('created_at', 'desc')->paginate($rowsPerPage);
            foreach ($affairsInbox as $affair) {
                if ($affair->user_affair_id == $user_id) {
                    $affair->type = 'Envio directo';
                } else {
                    $affair->type = 'Turnado';
                }
                $affair->docExist = AffairReceptionResponse::where('folio_origin', $affair->folio)->first() != null ? true : false;
                $affair->annexes = File::select('id', 'originalname')->where('affair_id', $affair->id)->where('is_affair', false)->get();
                if (isset($affair->folio_response)) {
                    $affair->response_id = AffairReception::select('id')->where('folio', $affair->folio_response)->first()->id;
                }
                $affair->to = AffairReceptionFrom::join('users', 'affair_reception_froms.receiver_id', '=', 'users.id')
                    ->join('people', 'users.id', '=', 'people.user_id')
                    ->join('positions','people.position_id','=','positions.id')
                    ->select('affair_reception_froms.id as id', 'people.name as name', 'people.last_name as lastname', 'people.second_lastname', 'positions.name as position_name')
                    ->where('affair_reception_froms.affair_receptions_id', $affair->id)
                    ->where('participation_types_id', 1)
                    ->get();
                $affair->ccp = AffairReceptionFrom::join('users', 'affair_reception_froms.receiver_id', '=', 'users.id')
                    ->join('people', 'users.id', '=', 'people.user_id')
                    ->join('positions','people.position_id','=','positions.id')
                    ->select('affair_reception_froms.id as id', 'people.name as name', 'people.last_name as lastname', 'people.second_lastname', 'positions.name as position_name')
                    ->where('affair_reception_froms.affair_receptions_id', $affair->id)
                    ->where('participation_types_id', 2)
                    ->get();
            }
            
            return response()->json([
                'success' => true,
                'affairsInbox' => $affairsInbox,
                // 'affairsSearch' => $affairsSearch
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    
    public function inboxbyIdUser(Request $request)
    {
      try {
        $rowsPerPage = $request->input('rowsPerPage');
        $search = $request->input('search');
        $responsable = $request->input('responsable');
        $user_id = Auth::user()->id;
  
        $turns = Turn::where('user_id', $user_id)
          ->orWhere('user_id', $responsable)
          ->where('status_id', 1)
          ->get()
          ->pluck('affair_id')
          ->toArray();
  
        $turns2 = Turn::where('user_id', $user_id)
          ->orWhere('user_id', $responsable)
          ->where('status_id', 2)
          ->get()
          ->pluck('affair_id')
          ->toArray();
        
        $affairsInbox = People::search($search)->join('positions','people.position_id','=','positions.id')
          ->join('cat_units','positions.cat_unit_id','=','cat_units.id')
          ->join('entities','cat_units.entity_id','=','entities.id')
          ->join('users','people.user_id','=','users.id')
          ->join('affair_reception_froms','users.id','=','affair_reception_froms.sender_id')
          ->join('affair_receptions','affair_reception_froms.affair_receptions_id','=','affair_receptions.id')
          ->join('participation_types','affair_reception_froms.participation_types_id','=','participation_types.id')
          ->join('cat_priorities','affair_receptions.cat_priorities_id','=','cat_priorities.id')
          ->leftjoin('affair_reception_responses','affair_receptions.folio','=','affair_reception_responses.folio_origin')
          ->select('affair_receptions.require_response', 'people.name as people_name', 'folio_origin', 'response' ,'people.last_name', 'people.second_lastname', 'affair_reception_froms.receiver_id as user_affair_id', 'positions.name as position_name',
            'entities.acronym as entity_name','affair_receptions.id as id','affair_receptions.folio','affair_receptions.subject',
            'cat_priorities.name as priority','affair_receptions.document_date','participation_types.name as attentionType', 'affair_reception_responses.folio_response as folio_response')
          ->whereIn('affair_reception_froms.receiver_id', [$user_id, $responsable])
          ->where('affair_receptions.draft', false)
          ->whereNotIn('affair_receptions.id', $turns2)
          ->orWhereIn('affair_receptions.id', $turns)
          ->orderBy('affair_receptions.created_at', 'desc')
          ->paginate($rowsPerPage);
        // $affairsSearch = AffairReception::search($search)->orderBy('created_at', 'desc')->paginate($rowsPerPage);
        foreach ($affairsInbox as $affair) {
          if ($affair->user_affair_id == $user_id) {
            $affair->type = 'Envio directo';
          } else {
            $affair->type = 'Turnado';
          }
          $affair->docExist = AffairReceptionResponse::where('folio_origin', $affair->folio)->first() != null ? true : false;
          if (isset($affair->folio_response)) {
            $affair->response_id = AffairReception::select('id')->where('folio', $affair->folio_response)->first()->id;
          }
        }
  
        return response()->json([
          'success' => true,
          'affairsInbox' => $affairsInbox,
          // 'affairsSearch' => $affairsSearch
        ]);
      } catch (\Exception $e) {
        DB::rollback();
        return response()->json([
          'success' => false,
          'message' => $e->getMessage()
        ]);
      }
    }


  public function turn(Request $request)
    {
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');
            $responsable = $request->input('responsable');

            $user_id = Auth::user()->id;

            $turns = Turn::whereIn('user_id', [$user_id, $responsable])
                ->where('status_id', 2)
                ->get()
                ->pluck('affair_id')
                ->toArray();

            $affairsInbox = People::search($search)->join('positions','people.position_id','=','positions.id')
            ->join('cat_units','positions.cat_unit_id','=','cat_units.id')
            ->join('entities','cat_units.entity_id','=','entities.id')
            ->join('users','people.user_id','=','users.id')
            ->join('affair_reception_froms','users.id','=','affair_reception_froms.sender_id')
            ->join('affair_receptions','affair_reception_froms.affair_receptions_id','=','affair_receptions.id')
            ->join('participation_types','affair_reception_froms.participation_types_id','=','participation_types.id')
            ->join('cat_priorities','affair_receptions.cat_priorities_id','=','cat_priorities.id')
            ->leftjoin('affair_reception_responses','affair_receptions.folio','=','affair_reception_responses.folio_origin')
            ->select('affair_receptions.require_response', 'people.name as people_name', 'folio_origin','people.last_name', 'people.second_lastname', 'affair_reception_froms.receiver_id as user_affair_id', 'positions.name as position_name',
            'entities.acronym as entity_name','affair_receptions.id as id','affair_receptions.folio','affair_receptions.subject','affair_receptions.date_send', 'affair_reception_froms.seen_date as seen_affair',
            'cat_priorities.name as priority','affair_receptions.document_date','participation_types.name as attentionType', 'affair_reception_responses.folio_response as folio_response')
            ->whereIn('affair_receptions.id', $turns)
            ->orderBy('affair_receptions.created_at', 'desc')
            ->paginate($rowsPerPage);

            // $affairsSearch = AffairReception::search($search)->orderBy('created_at', 'desc')->paginate($rowsPerPage);
            foreach ($affairsInbox as $affair) {
                if ($affair->user_affair_id == $user_id) {
                    $affair->type = 'Envio directo';
                } else {
                    $affair->type = 'Turnado';
                }
                $affair->docExist = AffairReceptionResponse::where('folio_origin', $affair->folio)->first() != null ? true : false;
                $affair->annexes = File::select('id', 'originalname')->where('affair_id', $affair->id)->where('is_affair', false)->get();
                if (isset($affair->folio_response)) {
                    $affair->response_id = AffairReception::select('id')->where('folio', $affair->folio_response)->first()->id;
                }
                $affair->to = AffairReceptionFrom::join('users', 'affair_reception_froms.receiver_id', '=', 'users.id')
                    ->join('people', 'users.id', '=', 'people.user_id')
                    ->join('positions','people.position_id','=','positions.id')
                    ->select('affair_reception_froms.id as id', 'people.name as name', 'people.last_name as lastname', 'people.second_lastname', 'positions.name as position_name')
                    ->where('affair_reception_froms.affair_receptions_id', $affair->id)
                    ->where('participation_types_id', 1)
                    ->get();
                $affair->ccp = AffairReceptionFrom::join('users', 'affair_reception_froms.receiver_id', '=', 'users.id')
                    ->join('people', 'users.id', '=', 'people.user_id')
                    ->join('positions','people.position_id','=','positions.id')
                    ->select('affair_reception_froms.id as id', 'people.name as name', 'people.last_name as lastname', 'people.second_lastname', 'positions.name as position_name')
                    ->where('affair_reception_froms.affair_receptions_id', $affair->id)
                    ->where('participation_types_id', 2)
                    ->get();
            }

            return response()->json([
                'success' => true,
                'affairsInbox' => $affairsInbox,
                // 'affairsSearch' => $affairsSearch
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

  public function inboxHistory(Request $request)
  {
    try {
      $rowsPerPage = $request->input('rowsPerPage');
      $search = $request->input('search');

      $user_id = Auth::user()->id;

      $affairsInbox = AffairReception::search($search)
        ->join('affair_reception_froms','affair_receptions.id','=','affair_reception_froms.affair_receptions_id')
        ->join('users','affair_reception_froms.receiver_id','=','users.id')
        ->join('people','users.id','=','people.user_id')
        ->join('positions','people.position_id','=','positions.id')
        ->join('cat_units','positions.cat_unit_id','=','cat_units.id')
        ->join('entities','cat_units.entity_id','=','entities.id')
        ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
        ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
        ->select('people.name as receiver_name','people.last_name','people.second_lastname','entities.acronym as entity_name',
          'positions.name as position_name','affair_receptions.id as id','affair_receptions.folio','cat_priorities.name as priorities',
          'cat_doc_types.name as doc_type','affair_receptions.document_date',)
        ->orWhere('affair_receptions.user_id', $user_id)
        ->where('affair_receptions.historical', true)
        ->orderBy('affair_receptions.created_at', 'desc')
        ->paginate($rowsPerPage);
      // $affairsSearch = AffairReception::search($search)->orderBy('created_at', 'desc')->paginate($rowsPerPage);

      return response()->json([
        'success' => true,
        'affairsInbox' => $affairsInbox,
        // 'affairsSearch' => $affairsSearch
      ]);
    } catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
  }
    
  public function draft(Request $request)
  {
    // return 1;
    try {
        $rowsPerPage = $request->input('rowsPerPage');
        $search = $request->input('search');

        $user_id = Auth::user()->id;
        $files = AccessToken::where('user_id', $user_id)->get()->pluck('file_id')->toArray();
        $affairs_id = File::whereIn('id', $files)->get()->pluck('affair_id')->toArray();

        $affairs = AffairReception::search($search)
            ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
            ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
            ->join('users', 'users.id', '=', 'affair_receptions.user_id')
            ->join('people', 'people.user_id', '=', 'users.id')
            ->join('positions', 'positions.id', '=', 'people.position_id')
            ->select('affair_receptions.id as id','affair_receptions.folio','cat_priorities.name as priorities','cat_doc_types.name as doc_type','affair_receptions.document_date', 'subject','people.name as people_name', 'people.last_name', 'people.second_lastname', 'positions.name as position_name')
            ->whereIn('affair_receptions.id', $affairs_id)
            ->where('affair_receptions.collabora',true)
            ->orderBy('affair_receptions.created_at', 'desc')
            ->paginate($rowsPerPage);

        return response()->json([
            'success' => true,
            'affairs' => $affairs,
        ]);
    } catch (\Exception $e) {
        DB::rollback();
        return response()->json([
            'success' => false,
            'message' => $e->getMessage()
        ]);
    }
  }

  public function collabora(Request $request)
  {
    // return 1;
    try {
      $rowsPerPage = $request->input('rowsPerPage');
      $search = $request->input('search');
      $responsable = $request->input('responsable');

      $user_id = Auth::user()->id;
      $files = AccessToken::where('user_id', $user_id)->orWhere('user_id', $responsable)->get()->pluck('file_id')->toArray();
      $affairs_id = File::whereIn('id', $files)->get()->pluck('affair_id')->toArray();
      error_log('check');

      $affairs = AffairReception::search($search)
        ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
        ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
        ->join('users', 'users.id', '=', 'affair_receptions.user_id')
        ->join('people', 'people.user_id', '=', 'users.id')
        ->join('positions', 'positions.id', '=', 'people.position_id')
        ->select('affair_receptions.id as id','affair_receptions.folio','cat_priorities.name as priorities','cat_doc_types.name as doc_type','affair_receptions.document_date', 'subject','people.name as people_name', 'people.last_name', 'people.second_lastname', 'positions.name as position_name')
        ->whereIn('affair_receptions.id', $affairs_id)
        ->where('affair_receptions.collabora',true)
        ->orderBy('affair_receptions.created_at', 'desc')
        ->paginate($rowsPerPage);

      error_log(print_r($affairs, true));
      return response()->json([
        'success' => true,
        'affairs' => $affairs,
      ]);
    } catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
  }
  
  public function priorityBox(Request $request)
    {
      // return 1;
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');

            $user_id = Auth::user()->id;

            $priorityAffairs = People::search($search)
							->join('positions','people.position_id','=','positions.id')
							->join('cat_units','positions.cat_unit_id','=','cat_units.id')
							->join('entities','cat_units.entity_id','=','entities.id')
              ->join('users','people.user_id','=','users.id')
              ->join('affair_reception_froms','users.id','=','affair_reception_froms.sender_id')
              ->join('affair_receptions','affair_reception_froms.affair_receptions_id','=','affair_receptions.id')
              ->join('participation_types','affair_reception_froms.participation_types_id','=','participation_types.id')
              ->join('cat_priorities','affair_receptions.cat_priorities_id','=','cat_priorities.id')
              ->select('people.name as people_name', 'people.last_name', 'people.second_lastname','positions.name as position_name',
							'entities.acronym as entity_name','affair_receptions.id as id','affair_receptions.folio','affair_receptions.subject',
							'cat_priorities.name as priority','affair_receptions.document_date','participation_types.name as attentionType')
              ->where('affair_reception_froms.receiver_id', $user_id)
              ->where('affair_receptions.draft', false)
              ->where('cat_priorities.name', 'Urgente')
              ->orderBy('affair_receptions.created_at', 'desc')
              ->paginate($rowsPerPage);
            
            return response()->json([
                'success' => true,
                'priorityAffairs' => $priorityAffairs
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    public function affairsToCentral(Request $request)
    {
      // return 1;
        try {
            $rowsPerPage = $request->input('rowsPerPage');
            $search = $request->input('search');

            $affairInfoSender = AffairReception::search($search)
						->join('affair_reception_froms','affair_receptions.id','=','affair_reception_froms.affair_receptions_id')
						->join('users','affair_reception_froms.sender_id','=','users.id')
						->join('people','users.id','=','people.user_id')
						->join('positions','people.position_id','=','positions.id')
						->join('cat_units','positions.cat_unit_id','=','cat_units.id')
						->join('entities','cat_units.entity_id','=','entities.id')
            ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
            ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
            ->select('affair_receptions.id as affair_id','affair_receptions.folio','affair_reception_froms.sender_id',
            'entities.id as origin_entity_id','entities.acronym as origin_entity_name','people.name as sender_name',
            'people.last_name','people.second_lastname','positions.name as position_name',
            'cat_priorities.name as priorities','cat_doc_types.name as doc_type','affair_receptions.document_date')
            ->where('affair_receptions.draft', false)
            ->orderBy('affair_receptions.created_at', 'desc')
            ->paginate($rowsPerPage);
            
            $affairInfoReceiver = AffairReception::search($search)
						->join('affair_reception_froms','affair_receptions.id','=','affair_reception_froms.affair_receptions_id')
						->join('users','affair_reception_froms.receiver_id','=','users.id')
						->join('people','users.id','=','people.user_id')
						->join('positions','people.position_id','=','positions.id')
						->join('cat_units','positions.cat_unit_id','=','cat_units.id')
						->join('entities','cat_units.entity_id','=','entities.id')
            ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
            ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
            ->select('entities.id as destiny_entity_id','entities.acronym as destiny_entity_name','people.name as receiver_name',
            'people.last_name','people.second_lastname','positions.name as position_name')
            ->where('affair_receptions.draft', false)
            ->orderBy('affair_receptions.created_at', 'desc')
            ->paginate($rowsPerPage);

            // $affairs = DB::table('affair_receptions')
            // ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
            // ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
            // ->select('affair_receptions.folio','cat_priorities.name as priorities','cat_doc_types.name as doc_type','affair_receptions.document_date')
            // ->get();
            
            return response()->json([
                'success' => true,
                'affairInfoSender' => $affairInfoSender,
                'affairInfoReceiver' => $affairInfoReceiver,
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
// 
//         $list = AffairReception::all();
//         return response()->json(['success'=>true,'data'=>['schedules'=>$list]]);
    }
    
    public function showHistory($folio)
    {
      try {
        $affairFromData = AffairReceptionFrom::join('affair_receptions','affair_receptions.id','=','affair_reception_froms.affair_receptions_id')
          ->join('affair_reception_responses','affair_reception_responses.folio_origin', '=', 'affair_receptions.folio')
          ->join('people','people.user_id','=','affair_reception_froms.receiver_id')
          ->where('affair_reception_responses.folio_origin','=',$folio)
          ->select('affair_reception_responses.folio_response','affair_receptions.id',
          'affair_reception_responses.created_at as date', DB::raw("CONCAT(people.name, ' ', people.last_name) AS full_name"),
          'affair_reception_responses.folio_origin', 'affair_reception_responses.folio_response')
          ->get();
        return response()->json([
          'success' => true,
          'affairs' => $affairFromData,
        ]);
      } catch (\Exception $e) {
        return response()->json([
          'success' => false,
          'message' => $e->getMessage()
        ]);
      }
    }

  public function getAffairReceptionByFolio($folio)
  {
    try {
      $affair = AffairReception::where('folio',$folio)->first();
      return response()->json([
        'success' => true,
        'affairs' => $affair,
      ]);
    }catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
  }

  /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generateFolio()
    {
        try {
            $affair = AffairReception::select(DB::raw('max(id) as lastFolio'))->get();
            return response()->json([
                'success' => true,
                'affair' => $affair,
			]);
        }catch (\Exception $e) {
            DB::rollback();
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }

    public function scheduleDocument()
    {
        try {

          $user_id = Auth::user()->id;
          $files = AccessToken::where('user_id', $user_id)->get()->pluck('file_id')->toArray();
          $affairs_id = File::whereIn('id', $files)->get()->pluck('affair_id')->toArray();

            $affair = AffairReception::join('affair_reception_froms','affair_receptions.id','=','affair_reception_froms.affair_receptions_id')
            ->join('users as receiverUsers','affair_reception_froms.receiver_id','=','receiverUsers.id')
            ->join('people as receiverPeople','receiverUsers.id','=','receiverPeople.user_id')
            ->join('users as senderUsers','affair_reception_froms.sender_id','=','senderUsers.id')
            ->join('people as senderPeople','senderUsers.id','=','senderPeople.user_id')
            ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
            ->whereIn('affair_receptions.id', $affairs_id)
            ->select('affair_receptions.folio as folio', 'affair_receptions.document_date as oficeDate',
            DB::raw("CONCAT(\"receiverPeople\".name, ' ', \"receiverPeople\".last_name) AS receiver"),
            DB::raw("CONCAT(\"senderPeople\".name, ' ', \"senderPeople\".last_name) AS sender"),
            'cat_priorities.days as priorityDays')
            ->get();
            return response()->json([
                'success' => true,
                'affair' => $affair,
			]);
        }catch (\Exception $e) {
            DB::rollback();
			return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }

  public function getCollaborators($id)
  {
    try
    {
      $collaborators = People::join('users','users.id','=','people.user_id')
        ->join('access_tokens','access_tokens.user_id','=','users.id')
        ->join('files','files.id','=','access_tokens.file_id')
        ->join('affair_receptions','affair_receptions.id','=','files.affair_id')
        ->select(DB::raw("CONCAT(people.name, ' ' ,people.last_name, ' ' ,people.second_lastname) as full_name"),
        'access_tokens.created_at as collaborator_date')
        ->where('affair_receptions.id', $id)
        ->where('access_tokens.acceptCollabora', true)
        ->get();
        return response()->json([
          'success' => true,
          'collaborators' => $collaborators,
        ]);
      } catch (\Exception $e) {
        return response()->json([
          'success' => false,
          'message' => $e->getMessage()
        ]);
      }
  }

  public function getAffairReceptionByIdUser(Request $request)
  {
    try {
      $rowsPerPage = $request->input('rowsPerPage');
      $search = $request->input('search');

      $user_id = Auth::user()->id;

      $affairs = AffairReception::search($search)
        ->join('affair_reception_froms','affair_receptions.id','=','affair_reception_froms.affair_receptions_id')
        ->join('users','affair_reception_froms.receiver_id','=','users.id')
        ->join('people','users.id','=','people.user_id')
        ->join('positions','people.position_id','=','positions.id')
        ->join('cat_units','positions.cat_unit_id','=','cat_units.id')
        ->join('entities','cat_units.entity_id','=','entities.id')
        ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
        ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
        ->select('reject', 'people.name as receiver_name','people.last_name','people.second_lastname','entities.acronym as entity_name',
          'positions.name as position_name','affair_receptions.id as id','affair_receptions.folio','cat_priorities.name as priorities',
          'cat_doc_types.name as doc_type','affair_receptions.document_date', 'draft', 'collabora', 'answered', 'response', 'require_response')
        ->orWhere('affair_receptions.user_id', $request->input('idUser'))
        ->orWhere('affair_receptions.user_id', $user_id)
        ->where('affair_receptions.require_response', true)
        ->where('affair_receptions.historical', false)
        ->orderBy('affair_receptions.created_at', 'desc')
        ->paginate($rowsPerPage);

      // $affairs = DB::table('affair_receptions')
      // ->join('cat_doc_types', 'cat_doc_types.id', '=', 'affair_receptions.cat_doc_types_id')
      // ->join('cat_priorities', 'cat_priorities.id', '=', 'affair_receptions.cat_priorities_id')
      // ->select('affair_receptions.folio','cat_priorities.name as priorities','cat_doc_types.name as doc_type','affair_receptions.document_date')
      // ->get();

      return response()->json([
        'success' => true,
        'affairs' => $affairs,
      ]);
    } catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
  }
}
