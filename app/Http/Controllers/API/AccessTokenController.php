<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\People;
use Illuminate\Support\Facades\DB;
use App\Models\AccessToken;
use App\Models\File;
use Illuminate\Http\Request;
use App\Models\AffairReception;
use App\Events\docCollab;
use App\Models\User;
use Str;
use Auth;

class AccessTokenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $permissions = DB::table('access_tokens')
              ->join('files', 'files.id', '=', 'access_tokens.file_id')
              ->join('people', 'people.user_id', '=', 'access_tokens.user_id')
              ->join('positions', 'positions.id', '=', 'people.position_id')
              ->select('access_tokens.acceptCollabora', 'access_tokens.id as id', 'people.name as people_name', 'people.last_name', 'people.second_lastname', 'positions.name as position_name')
              ->where('files.affair_id', $request->input('affair_id'))
              ->orderBy('access_tokens.created_at','desc')->get();
            return response()->json([
                'success' => true,
                'permissions' => $permissions,
            ]);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
              'success' => false,
              'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $permission = new AccessToken();
            $permission->fill($request->all());
            $permission->token = Str::random(40);
            $affair_id = $request->input('affair_id');
            $file = File::where('affair_id', $affair_id)->first();
            $permission->file_id = $file->id;
            $permission->user_id = $request->input('userId');
            $permission->UserCanWrite = $request->input('canWrite');
            $permission->HidePrintOption = $request->input('hidePrintOption');
            $permission->DisablePrint = $request->input('disablePrint');
            $permission->HideSaveOption = $request->input('hideSaveOption');
            $permission->HideExportOption = $request->input('hideExportOption');
            $permission->DisableExport = $request->input('disableExport');
            $permission->DisableCopy = $request->input('disableCopy');
            $permission->acceptCollabora = $request->input('acceptCollabora');
            $permission->save();
            DB::commit();

            $user = User::find($request->input('userId'));
            broadcast(new docCollab($user->username, AffairReception::find($affair_id), Auth::user()->username));

            return response()->json([
                'success' => true,
                'message' => '',
                'permission'  => $permission
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AccessToken  $accessToken
     * @return \Illuminate\Http\Response
     */
    public function show(AccessToken $accessToken)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $permission = AccessToken::where('id',$id)->first();
            return response()->json([
                'success' => true,
                'permission' => $permission,
			]);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json([
              'success' => false,
              'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            DB::beginTransaction();
            $permission = AccessToken::find($id);
            $permission->fill($request->all());
//            $permission->user_id = $request->input('userId');
//            $permission->UserCanWrite = $request->input('UserCanWrite');
//            $permission->HidePrintOption = $request->input('HidePrintOption');
//            $permission->DisablePrint = $request->input('DisablePrint');
//            $permission->HideSaveOption = $request->input('HideSaveOption');
//            $permission->HideExportOption = $request->input('HideExportOption');
//            $permission->DisableExport = $request->input('DisableExport');
//            $permission->DisableCopy = $request->input('DisableCopy');
            $permission->acceptCollabora = $request->input('acceptCollabora');
            $permission->save();
            DB::commit();

            return response()->json([
                'success' => true,
                'message' => '',
                'permission'  => $permission
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::beginTransaction();
            $permission = AccessToken::where('id',$id)->first();
            $permission->delete();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
            error_log($e->getMessage());
            return response('',500);
        }
    }

    /**
     * Get AccesToken for user.
     *
     * @param  \App\Models\AccessToken  $accessToken
     * @return \Illuminate\Http\Response
     */
    public function getToken($id){
      //RESPONSABLE Copia Espejo
        try {
            $responsable = People::where('user_id', \Illuminate\Support\Facades\Auth::user()->id)->first();
            $idUser = Auth::user()->id;
            if($responsable->responsable != null) {
                $idUser = $responsable->responsable;
            }
            $file = File::where('affair_id', $id)
                ->where('is_affair', true)
                ->first();
            $token = AccessToken::select('token', 'file_id')
                ->where('user_id', $idUser)
                ->where('file_id', $file->id)
                ->first();
            $affair = AffairReception::find($id);

			return response()->json([
				'success' => true,
				'token' => $token,
                'affair' => $affair
                    ], 200);
		} catch (\Exception $e) {
			return response()->json([
                'success' => false,
                'message' => $e->getMessage()
            ]);
		}
    }

    public function shareFile(Request $request, $id){
        try {
            DB::beginTransaction();
            $token = new AcessToken();
            $token->token = Str::random(40);
            $token->fill($request->all());
            $token->save();
            DB::commit();

            return response()->json([
				'success' => true,
				'message' => '',
			], 200);
        } catch (\Exception $e) {
            DB::rollback();
            error_log($e->getMessage());
            return response()->json([
				'success' => false,
				'message' => $e->getMessage()
			]);
        }
    }

  public function getCollaborators($id)
  {
    try {
      $collaborators = DB::table('access_tokens')
        ->join('files', 'files.id', '=', 'access_tokens.file_id')
        ->join('people', 'people.user_id', '=', 'access_tokens.user_id')
        ->join('positions', 'positions.id', '=', 'people.position_id')
        ->select('access_tokens.acceptCollabora', 'access_tokens.id as id', 'access_tokens.user_id', 'files.affair_id', 'people.name as people_name', 'people.last_name', 
          'people.second_lastname', 'positions.name as position_name', 'UserCanWrite', 
          'HidePrintOption', 'DisablePrint', 'HideSaveOption', 'HideExportOption', 'DisableExport', 'DisableCopy')
        ->where('files.affair_id', $id)
        ->orderBy('access_tokens.created_at','desc')->get();
      return response()->json([
        'success' => true,
        'collaborators' => $collaborators,
      ]);
    }catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
  } 
  
  public function getPermissions($userID, $affairID)
  {
    try {
      $permissions = DB::table('access_tokens')
        ->join('files', 'files.id', '=', 'access_tokens.file_id')
        ->join('people', 'people.user_id', '=', 'access_tokens.user_id')
        ->join('positions', 'positions.id', '=', 'people.position_id')
        ->select('access_tokens.id as id', 'access_tokens.user_id', 'files.affair_id','people.name as people_name', 'people.last_name', 
          'people.second_lastname', 'positions.name as position_name', 'UserCanWrite', 
          'HidePrintOption', 'DisablePrint', 'HideSaveOption', 'HideExportOption', 'DisableExport', 'DisableCopy')
        ->where('access_tokens.user_id', $userID)
        ->where('files.affair_id', $affairID)
        ->orderBy('access_tokens.created_at','desc')->get();
      return response()->json([
        'success' => true,
        'permissions' => $permissions,
      ]);
    }catch (\Exception $e) {
      DB::rollback();
      return response()->json([
        'success' => false,
        'message' => $e->getMessage()
      ]);
    }
  }
}
