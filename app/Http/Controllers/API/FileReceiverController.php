<?php

namespace App\Http\Controllers\API;

use App\Models\AccessToken;
use App\Models\AffairReception;
use App\Models\AffairReceptionFrom;
use App\Models\AffairReceptionResponse;
use App\Models\File;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Str;
use App\Events\AffairReceived;
use App\Models\User;

class FileReceiverController extends Controller
{
  public function receive(Request $request) {
    /*********************FILE*************************/
    $fileName = $request->input('file_name');
    $fileBase64 = $request->input('file');
    
    $phpWord = new \PhpOffice\PhpWord\PhpWord();
    $section = $phpWord->addSection();
    header("Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document");
    header('Content-Disposition: attachment; filename=' . $fileName);
    $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
    $objWriter->save(storage_path("app/affair/" . $fileName));

    $ifp = fopen( storage_path("app/affair/" . $fileName), 'wb' );
    // we could add validation here with ensuring count( $data ) > 1
    fwrite( $ifp, base64_decode( $fileBase64 ) );
    // clean up the file resource
    fclose( $ifp );

    /**********************************INSERT AFFAIR AND AFFAIRFROM***********************/
//    $origin = $request->input('origin_entity_id');
//    $destiny = $request->input('destiny_entity_id');
//    $file = $request->input('file');
    $affairId = $request->input('affair_id');
    $receiverId = $request->input('receiver_id');
    $senderId = $request->input('sender_id');
    $filesize = $request->input('file_size');
    $fileoriginalname = $request->input('file_original_name');
//    $docTypeId = $request->input('doc_type_id');
//    $folio = $request->input('folio');
//    $date = $request->input('transaction_date');
//    $subject = $request->input('subject');
//    $catPriorities = $request->input('cat_priorities_id');

    $affair = new AffairReception();
    $affair->fill($request->all());
    $affair->subject = $request->input('subject');
    $affair->draft = $request->input('draft');
    $affair->response = $request->input('response');
    $affair->cat_doc_types_id = $request->input('cat_doc_types_id');
    $affair->cat_priorities_id = $request->input('cat_priorities_id');
    $affair->user_id = $request->input('sender_id');
    
    $affair->save();
    
    $affairFrom = new AffairReceptionFrom();
    $affairFrom->affair_receptions_id = $affair->id;
    $affairFrom->receiver_id = $receiverId;
    $affairFrom->participation_types_id = 1;
    $affairFrom->sender_id = $senderId;
    $affairFrom->save();
    
    if($request->input('folio_origin') != '') {
      $affairResponse = new AffairReceptionResponse();
      $affairResponse->folio_origin = $request->input('folio_origin');
      $affairResponse->folio_response = $affair->folio;
      $affairResponse->save();
    }
    
    $file = new File();
    $file->user_id = $receiverId;
    $file->affair_id = $affair->id;
    $file->filetype = 'docx';
    $file->path = 'affair/'.$fileName;
    $file->filename = $fileName;
    $file->filesize = $filesize;
    $file->originalname = $fileoriginalname;
    $file->is_secure = false;
    $file->isActive = true;
    $file->LastModifiedTime = Carbon::now();
    $file->save();

    $token = new AccessToken();
    $token->token = Str::random(40);
    $token->user_id = $receiverId;
    $token->file_id = $file->id;
    $token->UserCanWrite = false;
    $token->HidePrintOption = false;
    $token->DisablePrint = false;
    $token->HideSaveOption = false;
    $token->HideExportOption = false;
    $token->DisableExport = false;
    $token->DisableCopy = false;
    $token->save();

    $user = User::find($receiverId);
    $user2 = User::find($senderId);
    broadcast(new AffairReceived($user->username, $affair, $user2->username));

    return $affair->id;
  }
}
