<?php

namespace App\Http\Controllers;

use App\Models\CatSenderType;
use Illuminate\Http\Request;

class CatSenderTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CatSenderType  $catSenderType
     * @return \Illuminate\Http\Response
     */
    public function show(CatSenderType $catSenderType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CatSenderType  $catSenderType
     * @return \Illuminate\Http\Response
     */
    public function edit(CatSenderType $catSenderType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CatSenderType  $catSenderType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CatSenderType $catSenderType)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CatSenderType  $catSenderType
     * @return \Illuminate\Http\Response
     */
    public function destroy(CatSenderType $catSenderType)
    {
        //
    }
}
