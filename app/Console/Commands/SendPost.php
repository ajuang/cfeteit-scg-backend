<?php

namespace App\Console\Commands;

use App\Http\Controllers\API\AffairReceptionFromController;
use App\Models\AffairReception;
use App\Models\AffairReceptionFrom;
use App\Models\AffairReceptionResponse;
use App\Models\Catalogs\CatUnit;
use App\Models\Entity;
use App\Models\File;
use App\Models\Ftp;
use App\Models\People;
use App\Models\Position;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class SendPost extends Command
{
  /**
   * The name and signature of the console command.
   *
   * @var string
   */
  protected $signature = 'sendpost';

  /**
   * The console command description.
   *
   * @var string
   */
  protected $description = 'Command description';

  /**
   * Create a new command instance.
   *
   * @return void
   */
  public function __construct()
  {
    parent::__construct();
  }

  /**
   * Execute the console command.
   *
   * @return int
   */
  public function handle()
  {

    $connection = new AMQPStreamConnection(env('RABBITMQ_HOST'), env('RABBITMQ_PORT'), env('RABBITMQ_USER'), env('RABBITMQ_PASSWORD'));
    $channel = $connection->channel();

    $channel->queue_declare(env('RABBITMQ_QUEUE'), false, false, false, false);

    echo " [*] Waiting for messages. To exit press CTRL+C\n";
//
    $callback = function ($msg) {
      echo $msg->body;

//        var_dump(explode("=", $msg->body));
      $affairFromReceiverId = explode("&", explode("=", $msg->body)[1])[0];
      $fileName = explode("&", explode("=", $msg->body)[2])[0];
      $authID = explode("=", $msg->body)[3];
      echo 'AUTH = ' . $authID;
      echo 'AFFAIR = ' . $affairFromReceiverId;
      echo 'FILENAME = ' . $fileName;

      //Entidad de origen
      $affairFromReceptionObj = AffairReceptionFrom::where('id', $affairFromReceiverId)->first();
      $affairReceptionObj = AffairReception::where('id', $affairFromReceptionObj->affair_receptions_id)->first();
      $file = File::where('affair_id', $affairFromReceptionObj->affair_receptions_id)->first();
      $peopleOrigin = People::where('user_id', $authID)->first();
      $positionOrigin = Position::where('id', $peopleOrigin->position_id)->first();
      $unitOrigin = CatUnit::where('id', $positionOrigin->cat_unit_id)->first();
      $entityOrigin = Entity::where('id', $unitOrigin->entity_id)->first();

      //Entidad de envío
      $peopleDestiny = People::where('id', $affairFromReceptionObj->receiver_id)->first();
      $positionDestiny = Position::where('id', $peopleDestiny->position_id)->first();
      $unitDestiny = CatUnit::where('id', $positionDestiny->cat_unit_id)->first();
      $entityDestiny = Entity::where('id', $unitDestiny->entity_id)->first();
      $ftp = Ftp::where('entity_id', $entityDestiny->id)->first();
      
      $affairResponse = AffairReceptionResponse::where('folio_response', $affairReceptionObj->folio)->first();
      $folio_origin = "";
      if($affairResponse != null) {
        $folio_origin = $affairResponse->folio_origin;
      }

      if($unitOrigin->entity_id != $entityDestiny->id) {
//          // carpeta = "nombre-de-tu-carpeta";
        $files =  storage_path('app/affair/' . $fileName);
        $fileBase64 = chunk_split(base64_encode(file_get_contents($files)));
        /**************************************************HTTP***********************/
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', env('SERVER_CENTRAL') . 'api/receivefile', [
          'form_params' => [
            'origin_entity_id' => $entityOrigin->id,
            'destiny_entity_id' => $entityDestiny->id,
            'sender_id' => $authID,
            'receiver_id' => $peopleDestiny->user_id,
            'cat_doc_types_id' => $affairReceptionObj->cat_doc_types_id,
            'folio' => $affairReceptionObj->folio,
            'subject' => $affairReceptionObj->subject,
            'cat_priorities_id' => $affairReceptionObj->cat_priorities_id,
            'transaction_date' => $affairReceptionObj->document_date,
            'affair_id' => $affairReceptionObj->id,
            'response' => $affairReceptionObj->response,
            'file' => $fileBase64,
            'file_name' => $fileName,
            'file_size' => $file->filesize,
            'file_original_name' => $file->originalname,
            'folio_origin' => $folio_origin
          ]
        ]);
        $data = json_decode($response->getBody());
        var_dump($data);
      }
      echo ' [x] Received:', $affairReceptionObj->response, ' BODY', $msg->body, "\n";
    };

    $channel->basic_consume(env('RABBITMQ_QUEUE'), '', false, true, false, false, $callback);

    while ($channel->is_consuming()) {
      $channel->wait();
    }

    $channel->close();
    $connection->close();
  }
}
