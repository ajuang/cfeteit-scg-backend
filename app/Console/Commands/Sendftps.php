<?php

namespace App\Console\Commands;

use App\Http\Controllers\API\AffairReceptionFromController;
use App\Models\AffairReception;
use App\Models\AffairReceptionFrom;
use App\Models\Catalogs\CatUnit;
use App\Models\Entity;
use App\Models\Ftp;
use App\Models\People;
use App\Models\Position;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class Sendftps extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendftps';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
//      $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
//      $channel = $connection->channel();
//
//      $channel->queue_declare('dependencia2', false, false, false, false);
//
//      echo " [*] Waiting for messages. To exit press CTRL+C\n";
//
//      $callback = function ($msg) {
//        echo ' [x] Received ', $msg->body, "\n";
//      };
//
//      $channel->basic_consume('dependencia2', '', false, true, false, false, $callback);
//
//      while ($channel->is_consuming()) {
//        $channel->wait();
//      }
//
//      $channel->close();
//      $connection->close();      
      
      
      $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');
      $channel = $connection->channel();

      $channel->queue_declare('queue', false, false, false, false);

      echo " [*] Waiting for messages. To exit press CTRL+C\n";
//
      $callback = function ($msg) {
        echo $msg->body;
        
//        var_dump(explode("=", $msg->body));
        $affairFromReceiverId = explode("&", explode("=", $msg->body)[1])[0];
        $fileName = explode("&", explode("=", $msg->body)[2])[0];
        $authID = explode("=", $msg->body)[3];
        echo 'AUTH = ' . $authID;
        echo 'AFFAIR = ' . $affairFromReceiverId;
        echo 'FILENAME = ' . $fileName;
        
        //Entidad de origen
        $affairFromReceptionObj = AffairReceptionFrom::where('id', $affairFromReceiverId)->first();
        $affairReceptionObj = AffairReception::where('id', $affairFromReceptionObj->affair_receptions_id)->first();
        $peopleOrigin = People::where('user_id', $authID)->first();
        $positionOrigin = Position::where('id', $peopleOrigin->position_id)->first();
        $unitOrigin = CatUnit::where('id', $positionOrigin->cat_unit_id)->first();
        $entityOrigin = Entity::where('id', $unitOrigin->entity_id)->first();
        
        //Entidad de envío
        $peopleDestiny = People::where('id', $affairFromReceiverId)->first();
        $positionDestiny = Position::where('id', $peopleDestiny->position_id)->first();
        $unitDestiny = CatUnit::where('id', $positionDestiny->cat_unit_id)->first();
        $entityDestiny = Entity::where('id', $unitDestiny->entity_id)->first();
        $ftp = Ftp::where('entity_id', $entityDestiny->id)->first();

        if($unitOrigin->entity_id != $entityDestiny->id) {
          // carpeta = "nombre-de-tu-carpeta";
          $files =  storage_path('app/affair/' . $fileName);

          /**************************************************FTP***********************/
          //    $nombre_archivo = $files->getClientOriginalName();
          config(['FTP_HOST' => $ftp->host]);
          config(['FTP_USERNAME' => $ftp->username]);
          config(['FTP_PASSWORD' => $ftp->password]);
          Storage::disk('ftp')->put($fileName, \File::get($files));
        }
        echo ' [x] Received ', $msg->body, "\n";
      };

      $channel->basic_consume('queue', '', false, true, false, false, $callback);

      while ($channel->is_consuming()) {
        $channel->wait();
      }

      $channel->close();
      $connection->close();
    }
}
